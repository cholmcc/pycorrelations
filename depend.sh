#!/usr/bin/bash

one()
{
    inp=$1
    p=`dirname $inp`
    gp=`dirname $p`

    frm=$(sed -n -e '/^[[:space:]]*from \(\.\|correlations\)/p' $inp | \
	      sed 's/^[[:space:]]*from *//' | \
	      sed -e "s,\.\.[[:space:]][[:space:]]*,${gp}/," \
	      	  -e "s,\.[[:space:]][[:space:]]*,${p}/," \
		  -e 's/\. /./' \
		  -e 's/[[:space:]]* import .*/.py/' | \
	      sed -e 's,\.\([^p][^y]\),/\1,g' -e 's,//,,' | \
	      sed -e 's,correlations.py,correlations/__init__.py,' \
		  -e 's,correlations/ana.py,correlations/ana/__init__.py,' \
		  -e 's,correlations/stat.py,correlations/stat/__init__.py,' \
		  -e 's,correlations/gen.py,correlations/gen/__init__.py,' \
		  -e 's,correlations/flow.py,correlations/flow/__init__.py,' \
		  -e 's,correlations/flow.py,correlations/flow/__init__.py,' \
		  -e 's,ana\.,ana/,' | \
	      sort -u)

    if test "x$frm" = "x" ; then
	return
    fi
    echo "${inp}: `echo ${frm} | tr '\n' ' '`"
}

#l=`find . -name "*.py" -and -not -name nbi_stat.py -and -not -name "conf.py"`
for f in $@; do
    case "$f" in
	*.sh) continue ;;
    esac
    
    one $f 
done 
