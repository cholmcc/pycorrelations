#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.insert(0,'.')

from correlations.stat import Estimator
from correlations.stat import Derivatives, Jackknife, Bootstrap

def _f(x):
    a, b, c = x
    return a + b**2 + c**3

def _df(x):
    from numpy import array 
    _, b, c = x
    
    return array([1, 2*b, 3*c**2])
    
class Test(Estimator):
    def __init__(self,policy,**kwargs):
        super(Test,self).__init__(3,policy,**kwargs)

    def value(self,x):
        # print(x)
        return _f(x)

    def derivatives(self,x,dx=None):
        """If left undefined, uses numeric differentiation"""
        return _df(x)

def genData(mean,nev,seed):
    from numpy.random import default_rng
    from numpy import real, imag, sqrt, array

    rng = default_rng(seed)
    x   = rng.normal(real(mean),1,size=(nev,3))
    if type(mean) is complex:
        ix = rng.normal(imag(mean),1,size=(nev,3))
        x  = x + x*1j

    x[:,0] += 1
    x[:,1] /= 10
    x[:,2] /= 10
    x[:,2] += x[:,0]
    w      =  rng.uniform(.75,1,size=(nev,3))

    return x,w, 

def writeData(x,w,out):
    with open(out,'w') as file:
        for xx,ww in zip(x,w):
            print('\t'.join([str(t) for t in xx.tolist()+ww.tolist()]),
                  file=file)

def readData(inp,mean):
    from numpy import array
    
    lx = []
    lw = []
    tp = type(mean)
    with open(inp,'r') as file:
        for i, l in enumerate(file):
            ll = [tp(t) for t in l.split()]
            lx.append(ll[:3])
            lw.append(ll[3:])

    return array(lx), array(lw)
    
def testPolicy(policy=None,mean=0,seed=None,**kwargs):
    from numpy.random import default_rng
    from time import time

    rnd = default_rng(seed) if kwargs.pop('ransel',False) else None
    inp = kwargs.pop('input','')
    nev = kwargs.pop('nev',100)
    
    if inp != "":
        x, w = readData(inp,mean)
    else:
        x, w = genData(mean,nev,seed)

    stt = time()
    if policy is None:
        from numpy import average, diagonal, sqrt
        from nbi_stat import cov 
        md = average(x,weights=w,axis=0)
        cd = cov(x,w,frequency=False,component=True)
        dd = _df(md)
        vd = _f(md)
        ud = sqrt(dd.T @ cd @ dd)/sqrt(len(x))
        return (vd,ud), time()-stt
        
    t = Test(policy,rnd=rnd,**kwargs)    
    t.fill(x,w)
        
    return t(), time()-stt

def ft(t):
    hour = int(t // 3600)
    mins = int((t - hour * 3600) // 60)
    secs = (t - mins * 60)
    return f'{hour}:{mins:02d}:{secs:06.3f}'
    
def run(mean, **kwargs):
    seed = kwargs.pop('seed',0)
    if seed <= 0:
        seed = None

    out = kwargs.pop('output','')
    if out != '':
        x,w = genData(mean,nev,seed)
        writeData(x,w,out)
        
            
    r = {'Direct':      testPolicy(None,       mean,seed,**kwargs),
         'Derivatives': testPolicy(Derivatives,mean,seed,**kwargs),
         'Jackknife':   testPolicy(Jackknife,  mean,seed,**kwargs),
         'Bootstrap':   testPolicy(Bootstrap,  mean,seed,**kwargs),
         }

    for k, (v, t) in r.items():
        print(f'{k:12s}: {v[0]:.5f} +/- {v[1]:.5f}\t{ft(t)}')


if __name__ == "__main__":
    from argparse import ArgumentParser

    ap = ArgumentParser('test')
    ap.add_argument('-i','--input',   type=str,help='input',   default='')
    ap.add_argument('-o','--output',  type=str,help='output',  default='')
    ap.add_argument('-n','--nev',     type=int,help='# events',default=100)
    ap.add_argument('-m','--nsub',    type=int,help='# subs',  default=10)
    ap.add_argument('-M','--nsim',    type=int,help='# sims',  default=1000)
    ap.add_argument('-s','--seed',    type=int,help='Seed',    default=123456)
    ap.add_argument('-c-','--complex',help='Complex type', action='store_true')
    ap.add_argument('-r-','--ransel', help='Random selection',
                    action='store_true')

    args = ap.parse_args()

    mean = 0. if not args.complex else 0+1j

    run(mean,**vars(args))
    
    


                   
        
