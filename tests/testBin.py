#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.insert(0,'.')

from correlations import Result
from correlations.flow import Reference, Full
from correlations.stat import Derivatives, Jackknife, Bootstrap
from unittest import TestCase, main as utmain

class TestReferenceDict(TestCase):
    def _test(self,policy):
        from numpy.random import uniform
        from numpy import pi, exp
        
        i1 = Reference(4,4,policy=policy)
        i1.fill(uniform(0,2*pi,size=10),uniform(.75,1,size=10),
                None,None)
        i1.update()
        d = i1.todict()

        i2 = Reference(4,4,policy=policy)
        i2.fromdict(d)

        for n, vm in i1.v.items():
            for m, v in vm.items():
                for s1, s2 in zip(v._policy._s._state,
                                  i2.v[n][m]._policy._s._state):
                    if s1 is not None and s2 is not None:
                        self.assertListEqual(s1.ravel().tolist(),
                                             s2.ravel().tolist())
                    else:
                        self.assertEqual(s1,s2)


    def testDerivatives(self):
        self._test(Derivatives)

    def testJackknife(self):        
        self._test(Jackknife)

    def testBootstrap(self):
        self._test(Bootstrap)

        
if __name__ == "__main__":
    utmain()

    
    
