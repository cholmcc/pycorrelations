#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.insert(0,'.')

from correlations.stat import Estimator
from correlations.stat import Derivatives, Jackknife, Bootstrap
from unittest import TestCase, main as utmain

class TestEstimatorDict(TestCase):
    def _test(self,policy):
        from numpy.random import normal, uniform
        
        e = Estimator(3,policy)
        e.fill(normal(0,1,size=(10,3)),uniform(.75,1,size=(10,3)))
        
        d = e.todict()

        ee = Estimator(3,policy)
        ee.fromdict(d)

        for s1, s2 in zip(e._policy._s._state,ee._policy._s._state):
            if s1 is not None and s2 is not None:
                self.assertListEqual(s1.ravel().tolist(),s2.ravel().tolist())
            else:
                self.assertEqual(s1,s2)

    def testDerivatives(self):
        self._test(Derivatives)

    def testJackknife(self):        
        self._test(Jackknife)

    def testBootstrap(self):
        self._test(Bootstrap)


class TestEstimatorSample(TestCase):
    class Test(Estimator):
        def __init__(self,policy,**kwargs):
            super(TestEstimatorSample.Test,self).__init__(3,policy,**kwargs)

        @classmethod
        def f(cls,x):
            return sum([xx**(n+1) for n,xx in enumerate(x)])

        @classmethod
        def df(cls,x):
            from numpy import array 
            return array([(n+1)*xx**n for n,xx in enumerate(x)])
            
        def value(self,x):
            return self.__class__.f(x)
    
        def derivatives(self,x,dx=None):
            return self.__class__.df(x)

    @classmethod
    def setUpClass(cls):

        from numpy.random import default_rng
        from numpy import sqrt, array, average, diagonal, sqrt
        from nbi_stat import cov 

        mean = 0
        nev  = 100
        rng  = default_rng(12345)
        x    = rng.normal(mean,1,size=(nev,3))
        x[:,0] += 1
        x[:,1] /= 10
        x[:,2] /= 10
        x[:,2] += x[:,0]
        w      =  rng.uniform(.75,1,size=(nev,3))

        cls.data = (x,w)

        md = average(x,weights=w,axis=0)
        cd = cov(x,w,frequency=False,component=True)
        dd = cls.Test.df(md)
        vd = cls.Test.f(md)
        ud = sqrt(dd.T @ cd @ dd)/sqrt(len(x))

        cls.r = (vd,ud)
        
    def _test(self,policy):
        t = TestEstimatorSample.Test(policy)
        t.fill(*TestEstimatorSample.data)
        v,u = t()
        cv, cu = TestEstimatorSample.r

        self.assertAlmostEqual(v, cv)
        if policy is Derivatives:
            self.assertAlmostEqual(u, cu, 2)

    def testDerivatives(self):
        self._test(Derivatives)

    def testJackknife(self):        
        self._test(Jackknife)

    def testBootstrap(self):
        self._test(Bootstrap)

    
        


if __name__ == "__main__":
    utmain()
