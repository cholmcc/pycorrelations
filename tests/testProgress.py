#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

def run(n):
    import sys
    sys.path.insert(0,'.')
    
    from correlations.ana.progress import Progress  
    from time import sleep  

    p = Progress(30) 
    for i in range(n): 
        sleep(.5) 
        p(i+1,n)


if __name__ == '__main__':
    from argparse import ArgumentParser

    ap = ArgumentParser('Test progress bar')
    ap.add_argument('steps',help='Number of steps',default=100,
                    type=int, nargs='?')

    args = ap.parse_args()
    run(args.steps)

    
