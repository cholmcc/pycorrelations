#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

sys.path.insert(0,'.')

from correlations import HP
from unittest import TestCase, main as uttest


class TestHP(TestCase):

    def setUp(self):
        self.h21 = HP(2,1)
        self.h31 = HP(3,1)
        self.h42 = HP(4,2)
        self.h51 = HP(5,1)

    def testAdd(self):
        h231 = self.h21+self.h31
        self.assertEqual(h231.h, 5)
        self.assertEqual(h231.p, 1)

    def testAddNotSame(self):
        h340 = self.h31+self.h42
        self.assertEqual(h340.h, 7)
        self.assertEqual(h340.p, None)

    def testSum(self):
        l1  = [self.h21,self.h31,self.h51]
        s1  = sum(l1)
        self.assertEqual(s1.h, 10)
        self.assertEqual(s1.p, 1)

    def testList(self):
        k  = 1
        l1  = [self.h21,self.h31,self.h51]
        l2 = l1[:k]+[l1[k]+l1[0]]+l1[k+1:]
        self.assertEqual(l2[k].h, 5)
        self.assertEqual(l2[k].p, 1)

if __name__ == "__main__":
    uttest()
