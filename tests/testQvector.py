#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

sys.path.insert(0,'.')

from correlations import QVector
from unittest import TestCase, main as utmain

class TestQVector(TestCase):
    def testFill(self):
        from numpy import zeros, array, full, pi, isreal
        from cmath import polar, exp

        phi, w = array([pi/4,pi/2]), full(2,2)
        q      = QVector.make(harmonics=[-1,1])
        q.fill(phi,w)

        for n in range(q.maxN+1):
            for m in range(q.maxM+1):
                z = q[n,m]

                if n == 0:
                    self.assertTrue(isreal(z))

                t = sum([ww**m * exp(1j*n*pp) for ww,pp in zip(w,phi)])
                self.assertAlmostEqual(z,t)


if __name__ == "__main__":
    utmain()



        
