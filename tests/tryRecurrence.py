#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

def combs(n,m,k,ind):
    from itertools import combinations
    print(f'{ind}Combinations of length {m-k} (starting {k}) from {n[:m]}')
    for nn in list(combinations(n[0:m],m-k))[k:]:
        print(f'{ind} * {nn}')

def nextCombination(data,k):
    first = 0
    last  = len(data)
    if k in [first,last] or last in [0,1]:
        # print('Nothing')
        return False,data

    # print(f'last={last} {data[last-1]}')
    
    i1 = k           # End of selection
    i2 = last - 1    # Second to last 

    while first != i1:   # Not at beginning
        i1 -= 1
        # print(f' i1={i1}->{data[i1]} i2={i2}->{data[i2]}')
        if data[i1] < data[i2]:  # Previous smaller than second to last
            # IF value at marker is less than the last entry, then point
            # to division 
            j = k  

            # and if it is not smaller than current value, step into
            # unused territory.
            while data[i1] >= data[j]:
                j += 1

            # print(f'  j={j}->{data[j]} vs {data[i1]}')

            # Swap the greater value with our current object 
            data[i1], data[j] = data[j], data[i1]
            # print(f'   {data}')
            # Increment by one 
            i1 += 1
            j  += 1

            # go back to divider 
            i2 = k

            # Rotate elements in range i1 to j and put them at end
            # print(f'  Rotate i1={i1}->j={j}')
            data[i1:] = data[j:]+data[i1:j]
            # print(f'   {data}')

            # Then, while we haven't found our greater value,
            # increment into unused territory.
            while last != j:
                j  += 1
                i2 += 1

            # rotate again past point 
            # print(f'  Rotate k={k}->i2={i2}')
            data[k:] = data[i2:]+data[k:i2]
            # print(f'   {data}')
            return True,data

    # Rotate
    # print(f'Rotate first={0}->k={k}')
    data[:] = data[k:]+data[:k]
    return False,data


def testNC(k=2,n=4):
    print(f'Test of combinations of size {k} of {n}')
    a = list(range(n)) 
    while True:        
        print(a)
        ret, a[:-1] = nextCombination(a[:-1],k)
        if not ret:
            break

            
def test2(m,n,ind=''):
    if m == 0:
        return ''
    if m == 1:
        return f'{ind}{"p" if n[0] == 0 else "r"}[h_{n[0]},1]'

    o = ''
    p = 1
    f = 1
    s = 1

    for i in range(m,0,-1):
        nn = n[:m]
        nn.sort()
        k  = i-1

        while True:
            t = test2(k, nn, ind+" ")

            st = ind
            if t != '':
                st += f'(\n{t}\n{ind}) * '

            st += f'{s*f} * {("p" if p==1 else "q") if nn[k]==0 else "r"}['
            st += '+'.join([f'h_{h}' for h in n[k:m]])
            st += f',{p}]'
            
            if o != '':
                o += ' + '
            o += st
            
            ret, nn[:-1] = nextCombination(nn[:-1],k)

            if not ret:
                break

        f *= (m-k)
        s *= -1
        p += 1

    return o

                
    
def test(m,n,ind=''):
    from itertools import combinations
    # if m == 0:
    #     return
    # if m == 1:
    #     print(f'{ind}{n[0:1]}')
    #     return
    
    p = 1
    f = 1
    s = 1

    for i in range(m,0,-1):
        nn = n[:m].copy()
        k  = i-1

        # print(f'{ind}{m-k}-combinations from {nn[:m]}')
        # combs(nn,m,k,ind)
        for nnn in list(combinations(nn[0:m],m-k))[:k]:
            # print(f'{ind}Starting point now {nn}')
            test(k, list(nnn), ind+" ")

            print(f'{ind}{nnn}')

        f *= (m-k)
        s *= -1
        p += 1
        
            
def run_test(m=4):
    n = list(range(m))
    print(test2(m,n))
    

if __name__ == "__main__":
    m = 4

    import sys
    print(sys.argv)
    if len(sys.argv) > 1:
        m = int(sys.argv[1])
        
    run_test(m)
    
# testNC(0)
# testNC(1)
# testNC(2)
# testNC(3)

# testNC(1,5)
# testNC(2,5)
# testNC(3,5)
# testNC(4,5)
