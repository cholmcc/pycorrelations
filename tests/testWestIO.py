#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

sys.path.insert(0,'.')

from nbi_stat import WestIO
from nbi_stat import West

def gen_one(n,c,f,k,m=100):
    from numpy.random import normal, uniform, poisson
    from tempfile import NamedTemporaryFile

    west = West(n,c,f,k)

    x = normal(size=(m,n))
    w = None

    ws = ((m,n) if k else m)
    w  = poisson(3,size=ws) if f else uniform(size=ws)

    for xx, ww in zip(x,w):
        west.update(xx,ww)
    
    with NamedTemporaryFile(mode='w',delete=False) as file:
        fname = file.name
        WestIO.dump(file,west)

    return west, fname

def read_one(fname):
    with open(fname,'r') as file:
        w = WestIO.load(file)

    return w

def do_one(n,c,f,k,m=100):
    from numpy import allclose
    
    w1, fname = gen_one(n,c,f,k,m=100)
    w2        = read_one(fname)

    for i,(s1, s2) in enumerate(zip(w1._state,w2._state)):
        if s1 is None and s2 is None:
            continue
        if s1 is None:
            print(f'State {i} in s1 is {s1} but {s2.shape} in s2')
        if s2 is None:
            print(f'State {i} in s2 is {s2} but {s1.shape} in s1')

        if not allclose(s1,s2):
            print(f'State {i} in s1 is not close to the same in {s2}\n'
                  f'\t{s2-s1}')

def do_all(n=3,m=100):
    from itertools import combinations
    
    l = list(set(combinations([True,False]*3,3)));
    l.sort();

    for c, f, k in l:
        do_one(n,c,f,k,m)



    
do_all()
