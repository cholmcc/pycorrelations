# Top of the correlations package 

This is the top of the correlations package.  Here we find the most
basic code, such as $`Q`$-vector, storage of these, correlators, and
so on. 

## Sub-packages 

- [`ana`](ana): Analysis code and tasks 
- [`gen`](gen): Data generation 
- [`stat`](stat): Statistics handling 
- [`flow`](flow): Flow calculations
- [`progs`](progs): Various programs 
- [`scripts`](scripts): Various visualisation scripts

## Status 

- [x] [$`Q`$-vector](qvector.py)
- [x] [$`Q`$-vector storage](qstore.py)
- [x] Partitioned (sub-events) harmonics 
- [x] [Correlator](correlator.py) algorithms
  Note, we will _not_ implement loop correlators (nested _or_
  recursive) as they would be _extremely_ slow in Python. 
  - [x] [Closed form](closed.py)
  - [x] [Recurrence](recurrence.py)
  - [x] [Recursive](recursive.py)
  - [x] [Test and benchmark](progs/corr.py)
  
  
## Tests

- [testQVector](../tests/testQvector.py)
- [testRecurrence](../tests/testRecurrence.py)

