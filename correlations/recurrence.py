# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from . correlator import FromQVector

# --- Recurrence form correlations -----------------------------------
class Recurrence(FromQVector):
    r"""Calculate correlators using recurrence expression 
    
    This code used either generic recurssion to calculate the
    correlator

    .. math::
    
        C\{m\}(\mathbf{h}) 
        = \frac{QC\{m\}(\mathbf{h})}{QC\{m\}(\mathbf{0})}

    Parameters
    ----------
    qstore : QStore
        Storage of Q-vectors
    """
    def __init__(self,qstore):
        super(Recurrence,self).__init__(qstore)

    @property
    def maxSize(self):
        """Largest number of particles we can correlate"""
        return -1

    def qc(self,harmonic_vector):
        r"""Evaluate the :math:`m`-particle :math:`Q`-cumulant
        :math:`QC\{m\}(\mathbf{h})` using harmonics :math:`\mathbf{h}`. 

        Parameters 
        ----------
        harmonic_vector : sequence of int or PartitionHarmonic 
            The harmonics to use 
        
        Returns 
        -------
        qc : complex 
            The :math:`Q`-cumulant
        """
        m = len(harmonic_vector)
        n = list(range(m))
        return self.qcm(m,harmonic_vector,n)

    @classmethod
    def nextCombination(cls,data,k):
        r"""Return the next combination of :math:`k` element from the range
        :math:`\mathbf{v}`
        
        Parameters
        ----------
        data : array 
            :math:`\mathbf{v}` 
        k : int 
            :math:`k` 

        Returns
        -------
        ret : bool 
            True if there are more (unique) combinations
        sub : array 
            :math:`k`-element combination

        Credit
        ------
        Mark Nelson http://marknelson.us

        See http://www.dogma.net/markn/articles/Permutations/
        """
        first = 0
        last  = len(data)
        if k in [first,last] or last in [0,1]:
            return False,data

        i1 = k           # End of selection
        i2 = last - 1    # Second to last 

        while first != i1:   # Not at beginning
            i1 -= 1
            if data[i1] < data[i2]:  # Previous smaller than second to
                # last IF value at marker is less than the last entry,
                # then point to division
                j = k  

                # and if it is not smaller than current value, step
                # into unused territory.
                while data[i1] >= data[j]:
                    j += 1

                # Swap the greater value with our current object 
                data[i1], data[j] = data[j], data[i1]
                # Increment by one 
                i1 += 1
                j  += 1

                # go back to divider 
                i2 = k

                # Rotate elements in range i1 to j and put them at end
                data[i1:] = data[j:]+data[i1:j]

                # Then, while we haven't found our greater value,
                # increment into unused territory.
                while last != j:
                    j  += 1
                    i2 += 1

                # rotate again past point 
                data[k:] = data[i2:]+data[k:i2]
                return True,data

        # Rotate
        data = data[k:]+data[:k]
        return False,data

    def qcm(self, m, h, n):
        r"""Evaluate the :math:`m` :math:`Q`-cumulant
        :math:`QC\{m\}(\mathbf{h})` using harmonics :math:`\mathbf{h}`. 
        
        The calculation used the following algorithm 

        .. code-block:: none
        
            C = (0,0)
            for k from n-1 downto 0 do 
                for each combination c of k harmonics except h_n do 
                    let m = sum of harmonics not in c 
                    let p = number of harmonics not in c 
                    let s = -1^(n-k)
                    let C = C + s * (n-1-k)! * QC{k}(c) * Q(m,p)
                end for each c 
            end for k 

        Note that above ``QC{k}(c)`` corresponds to a recursive call
        to this method.
        
        Parameters 
        ----------
        m : int 
            :math:`m` - order (number of particles) of correlation 
        h : array 
            :math:`\mathbf{h}` harmonic vector 
        n : array 
            Index array which we will permute to step through all
            unique combinations.
        
        Returns
        -------
        qc : complex 
            :math:`QC\{k\}(\mathbf{h})`

        """
        rv = self._r
        pv = self._p
        qv = self._q
        
        if m == 0:
            return 1+0j
        if m == 1:
            return pv(h[n[0]],1) if n[0] == 0 else rv(h[n[0]],1)

        o = 0
        p = 1
        f = 1
        s = 1
    
        for i in range(m,0,-1):
            nn = n[:m]
            nn.sort()
            k  = i-1
    
            while True:
                t = self.qcm(k, h, nn)

                # This sum is a little funny because of correlations.HP
                a  =  sum([h[j] for j in nn[k+1:m]],h[nn[k]])
                vv =  (pv if p==1 else qv) if nn[k]==0 else rv
                o  += s * f * t * vv(a,p);
                
                x, nn[:-1] = self.nextCombination(nn[:-1],k)
    
                if not x:
                    break
    
            f *= (m-k)
            s *= -1
            p += 1
    
        return o
        
    
    
#
# EOF
#
