# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from . correlator import FromQVector

# --- Recursive form correlations -----------------------------------
class Recursive(FromQVector):
    r"""Calculate correlators using recursive expression 
    
    This code used either generic recurssion to calculate the
    correlator 

    .. math::
    
        C\{m\}(\mathbf{h}) 
        = \frac{QC\{m\}(\mathbf{h})}{QC\{m\}(\mathbf{0})}

    Parameters
    ----------
    qstore : QStore
        Storage of Q-vectors
    """
    def __init__(self,qstore):
        super(Recursive,self).__init__(qstore)

    @property
    def maxSize(self):
        """Largest number of particles we can correlate"""
        return -1

    def qc(self,harmonic_vector):
        r"""Evaluate the :math:`m` :math:`Q`-cumulant
        :math:`QC\{m\}(\mathbf{h})` using harmonics :math:`\mathbf{h}`. 

        Parameters 
        ----------
        m : int 
            :math:`m` - order (number of particles) of correlation 
        h : array 
            :math:`\mathbf{h}` harmonic vector 
        
        Returns
        -------
        qc : complex 
            :math:`QC\{k\}(\mathbf{h})`
        """
        from numpy import ones 
        m = len(harmonic_vector)
        n = ones(m,dtype=int)
        return self.qcm(m,harmonic_vector,n)

    def qc1(self,h1,n1):
        r"""Calculate the one particle :math:`Q`-cumulant 

        .. math::
         
            QC\{1\}(h_1) = p(h_1,n_1)
        
        Parameters
        ----------
        h1 : int 
            :math:`h_1` 
        n1 : int 
            :math:`n_1` Number of terms in :math:`h_1`
     
        Returns
        -------
        qc : complex 
            :math:`QC\{1\}(h_1)` - the two particle :math:`Q`-cumulant
        """
        return self._p(h1,n1)

    def qc2(self,h1,h2,n1,n2):
        r"""Calculate the two particle correlator.  If :math:`h_1` is 
        _not_ the sum of other harmonics (:math:`n_1 = 1`), then we 
        calculate
        
        .. math::

            QC\{2\}(h_1,h_2)=p(h_1,n_1)r(h_2,n_2) - q(h_1+h_2,n_1+n_2)\quad,
     
        otherwise we need to replace :math:`p` with :math:`q` to get 
     
        .. math::
        
            QC\{2\}(h_1,h_2)=q(h_1,n_1)r(h_2,n_2) - q(h_1+h_2,n_1+n_2)
        
        Parameters
        ----------
        h1 : int 
            :math:`h_1` 
        h2 : int 
            :math:`h_2` 
        n1 : int 
            :math:`n_1` Number of terms in :math:`h_1`
        n2 : int 
            :math:`n_2` Number of terms in :math:`h_2`
     
        Returns
        -------
        qc : complex 
            :math:`QC\{2\}(h_1,h_2)` - the two particle :math:`Q`-cumulant
        """
        v = self._q if n1 > 1 else self._p
        return v(h1,n1) * self._r(h2,n2) - self._q(h1+h2,n1+n2)

    def qcm(self, m, h, n):
        r"""Evaluate the :math:`m` :math:`Q`-cumulant
        :math:`QC\{m\}(\mathbf{h})` using harmonics :math:`\mathbf{h}`. 

        This follows the following algorithm 
        
        .. code-block:: none
            
            C = q(h_{m}, cnt_{m});

            if m == 1 then return c;
     
            C = C * QC(m, h, cnt);
     
            if (cnt[nm] > 1) return C;
     
            for i from 1 upto m-1 do
                 let h[i]   = h[i] + h[m];
                 let cnt[i] = cnt[i]-1;
         
                 C -= (cnt[i]-1) * QC(m-1, h, cnt);
            
                 let cnt[i] = cnt[i]-1;
                 let h[i]   = h[i] - h[m];
            end for i

        Note, above ``QC(m,h,cnt)`` are recursive calls to this method. 

        Parameters 
        ----------
        m : int 
            :math:`m` - order (number of particles) of correlation 
        h : array 
            :math:`\mathbf{h}` harmonic vector 
        n : array 
            Counter of terms 
        
        Returns
        -------
        qc : complex 
            :math:`QC\{k\}(\mathbf{h})`
        """
        from numpy import zeros_like
        
        if m == 1: return self.qc1(h[0],n[0])
        if m == 2: return self.qc2(h[0],h[1],n[0],n[1])

        j  = m-1
        hl = list(h)
        hj = h[j]
        nj = n[j]
        c  = self._r(hj,nj) * self.qcm(j,h,n)

        if nj > 1: return c

        t  = zeros_like(h)
        
        for i in range(j):
            t    =  hl[:i]+[h[i]+hj]+hl[i+1:]
            n[i] += 1
            c    -= (n[i]-1) * self.qcm(j,t,n)
            n[i] -= 1

        return c
    
#
# EOF
#
