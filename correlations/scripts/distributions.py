#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.insert(0,'.')

def plot(file,ax=None):
    from matplotlib.pyplot import subplots
    from correlations.scripts.utils import load, plot_dist, gen_title

    fig, ax = subplots(nrows=2,ncols=2) if ax is None else (ax.figure, ax)
    
    header, results, info = load(file)
    opts = {
        'dndeta': {'varname': r'\eta'},
        'dndpt':  {'varname': r'p_{\mathrm{T}}', 'logy': True },
        'dndphi': {'varname': r'\varphi'},
        'dndw':   {'varname': r'w' } }

    for i, (r,a) in enumerate(zip(results,ax.ravel())):
        plot_dist(results[r],a,**opts[r],color=f'C{i}',ecolor=f'C{i}')

    fig.suptitle(f'{header["nev"]} events')
    ax[0,0].set_title(fr'$M\in{header["multiplicity"]}$')
    ax[0,1].set_title(r'$v_n(p_{\mathrm{T}})='+
                      ''.join([fr'{c:+}p_{{\mathrm{{T}}}}^{i}'
                               for i,c in enumerate(header["pt_coef"])
                               if c != 0])+'$\n'
                      r'$p_{\mathrm{T}}\sim'
                      fr'\mathrm{{E}}[1/{header["mean_pt"]}]$')
    ax[1,0].set_title(' '.join([fr'$v_{n+1}={v}$'
                                for n, v in enumerate(header['flow_coef'])]))

    fig.suptitle(gen_title(info))
    fig.tight_layout()
    return fig

if __name__ == "__main__":
    from correlations.scripts.utils import plot_main

    plot_main('Plot distributions', plot)
    
    

    
    
