#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.insert(0,'.')

def plot(file,cut=.5,ax=None):
    from matplotlib.pyplot import gca
    from matplotlib.legend_handler import HandlerLine2D, HandlerTuple
    from numpy import arange, unique
    from correlations.scripts.utils import load, plot_vp, plot_vpn, \
        get_axes_with_ratio, gen_title, annotate
    from copy import copy
    
    fig, ax               = get_axes_with_ratio(ax)
    header, results, info = load(file)
    inp_vn                = header['flow_coef']
    pt_coef               = header['pt_coef']
    pt_bins               = header['pt_bin']
    ns                    = arange(1,len(inp_vn)+1)
    p,f                   = plot_vpn(0,inp_vn,pt_bins,pt_coef,ax=ax)
    il                    = list(p.values())[0]
    il.set_color('k')
    # p.set_label('Input')

    labels          = plot_vp(results, ax=ax, cut=cut, f=f)
    ml              = {k:copy(list(labels[k].values())[0]) for k in labels}
    mm              = [fr'$m={m}$' for m in ml]
    ml              = list(ml.values())
    for l in ml: 
        l.set_color('k')
        if l.get_markerfacecolor() != 'none':
            l.set_markerfacecolor('k')

    pp = {}
    for n in set(p.keys()) | \
        set(sum([list(l.keys()) for l in labels.values()],[])):
        pp[n] = [p.get(n,[])]+[labels[m][n] for m in labels]
        

    lp = list(pp.values())
    de = info.get('eta_gap',None)
    ax[0].legend([il]+ml+lp,
                 [il.get_label()]+mm+[fr'$n={n}$' for n in pp],
                 numpoints=1,
                 handler_map={list: HandlerTuple(ndivide=None)})
    ax[1].set_xlabel(r'$p_{\mathrm{T}}$')
    ax[0].set_ylabel(r"$v'_{n}\{m\}$")
    ax[1].set_ylabel(r"$\Delta v'_{n}\{m\}$")
    ax[0].set_title(fr'$p_{{\mathrm{{T}}}}$-differential flow '
                    f'over {header["nev"]} events'+
                    ('' if de is None or de < 0 else fr' $(\Delta\eta>{de})$'))
    ax[1].set_ylim(-1,1)
    ax[1].axhline(0,ls='--',color='gray')

    annotate(ax[1],.05,.02,info,True)
    
    fig = ax[0].figure
    fig.suptitle(gen_title(info,cut))
    fig.tight_layout()

    return fig

if __name__ == "__main__":
    from correlations.scripts.utils import plot_main
    
    plot_main('Plot differential flow',plot,.5)
    
#
# EOF
#
