#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.insert(0,'.')

def one(input,off,w,ax):
    from correlations.scripts.utils import load
    from numpy import array, sqrt
    from matplotlib.lines import Line2D
    
    header, result, info = load(input)
    mode                 = result['mode']
    times                = result['times']
    t0, dt0              = times["0"]["t"], times["0"]["dt"]
    c                    = array([[float(k),v['real'],v['imag'],v['t'],v['dt']]
                                  for k, v in times.items() if k != 0])
    r                    = ax[0].plot(c[1:,0]+off,c[1:,1],'o')
    i                    = ax[0].plot(c[1:,0]+off,c[1:,2],'o',
                                      color=r[0].get_color(),
                                      markerfacecolor='none')

    ax[1].bar(0+off,2*dt0,w,t0,color=r[0].get_color(),alpha=.5)[0]
    ax[1].errorbar(c[1:,0],c[1:,3],c[1:,4],fmt='o-',color=r[0].get_color())
    ax[1].errorbar(c[1:,0],c[1:,3]+t0,sqrt(c[1:,4]**2+dt0**2),
                   fmt='o-',color=r[0].get_color(),markerfacecolor='none')
        
    lbl = mode.replace('qvector_',r'$Q$-vector ').replace('nested_', r'Loop ')
    return Line2D([],[],ls='none',marker='o',color=r[0].get_color(), label=lbl)
    
    
def plot(input,ax=None):
    from matplotlib.pyplot import subplots
    from matplotlib.lines import Line2D
    from matplotlib.patches import Rectangle
    from numpy import linspace
    
    try:
        len(input)
    except:
        input = [input]

    fig, ax = subplots(ncols=2) if ax is None else (ax,ax.figure)
    offs,s  = linspace(-.3,.3,len(input),retstep=True)

    l = [Line2D([],[],ls='none',marker='o',color='k',
                label=r'$\mathcal{R}(C_n)$'),
         Line2D([],[],ls='none',marker='o',color='k',markerfacecolor='none',
                label=r'$\mathcal{I}(C_n)$')] + \
        [one(i,o,.8*s,ax) for i,o in zip(input,offs)]
    ax[0].legend(l,[ll.get_label() for ll in l])
    ax[0].set_ylabel(r'Correlator') 
    ax[0].set_xlabel(r'$m$')


    ll = [Rectangle([0,0],s,s,color='k',alpha=.5,
                    label=r'Fill $\varphi$'),
          Line2D([],[],marker='o',ls='none',color='k',
                 label='Correlator calculation'),
          Line2D([],[],marker='o',ls='none',color='k',markerfacecolor='none',
                 label='Total')]
    ax[1].legend(ll,[lll.get_label() for lll in ll])
    ax[1].set_ylabel(r'$\overline{t}$ per event')
    ax[1].set_xlabel(r'$m$')          
    ax[1].set_xticks(ax[0].get_xticks())
    ax[1].set_yscale('log')
    #ax[1].set_xticklabels(ax[0].get_xticklabels())
    
    fig.tight_layout()
    return fig

if __name__ == "__main__":
    from argparse import ArgumentParser, FileType
    from correlations.scripts.utils import pltion, setup_style
    
    ap = ArgumentParser('Plot benchmark')
    ap.add_argument('input',nargs='+',type=FileType('r'),
                    help='Input files')
    ap.add_argument('-o','--output',nargs='?',type=FileType('wb'),default=None,
                     help='Output file')

    args = ap.parse_args()

    pltion()
    setup_style()
    
    fig = plot(args.input)

    if fig is not None and args.output is not None:
        fig.savefig(args.output)
