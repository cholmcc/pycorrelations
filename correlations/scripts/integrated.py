#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.insert(0,'.')

def plot(file,ax=None):
    from matplotlib.pyplot import gca
    from numpy import arange, unique
    from correlations.scripts.utils import load, plot_v, plot_vn, \
        gen_title, annotate

    ax = gca() if ax is None else ax 
    
    header, results, info = load(file)
    inp_vn                = header['flow_coef']
    ns                    = arange(1,len(inp_vn)+1)
    p                     = plot_vn(0,ns,inp_vn,ax=ax)    
    labels                = [p]+plot_v(results, ax=ax)

    p.set_label('Input')

    de = info.get('eta_gap',None)
    ax.legend(labels,[l.get_label() for l in labels])
    ax.set_xticks(ns)
    ax.set_xticklabels([fr'${n}$' for n in ns])
    ax.set_xlabel('$n$')
    ax.set_ylabel(r'$v_{n}\{m\}$')
    ax.set_title(fr'Integrated flow over {header["nev"]} events'+
                 ('' if de is None else fr' $(\Delta\eta>{de})$'))

    annotate(ax,.05,.9,info)
    
    fig = ax.figure
    fig.suptitle(gen_title(info))
    fig.tight_layout()

    return fig
    
if __name__ == "__main__":
    from correlations.scripts.utils import plot_main

    plot_main('Plot integrated flow',plot)
    
#
# EOF
#
