# Programs 

Here we find some programs to explore this project 

- [`writer.py`](writer.py) Write out simulated events 
- [`corr.py`](corr.py) tests and benchmarks the various correlator
  algorithms. 
- [`integrated.py`](integrated.py) determine integrated flow.  Use the
  script
  [`correlations.scripts.integrated.py`](../scripts/integrated.py) to
  visualize results 
- [`differential.py`](differential.py) determine $`p_{\mathrm{T}}`$
  differential flow.  Use the script
  [`correlations.scripts.differential.py`](../scripts/differential.py)
  to visualize results
- ... (more to come)


## Status 

- [x] [Event generator](writer.py)
- [x] [Correlations benchmark](corr.py)
- [x] [Integrated flow](integrated.py)
- [x] [Differential flow](differential.py)
- [ ] [Differential flow in partitions](partitions.py)
- [ ] Flow programs

