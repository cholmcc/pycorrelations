#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.insert(0,'.')

from correlations.ana import Writer
from correlations.ana.tools import *

def run(**kwargs):
    from numpy.random import default_rng

    with CleanOutput(kwargs.get('output',None)):
        seed    = kwargs.get('seed',		None)
        rnd     = default_rng(None if seed==0 else seed)
        ana     = Writer()
        kwargs['no_final'] = True
    
        run_ana(ana,rnd,**kwargs)


if __name__ == "__main__":
    ap = arg_setup('Write data to file')

    args = ap.parse_args()
    run(**vars(args))

    
