#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.insert(0,'.')

from correlations.ana.benchmark import Benchmark
from correlations.ana.tools import *

def run(**kwargs):
    from numpy.random import default_rng
    from numpy import unique
    
    with CleanOutput(kwargs.get('output',None)):
        seed    = kwargs.get('seed',		None)
        rnd     = default_rng(None if seed==0 else seed)
        ana     = Benchmark(kwargs.get('mode'),
                            rnd,
                            kwargs.get('max_correlator',5),
                            kwargs.get('harmonics',None),
                            kwargs.get('partitions',None),
                            kwargs.get('use_weights',True))
        
        run_ana(ana,rnd,**kwargs)
    
if __name__ == "__main__":
    ap = arg_setup('Calculate correlators and benchmark')
    gr = ap.add_argument_group('Correlations',
                               'Specific options for this program')
    gr.add_argument('-a','--mode',type=str,default='qvector_closed',
                    help='Type of correlator to use')
    gr.add_argument('-H','--harmonics',nargs='*',type=int,
                    help='Harmonics to use',default=None);
    gr.add_argument('-p','--partitions',nargs='*',type=int,
                    help='Partitions to use',default=None)
    ap.add_argument('-w','--use-weights',action='store_false',
                    help='Use and filter on weights')

    args = ap.parse_args()
    run(**vars(args))

#
# EOF
#
