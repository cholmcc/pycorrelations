#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.insert(0,'.')

from correlations.ana.partitioned import Differential, Integrated 
from correlations.ana.tools        import *


def run(**kwargs):
    from numpy.random import default_rng
    from numpy import linspace
    
    with CleanOutput(kwargs.get('output',None)):
        seed    = kwargs.get('seed',		None)
        rnd     = default_rng(None if seed==0 else seed)
        delta   = kwargs.get('delta_eta',.2)
        maxN    = kwargs.get('max_harmonic',4)
        maxM    = kwargs.get('max_particles',4)
        weight  = kwargs.get('use_weights',True)
        ptbins  = kwargs.get('pt_bins', linspace(0,5,11))
        policy  = kwargs.get('uncertainty','Derivatives')
        corr    = kwargs.get('correlator','Closed')
        
        if kwargs.get('integrated',True):
            ana = Integrated(delta,maxN,maxM,weight,
                             policy=policy,
                             correlator=corr)
        else:
            ana = Differential(delta,maxN,maxM,weight,ptbins,
                               policy=policy,
                               correlator=corr)

        print(ana)
        run_ana(ana, rnd, **kwargs)

if __name__ == "__main__":
    ap   = arg_setup('Calculate pT-differential flow',flow=True)
    gr   = ap.add_argument_group('Partitions')
    gr.add_argument('-x','--delta-eta',type=float,
                    default=.4,help='Separation between partitions')
    gr1  = ap.add_mutually_exclusive_group(required=True)
    gr1.add_argument('-I','--integrated',  action='store_true',
                     help='Do integrated flow')
    gr1.add_argument('-D','--differential',action='store_true',
                     help='Do differential flow')
    args = ap.parse_args()
    run(**vars(args))

    

