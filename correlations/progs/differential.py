#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.insert(0,'.')

from correlations.ana.differential import Differential
from correlations.ana.tools        import *


def run(**kwargs):
    from numpy.random import default_rng
    from numpy import linspace

    with CleanOutput(kwargs.get('output',None)):
        seed    = kwargs.get('seed',		None)
        rnd     = default_rng(None if seed==0 else seed)
        ana     = Differential(kwargs.get('max_harmonic',4),
                               kwargs.get('max_particles',4),
                               kwargs.get('use_weights',True),
                               kwargs.get('pt_bins', linspace(0,5,11)),
                               policy=kwargs.get('uncertainty','Derivatives'),
                               correlator=kwargs.get('correlator','Closed'))

        run_ana(ana, rnd, **kwargs)

if __name__ == "__main__":
    ap   = arg_setup('Calculate pT-differential flow',flow=True)
    args = ap.parse_args()
    run(**vars(args))

    

