# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
class HP(tuple):
    r"""A harmonic for a particular partition. 

    The harmonic :math:`h` is the prefactor in the :math:`Q`-vector
    definition

    .. math:: 

        Q_{h,p} = \sum_{i=1}^{M} e^{ih\varphi}


    The partition :math:`p` is any identifier of a partition.  This
    can in principle be anything but should be __hashable__.  

    We can write objects of this kind as a short hand 

    .. math::

        h^p


    We overload the ``+`` operation between objects of this kind satisfying 

    .. math::
    
        x^{p} + y^{q} 
        = \begin{cases} 
            (x+y)^p & \text{if}\quad p=q\\
            0^{\emptyset} & \text{if}\quad p\ne q
          \end{cases}
    

    That is, if we try to combine two (or more) particles from
    different partitions, we get no contribution.  In this way, we can
    automatically perform gapped measurements by simply specifying
    which partition a given harmonic belongs to.

    Parameters
    ----------
    h : int 
        Harmonic :math:`h` 
    p : hash-able 
        Partition identifier 
    """
    def __new__(self,h,p=0):
        return tuple.__new__(HP,(h,p))

    @property
    def h(self):
        """Get the harmonic :math:`h`"""
        return self[0]
    
    @property
    def p(self):
        """Get the partition identifier :math:`p`"""
        return self[1]

    def __str__(self):
        return f'{self.h}{{{self.p}}}'

    def __add__(self,o):
        return HP(self.h + o.h, self.p if self.p == o.p else None)

    def __radd__(self,o):
        return HP(self.h+o,self.p)

    @classmethod
    def _fullname(cls):
        return cls.__module__+'.'+cls.__name__

    def todict(self):
        return {'class':HP._fullname(),
                'h': self.h,
                'p': self.p }

    @classmethod
    def fromdict(cls,d):
        assert d['class'] == HP._fullname()

        return cls(d['h'], d['p'])
        

#
# EOF
#
