# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --- Result of calculation from Q-vectors ---------------------------
class Result:
    """Result of Q-vector calculations 

    Parameters 
    ----------
    c : complex 
        Numerator 
    w : float 
        Denominator 
    """
    def __init__(self,c,w):
        self._sum = c
        self._weights = w

    @property
    def sum(self):
        """The :math:`Q`-cumulant sum"""
        return self._sum

    @sum.setter
    def sum(self,value):
        self._sum = value
    
    @property
    def weight(self):
        """Weight"""
        return self._weights

    def __iadd__(self,other):
        """Add another result to this

        Parameters
        ----------
        other : Result
            Other result to add to this 

        Returns
        -------
        self
        """
        self._sum    += other.sum
        self._weight += other.weight

        return self
    
    def eval(self):
        """Evaluate result
        
        Returns 
        -------
        q : complex 
            Fraction of sum to weights 
        """
        return self._sum / self._weights if self._weights != 0 else 0+0j

    @property 
    def real(self):
        from numpy import real
        return real(self.eval())

    def __str__(self):
        return f'{self._sum}/{self._weights}'

    def __repr__(self):
        return self.__str__()

# --- Base class -----------------------------------------------------
class Correlator:
    """Base class for correlators"""
    def __init__(self):
        pass

    @property
    def maxSize(self):
        """Maximum correlate we can calculate"""
        return 0
    
    def __call__(self,harmonic_vector,n=None):
        """Calculate the correlator for the given harmonics

        Parameters 
        ----------
        n : int 
            Order to use. If not specified, use length or harmonic_vector
        harmonic_vector : array of int or PartitionHarmonic 
            Harmonics to use 

        Returns
        -------
        qc : Result 
            The result 
        """
        n = len(harmonic_vector) if n is None else n

        if self.maxSize > 0 and self.maxSize < n:
            raise ValueError(f'Number of particles {n} too big '
                             f'(max {self.maxSize})')

        # print(f'Calculating correlator {harmonic_vector}')
        return self.mcorr(n, harmonic_vector)

    def mcorr(self,m,harmonic_vector):
        r"""Calculate the correlator for the given harmonics

        Calculate the math:`m@`-particle correlation using harmonics
        :math:`\mathbf{h}`
     
        .. math::

            C_{\mathbf{h}}\{m\} = \langle\exp[i(\sum_j^m h_j\phi_j)]\rangle

        Parameters 
        ----------
        m : int 
            Order to use 
        harmonic_vector : array of int or PartitionHarmonic 
            :math:`=h_1,\ldots,h_n` Harmonic of each term

        Returns
        -------
        qc : Result 
            The result 
        """
        raise NotImplemented('Correlator.mcorr not implemented')

    def __str__(self):
        """String representation"""
        return "Correlator"

    @classmethod
    def type(cls,name):
        lname = name.lower()
        if 'clos' in lname:
            cn = "Closed"
        elif 'recurr' in lname:
            cn = "Recurrence"
        elif 'recurs' in lname:
            cn = "Recursive"
        else:
            raise ValueError(f'Unknown correlator "{name}"')

        from importlib import import_module

        return getattr(import_module('correlations.'+cn.lower()),cn)

    @classmethod
    def make(cls,name,qstore):
        cl = cls.type(name)
        return cl(qstore)
    
    

# --- Calculate from Q-vectors ---------------------------------------
class FromQVector(Correlator):
    r"""Create correlator that uses :math:`Q`-vectors 

    Parameters 
    ----------
    qstore : QStore 
        Storage of :math:`Q`-vectors 
    """
    def __init__(self, qstore):
        from numpy import empty
        super(FromQVector,self).__init__()

        self._s    = qstore

    def _r(self,harmonic, power):
        r""" Get reference :math:`Q`-vector component :math:`r_{h,p}`

        Parameters
        ---------- 
        h : int 
           :math:`h` - typically sum of a harmonics vector 
        p : int 
           :math:`p` (power) number of terms that make up :math:`h` 

        Returns:
        -------
        element : complex 
            :math:`r_{h,p}`
        """
        return self._s.r(harmonic,power)

    def _p(self,harmonic, power):
        r""" Get of-interest :math:`Q`-vector component :math:`p_{h,p}`

        Parameters
        ---------- 
        h : int 
           :math:`h` - typically sum of a harmonics vector 
        p : int 
           :math:`p` (power) number of terms that make up :math:`h` 

        Returns:
        -------
        element : complex 
            :math:`p_{h,p}`
        """
        return self._s.p(harmonic,power)

    def _q(self,harmonic, power):
        r""" Get overlap :math:`Q`-vector component :math:`q_{h,p}`

        Parameters
        ---------- 
        h : int 
           :math:`h` - typically sum of a harmonics vector 
        p : int 
           :math:`p` (power) number of terms that make up :math:`h` 

        Returns:
        -------
        element : complex 
            :math:`q_{h,p}`
        """
        return self._s.q(harmonic,power)

    def mcorr(self,m,harmonic_vector):
        r"""Calculate the :math:`m`-particle correlation using harmonics
        :math:`\mathbf{h}`

        For integrated observations (i.e., :math:`Q=p=r=q`)
        
        .. math::
        
           C_{\mathbf{h}} = \frac{N_{\mathbf{h}}}{D_{\mathbf{h}}}\quad,
        
       and for differential observations 
        
       .. math::

           C'_{\mathbf{h}} = \frac{N'_{\mathbf{h}}}{D'_{\mathbf{h}}}\quad.
        
        Parameters 
        ----------
        m : int 
            :math:`m` - how many particles to correlate
        harmonic_vector : array of int or PartitionHarmonic 
            :math:`\mathbf{h}` 

        Returns 
        ------- 
        qc : Result 
            Result of calculation (The correlator and the summed weights)
        """
        from numpy import zeros
        from . harmonic import HP
        
        null = [0]*m
        if isinstance(harmonic_vector[0],HP):
            null = [HP(0,hp.p) for hp in harmonic_vector[:m]]
        return Result(self.qc(harmonic_vector[:m]),
                      self.qc(null).real)

    def qc(self,harmonic_vector):
        r"""Calculate the :math:`Q`-cumulant of order :math:`m` using harmonics

        Parameters 
        ----------
        harmonic_vector : array of int or PartitionHarmonic 
            :math:`\mathbf{h}` - harmonics to use 

        Returns 
        ------- 
        qc : Result 
            The :math:`m`-particle correlator

        """
        raise NotImplemented('FromQVector.eval not implemented')
        
    
#
# EOF
#
