# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Various IO utilities for stat package.

Mainly turning statistics object of class :class:`nbi_stat.West` and
:class:`nbi_stat.Welford` into dictionaries for serialisation"""

def _arr2list(a):
    if a is None:
        return None
    
    return a.ravel().tolist()

def _list2arr(l,shp):
    from numpy import array 
    if l is None:
        return None

    return array(l).reshape(shp)
        

def west2dict(west):
    """Turn a west object into a dictionary"""
    from nbi_stat import West
    
    out = { 'class': West.__module__+'.'+West.__qualname__,
            'n': len(west),
            'cov': west.cov is not None,
            'frq': west.is_frequency(),
            'cmp': west.is_component() }

    out.update({nme : _arr2list(arr)
                for nme, arr in zip(['mean','cv','sumw','sumw2','summw'],
                                    west._state) })
    if west.is_component():
        out['var'] = welford2dict(west._var)

    #from pprint import pprint
    #for s in west._state:
    #    print(s)
    #pprint(out)
    return out

def dict2west(d):
    """Turn a dictionary into a West object"""
    from nbi_stat import West    

    assert d.get('class') == West.__module__+'.'+West.__qualname__
    
    n,c,f,k  = d['n'],d['cov'],d['frq'],d['cmp']
    w        = West(n,c,f,k)
    m        = _list2arr(d['mean'],  n)
    cv       = _list2arr(d['cv'],    (n,n)    if c else n)
    sumw     = _list2arr(d['sumw'],  cv.shape if k else 1)
    sumw2    = _list2arr(d['sumw2'], cv.shape if k and not f else
                         (1 if not f else 0))
    summw    = _list2arr(d['summw'], n        if c and k else 0)
    w._state = (m,cv,sumw,sumw2,summw)

    if w.is_component:
        w._var._state = dict2welford(d['var'])._state

    return w

def welford2dict(welford):
    from nbi_stat import Welford
    return {'class':  Welford.__module__+'.'+Welford.__name__,
            'n':      len(welford),
            'cov':    welford.cov is not None,
            'mean' :  _arr2list(welford._state[0]),
            'cv':     _arr2list(welford._state[1]),
            'count':  welford._state[2] }

def dict2welford(d):
    from nbi_stat import Welford

    assert d['class'] == Welford.__module__+'.'+Welford.__name__

    n, cov   = d['n'], d['cov']
    w        = Welford(n,cov)
    m        = _list2arr(d['mean'], n)
    cv       = _list2arr(d['cv'], (n,n) if cov else n)
    cnt      = d['count']
    w._state = (m,cv,cnt)

    return w
#
# EOF
#

    
