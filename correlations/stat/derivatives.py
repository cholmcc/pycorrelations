# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from . policy import Policy

class Derivatives(Policy):
    """Constructs a policy that uses the derivatives of the estimator with
    respect to the independent variable(s) to calculate the
    uncertainty.

    Parameters 
    ----------
    client : Estimator
        The client object that will use this.  If its class
        derives from estimator, then this is done automatic.

    """
    def __init__(self,client,**kwargs):
        super(Derivatives,self).__init__(client,True,**kwargs)

    def propagate(self,df,dx):
        r"""Propagate uncertainty of independent variables to estimator. 
        
        Calculates 

        .. math::

            e = \nabla^T C \nabla

        where :math:`\nabla` is the gradient (partial derivatives with
        respect to each independent variable) of the estimator

        .. math::

            \nabla_i = \frac{\partial v}{\partial x_i}

        and :math:`C` is the covariance matrix of the elements 

        .. math::
        
            C_{ij} = \rho_{ij}\delta_i\delta_j\quad.

        Parameters
        ----------
        df : array 
             Derivatives of estimator with respect to the independent
             variables near the point of evaluation.
        dx : array 
             Uncertainty and step size (for numerical differentiation)
             in each independent variable.

        Returns
        -------
        var : scalar 
            Variance of estimator

        """
        from numpy import newaxis

        covar  = (dx[:,newaxis] @ dx[newaxis,:]) * self.rho
        return df.T @ covar @ df

    def uncertainty(self,x,dx=None):
        """Calculates uncertainties on estimator value

        Parameters
        ----------
        x : array 
            Value of independent variables 
        dx : array (optional)
            Standard deviation of independent variables 
        
        Returns
        -------
        unc : scalar 
            Uncertainty on the estimator around x 
        """
        from numpy import sqrt

        if dx is None:
            dx = self.sem
            
        df = self._c.derivatives(x,dx)
        return sqrt(self.propagate(df,dx))

    def todict(self):
        """Turn this into a dictionary"""
        return {'class': Derivatives._fullname(),
                'base' : super(Derivatives,self).todict() }

    def fromdict(self,d):
        """Read state from dictionary

        Parameters
        ----------
        d : dict 
            Dictionary 
        """
        assert d['class'] == Derivatives._fullname()

        super(Derivatives,self).fromdict(d['base'])

#
# EOF
#
