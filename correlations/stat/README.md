# Statistics on estimators 

Here we find code to help estimate the statistical uncertainty on our
estimators (flow, correlations, cumulants). 

## Status 

- [x] [Policies](policy.py)
  - [x] [`Derivatives`](derivatives.py)
  - [x] [Sub-sampling](subsamples.py)
    - [x] [`Jackknife`](subsamples.py)
    - [x] [`Bootstrap`](subsamples.py)	
- [x] [`Estimator.py`](estimator.py)

## Tests 

- [`testEstimator`](../../tests/testEstimator.py)
- [`testWestIO`](../../tests/testWestIO.py)

	
