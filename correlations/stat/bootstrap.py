# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from . subsamples import SubSamples

# --- Bootstrapping --------------------------------------------------
class Bootstrap(SubSamples):
    r"""Initialize Bootstrap policy.

    Suppose we are interested in the quantity :math:`T` calculated
    over the data :math:`X` (more formally, :math:`T` is a
    *statistics*).  We estimate :math:`T` via the estimator
    :math:`\hat{T}` over the sample :math:`X_1,X_2,\ldots X_N:, of
    size :math:`N`. We are interested in estimating the variance
    :math:`\operatorname{Var}(T(X))`
 
    * First, estimate the :math:`T` over our sample :math:`X`
    * Secondly, for some number of iterations :math:`B`, do  
    
      * Select, at random _with_ replacement, :math:`N` samples
        from the original sample
      *  Calculate the estimate :math:`T` over this sample 

    * Finally, calculate the variance of :math:`T` estimated over
      the :math:`B` generated samples.

    Parameters 
    ----------
    client : Estimator
        The client object that will use this.  If its class
        derives from estimator, then this is done automatic.
    nsub : int 
        Number of sub-samples to use (:math:`N` above)
    nsim : int 
        Number of simulation samples to use (:math:`B` above)
    rnd : numpy.random.Generator (optional) Random number
         generator.  If passed, then it will be used to select
         sub-samples at random.  If not passed, then the
         sub-samples are chosen in a round-robin fashion.  Note,
         this also applies when we do the final simulations.
    kwargs : dict
         Additional arguments.

    """
    def __init__(self,client,nsub=10,nsim=1000,rnd=None,**kwargs):
        super(Bootstrap,self).__init__(client,nsub,rnd=rnd,**kwargs)
        self._nsim = nsim

    def simulate(self):
        """Run the simulations.  

        We run NSIM simulations.  In each simulation, we pick a
        sub-sample at random and calculate the value of the estimator
        from the means of that sub-sample.  Then, we determine the
        standard deviation of all those means to get an estimate of
        the uncertainty of the estimator.

        Returns 
        -------
        unc : scalar 
            The uncertainty estimate 
        """
        from numpy import array

        # Value of means of all sub-samples
        v = array([self._c.value(self._sub[i].mean)
                   for i in self.select(self._nsim)])
        return v.std()

    def todict(self):
        """Turn this into a dictionary"""
        return {'class': Bootstrap._fullname(),
                'nsim':  self._nsim,
                'base' : super(Bootstrap,self).todict() }
    
    def fromdict(self,d):
        """Read state from dictionary

        Parameters
        ----------
        d : dict 
            Dictionary 
        """
        assert d['class'] == Bootstrap._fullname()

        self._nsim = d['nsim']
        super(Bootstrap,self).fromdict(d['base'])

#
# EOF
#
