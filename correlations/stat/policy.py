# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Policy:
    """Base class for policies of how to calculate the uncertainty.

    The image below shows the integrated flow estimated using the
    three available methods

    * :class:`correlations.stat.Derivatives` for full error
      propagation using the full covariance of variables.
    
    * :class:`correlations.stat.Jackknife` for uncertainty estimates
      using 10 subsamples and calculating the variance of these using
      a leave-one-out strategy.

    * :class:`correlations.stat.Bootstrap` for uncertainty estimates
      using 1000 samples from 10 sub-samples and calculating the
      variance over these 1000 samples.

    .. image:: ../static/gen_phi_medium_intg_uncr.png

    We note that the full error propagation gives the most reliable
    estimates of the uncertainty.  Furthermore, the space complexity
    is smaller or only slightly larger than the other two methods,
    while the time complexity (in so far that it matters since these
    algorithms are executed once) is generally much smaller.

    Parameters 
    ----------
    client : Estimator
        The client object that will use this.  If its class
        derives from estimator, then this is done automatic.

    """
    def __init__(self,client,covar,**kwargs):
        self._c = client
        self._s = self._west(covar,**kwargs)

    def _west(self,covar,**kwargs):
        """Generates a West statistics object 
        
        Parameters
        ----------
        covar : bool 
            Whether to use covariance 
        **kwargs: dict 
            Additional arguments 
        
        Returns 
        -------
        west : West 
            West object 
        """
        from nbi_stat import West
        
        freq    = kwargs.get('frequency',False)
        comp    = kwargs.get('component',True)
        return West(len(self._c),covar=covar,frequency=freq,component=comp)
    
    def __len__(self):
        """Size (in number of independent variables) of this estimator"""
        return len(self._c)


    def fill(self,x,w):
        """Fill in observation(s) of the independent variables 

        Parameters 
        ----------
        x : array 
            Independent variable(s) value(s) of observation 
        w : array 
            Weight(s) of observation 
        """        
        self._s.update(x,w)

    @classmethod
    def _fullname(cls):
        return cls.__module__+'.'+cls.__qualname__
    
    def todict(self):
        """Turn this into a dictionary"""
        from . io import west2dict
        return {'class': Policy._fullname(),
                's':     west2dict(self._s) }

    def fromdict(self,d):
        """Read state from dictionary

        Parameters
        ----------
        d : dict 
            Dictionary 
        """
        from . io import dict2west

        assert d['class'] == Policy._fullname()

        self._s = dict2west(d['s'])

    def merge(self,other):
        """Merge state from input 

        Parameters
        ----------
        inp : stream 
            Stream to merge state from 
        """
        assert isinstance(other,Policy)
        assert len(other._s) == len(self._s)
        
        self._s += other._s

    @property
    def mean(self):
        r"""Get the weighted means of each component 

        .. math::
        
            \overline{x_i}_w = \frac{\sum_j w_{ij}x_{ij}}{\sum_j w_{ij}}
        
        where :math:`x_{ij}` and :math:`w_{ij}` are the :math:`i` -th
        component value and weight in the :math:`j` -th observation,
        respectively.
        """
        return self._s.mean

    @property
    def var(self):
        r"""Get the (weighted) variances of each component"""
        return self._s.var

    @property
    def sem(self):
        r"""Get the **standard error on the mean** of each component"""
        return self._s.sem

    @property
    def std(self):
        r"""Get the standard deviations of the components"""
        return self._s.std
    
    @property
    def rho(self):
        r"""Correlation matrix given by 

        .. math::

            \rho_{ij} = 
            \frac{\mathrm{Cov}[x_i,x_j]_w}{
            \sqrt{\mathrm{Var}[x_i]_w\mathrm{Var}[x_j]_w}}
        """
        return self._s.rho

    def uncertainty(self,x,dx=None):
        """Calculates uncertainties on estimator value

        Parameters
        ----------
        x : array 
            Value of independent variables 
        dx : array (optional)
            Standard deviation of independent variables 
        
        Returns
        -------
        unc : scalar 
            Uncertainty on the estimator around x 
        """
        raise NotImplementedError('Uncertainty calculation not implemented')

    @classmethod
    def type(cls,name):
        lname = name.lower()
        if   'jack'  in lname: cn = 'Jackknife'
        elif 'deriv' in lname: cn = 'Derivatives'
        elif 'boot'  in lname: cn = 'Bootstrap'
        else:                  raise ValueError(f'Unknown policy {name}')

        from importlib import import_module

        return getattr(import_module('correlations.stat.'+cn.lower()),cn)

    @classmethod
    def make(cls,name,client,*args,**kwargs):
        cl = cls.type(name)
        return cl(client,*args,**kwargs)
        
#
# EOF
#

