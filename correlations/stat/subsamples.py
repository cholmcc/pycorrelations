# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from . policy import Policy

class SubSamples(Policy):
    """Initialize sub-sampling policy.  This is the base for either the
    Jackknife or Bootstrap methods


    Parameters 
    ----------
    client : Estimator
        The client object that will use this.  If its class
        derives from estimator, then this is done automatic.
    nsub : int 
        Number of sub-samples to use 
    rnd : numpy.random.Generator (optional) Random number
         generator.  If passed, then it will be used to select
         sub-samples at random.  If not passed, then the
         sub-samples are chosen in a round-robin fashion.
    kwargs : dict
         Additional arguments.

    """
    def __init__(self,client,nsub,rnd=None,**kwargs):
        super(SubSamples,self).__init__(client,False,**kwargs)

        self._sub = [self._west(False,**kwargs) for _ in range(nsub)]
        self._rnd = rnd 
        self._std = None
        self._ord = list(range(nsub))
        
    def uncertainty(self,x=None,dx=None):
        """Calculates uncertainties on estimator value

        This is done by simulating samples and calculating the
        uncertainty using the policies method (Jackknife or
        Bootstrap).

        Parameters
        ----------
        x : array 
            Value of independent variables 
        dx : array (optional)
            Standard deviation of independent variables 
        
        Returns
        -------
        unc : scalar 
            Uncertainty on the estimator around x

        """
        if self._std is None:
            self._std = self.simulate()

        return self._std

    def fill(self,x,w):
        """Fill in observation(s) of the independent variables 

        For each observation (rows of x and w), select a sub-sample
        and update that in addition to the master statistics.

        Parameters 
        ----------
        x : array 
            Independent variable(s) value(s) of observation 
        w : array 
            Weight(s) of observation

        """                
        from numpy import atleast_1d, ndim

        super(SubSamples,self).fill(x,w)
        
        self._std = None
        if ndim(x) == 1:
            sel = self.select(1)
            self._sub[sel[0]].update(x,w)
            return
            
        sel = atleast_1d(self.select(len(x)))
        for i,s in enumerate(self._sub):
            msk = sel == i
            if sum(msk) <= 0:
                continue
            s.update(x[msk],w[msk] if w is not None else None)

    def todict(self):
        """Turn this into a dictionary"""

        from . io import west2dict
        return {'class': SubSamples._fullname(),
                'base' : super(SubSamples,self).todict(),
                'sub':   {i: west2dict(self._s)
                          for i, s in enumerate(self._sub) } }
    
    def fromdict(self,d):
        """Read state from dictionary

        Parameters
        ----------
        d : dict 
            Dictionary 
        """
        from . io import dict2west
        assert d['class'] == SubSamples._fullname()
        
        super(SubSamples,self).fromdict(d['base'])
        for s, sd in zip(self._sub,d['sub']):
            w        = dict2west(d['sub'][sd])
            s._state = w._state
            s._var   = w._var

    def merge(self,other):
        """Merge state from input 

        Parameters
        ----------
        other : SubSamples
            Other to merge in
        """
        assert isinstance(other,SubSamples)
        assert len(self._sub) == len(other._sub)
        
        super(SubSamples,self).merge(self,other)
        for s, so in zip(self._sub,other._sub):
            s += so

    def select(self,n=1):
        """Select sub-sample(s) for updates etc. 

        Parameters 
        ----------
        n : int 
            Number of samples to select 
        
        Returns
        -------
        sel : array 
            Selected sub-samples 
        """
        if self._rnd is not None:
            return self._rnd.integers(0,len(self._sub),size=n)

        r = []
        while n > 0: 
            m =  min(len(self._ord),n) 
            r += self._ord[:m] 
            n -= m 
            self._ord[:] = self._ord[m:]+self._ord[:m] 
                  
        return r 

    def simulate(self):
        """Run simulation of the method and return uncertainty

        Derived classes must implement this method 

        Returns
        -------
        uncer : scalar 
            Estimate of the uncertainty on the estimator. 
        """
        raise NotImplementedError('Simulation method not defined')


#
# EOF
#
