# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Estimator:
    """Some estimator of a random variable.  

    This defines an interface for determining the value and
    uncertainty of some estimator.  The estimator may itself be a
    function of other random variables. The uncertainty is
    estimated according to the chosen policy

    :class:`Derivatives` : 
        Direct propagation of uncertainty.  In this case, the
        derived class must provide a method for calculating the
        derivatives or rely on a numerical approximation.
    
    :class:`Jackknife` :  
        Using the Jackknife prescription.  In this case, we
        resample the distribution of the (independent) variables
        and calculate the variance over this (re_sample.

    :class:`Bootstrap` : 
        Using the Bootstrap prescription. 

    At a minimum, a derived class must implement the method 

    :meth:`Estimator.value` : 
        Must return value of estimator given the independent
        variable values ``x``

    If the chosen policy is Derivatives, then in addition the
    following method can be implemented 

    :meth:`Estimator.derivatives` : 
        Must return the derivatives with respect to each
        independent variable (a vector of size __len__) of the
        estimator value at the point ``x``.  

    If this method is not defined and the Derivatives policy is
    chosen, then derivatives will be estimated numerically.

    Parameters
    ----------
    policy : Policy (Derivatives, Jackknife, or Bootstrap)
        Method for calculating statistics of independent variables
        and estimator.
    n : int 
        Number of independent variables used by this estimator 
    """
    def __init__(self,n,policy,**kwargs):
        self._nindep = n
        self._policy = policy(self,**kwargs)
    
    def fill(self,x,w):
        """Fill in an observation 

        Parameters 
        ----------
        x : array 
            Independent variable(s) value(s) of observation 
        w : array 
            Weight(s) of observation 
        """
        from numpy import ndim
        assert (ndim(x) == 2 and len(x[0]) == self._nindep) or \
            len(x) == self._nindep
        
        self._policy.fill(x,w)

    def __call__(self):
        """Get value and uncertainty on value of estimator 
        
        Returns 
        -------
        value : scalar 
            Value of estimator 
        uncer : scalar 
            Uncertainty on estimator value 
        """
        val = self.value(self._policy.mean)
        unc = self._policy.uncertainty(self._policy.mean)

        return (val,unc)

    @property 
    def nindep(self): return self._nindep

    @property
    def mean(self): return self._policy.mean
    
    def __len__(self):
        """Size (in number of independent variables) of this estimator
        
        Derived classes must overload this 
        
        Returns
        -------
        n : int 
            Number of independent variables 
        """
        return self._nindep

    def __str__(self):
        """String representation of this
        
        Returns 
        -------
        rep : str 
            String representation of this estimator value (and uncertainty) 
        """
        v, u = self.__call__()
        return f'{v} +/- {u}'

    @classmethod
    def _fullname(cls):
        return cls.__module__+'.'+cls.__qualname__
    
    def todict(self):
        """Create dictionary representation of this"""
        return {'class':  Estimator._fullname(),
                'nvar':   len(self),
                'policy': self._policy.todict()}

    def fromdict(self,d):
        """Read state from dictionary"""
        assert d['class'] == Estimator._fullname()
        self._nindep = d['nvar']
        self._policy.fromdict(d['policy'])

    def merge(self,other):
        """Merge state from input 

        Parameters
        ----------
        inp : stream 
            Stream to merge state from 
        """
        assert isinstance(other,Estimator)
        assert self._nindep == other._nindep
        
        self._policy.merge(other._policy)

    def value(self,x):
        """Calculate value of estimator given value(s) x of independent
        variable(s)

        Derived classes must implement this 

        Parameters 
        ----------
        means : array 
            Mean(s) of independent variable(s)

        Returns 
        -------
        val : scalar 
            The value of the estimator

        """
        raise NotImplemented('Value of estimator not defined')

    def derivatives(self,x,dx=None):
        """Calculate derivatives of estimator around x 

        Derived classes can implement this if they are to use the
        Derivatives method of calculating uncertainties.  The default
        implementation uses numeric differentiation around x with step
        size dx (which is mandatory in that case). 

        Parameters
        ----------
        x : array 
            Where to calculate the derivates 
        dx : array (optional)
            Step size in each independent variable 
        
        Returns
        -------
        df : array 
             Derivatives of estimator around x

        """
        from numpy import diag, array
        
        if dx is None or len(dx) != len(x):
            raise ValueError('Step size in each variable not given')
        
        ds = diag(dx)
        df = array([(self.value(x+d)-self.value(x-d))/(2*s) if s > 0 else 0
                    for d,s in zip(ds,dx)])

        return df
#
# EOF
#
