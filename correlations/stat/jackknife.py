# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from . subsamples import SubSamples

# --- Jackknife ------------------------------------------------------
class Jackknife(SubSamples):
    r"""Initialize Jackknife policy.

    Suppose we are interested in the quantity :math:`T` calculated
    over the data :math:`X` (more formally, :math:`T` is a
    *statistics*).  We estimate :math:`T` via the estimator
    :math:`\hat{T}` over the sample 

    .. math::
    
        X_1,X_2,\ldots X_N\quad, 

    of size :math:`N`. We are interested in estimating the
    variance :math:`\operatorname{Var}(T(X))`

    * First, estimate the :math:`T` over our sample :math:`X`
    * Secondly, for :math:`N` iterations, we do 
    
      * For the :math:`i^{\mathrm{th}}` iteration, calculate the
        estimate :math:`T` leaving out the :math:`i^{\mathrm{th}}`
        data point.  That is we take the sample
        :math:`X_1,\ldots,X_{i-1},X_{i+1},\ldots,X_N` and
        calculate the estimate on that sample.

    * Finally, calculate the variance of :math:`T` estimated over
      the math:`N` generated samples, given by

      .. math::
      
          \operatorname{Var}[T] = 
          \frac{N-1}{N}\sum_i^{N} \left(T_i - \bar{T}\right)^2\quad,
  
      where :math:`T_i` is the estimate calculated over the
      :math:`i^{\mathrm{th}}` jackknife sample and :math:`\hat{T}` is the
      mean of the estimate calculated over all jackknife samples.

    Parameters 
    ----------
    client : Estimator
        The client object that will use this.  If its class
        derives from estimator, then this is done automatic.
    nsub : int 
        Number of sub-samples to use  (:math:`N` above)
    rnd : numpy.random.Generator (optional) Random number
         generator.  If passed, then it will be used to select
         sub-samples at random.  If not passed, then the
         sub-samples are chosen in a round-robin fashion.
    kwargs : dict
         Additional arguments.

    """
    def __init__(self,client,nsub=10,rnd=None,seed=None,**kwargs):
        super(Jackknife,self).__init__(client,nsub,rnd=rnd,**kwargs)


    def simulate(self):
        """Run the simulations. 

        We perform NSUB simulations.  In the ith simulation, we
        calculate the mean of the estimator values over all sub-sample
        means _except_ the ith sub-sample.  We then calculate the
        uncertainty as the (scaled) standard deviation of those NSUB
        means we got from leaving one sub-sample out.
        
        Returns
        -------
        uncer : scalar 
            Estimate of the uncertainty on the estimator. 
        """
        from numpy import array, mean, eye, logical_not

        # Value of means of all sub-samples
        v = array([self._c.value(s.mean) for s in self._sub])

        # Means of all samples except one
        m = array([mean(v[e]) for e in logical_not(eye(len(v),dtype=bool))])

        fac = (len(self._sub)-1)
        return fac * m.std()

    def todict(self):
        """Turn this into a dictionary"""
        return {'class': Jackknife._fullname(),
                'base' : super(Jackknife,self).todict() }
    
    def fromdict(self,d):
        """Read state from dictionary

        Parameters
        ----------
        d : dict 
            Dictionary 
        """
        assert d['class'] == Jackknife._fullname()

        super(Jackknife,self).fromdict(d['base'])

#
# EOF
#
