# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .. stat import Estimator, Derivatives

# --- Integrated flow ------------------------------------------------
class VNM(Estimator):
    r"""Calculate integrated flow :math:`v_{n}\{m\}`

    Parameters
    ----------
    n : int 
        :math:`n` - Order of flow 
    m : int 
        :math:`m` - number of particles 
    """
    def __init__(self,n,m,policy=Derivatives):
        super(VNM,self).__init__(m//2,policy)
        self._n = n
        self._m = m
        self._cnm = None
        self._dnm = None

    @property
    def m(self):
        """The number of particles :math:`m` to correlate for flow
        (:math:`v_n\{m\}`)"""
        return self._m

    @property
    def n(self):
        """The harmonic number :math:`n` of flow (:math:`v_n\{m\}`)"""
        return self._n
    
    def value(self,means):
        r"""Calculate the value of :math:`v_n\{m\}`. 
        
        .. math::

            v_n\{2k\} 
            = \sqrt[{\textstyle 2k}]{(-1)^{k-1}\frac{1}{M_k}c_{n}\{2k\}}
            \quad,

        
        where :math:`m=2k`, and 
        
        .. math::

            c_{n}\{2k\} = f(\overline{C}_{\mathbf{n}}\{2k\},\ldots,
               \overline{C}_{\mathbf{n}}\{2\})\quad.

        See Also
        --------

        * :func:`correlations.flow.flowexpr.vnm`
        * :func:`correlations.flow.cumexpr.cnm`
        
        Parameters
        ----------
        means : array 

            Mean values of correlators
            :math:`\overline{C}_{\mathbf{n}}\{2j\}` for
            :math:`j=1,\ldots,k` and :math:`m=2k`
        
        Returns
        --------
        vnm : float
            :math:`v_n\{m\}`

        """
        from . cumexpr import cnm
        from . flowexpr import vnm

        self._cnm = cnm(means)
        self._vnm = vnm(self._m, self._cnm)
        return self._vnm

    def derivatives(self,means,dx):
        r"""Get the derivatives of :math:`v_n\{m\}` with respect to the
        correlators used.
        
        .. math::

            \frac{\partial v_n\{2k\}}{
                \partial\overline{C}_{\mathbf{n}}\{2j}\quad,
        
        
        for :math:`m=2k` and :math:`j=1,\ldots k`. 
        
        This member function then returns 
        
        .. math::

            \nabla v_{n}\{2k\} &=
            \begin{bmatrix}
            \frac{\partial v_n\{2k\}}{\partial\overline{C}_{\mathbf{n}}\{2k\}}\\
            \ldots 
            \frac{\partial v_n\{2k\}}{\partial\overline{C}_{\mathbf{n}}\{2\}}\\
            \end{bmatrix}\\
            &= \frac{\mathrm{d} v_n\{2k\}}{\mathrm{d} c_{n}\{2k\}}
            \begin{bmatrix}
            \frac{\partial c_n\{2k\}}{\partial\overline{C}_{\mathbf{n}}\{2k\}}\\
            \ldots 
            \frac{\partial c_n\{2k\}}{\partial\overline{C}_{\mathbf{n}}\{2\}}\\
            \end{bmatrix}
        
        See Also 
        --------
        
        * :func:`correlations.flow.flowexpr.dvnm`  
          Derivative of :math:`v_{n}\{m\}` 
        * :func:`correlations.flow.cumexpr.gcnm`
          Gradient of :math:`c_{n}\{m\}`
        
        Parameters
        ----------
        means : array 

            Mean values of correlators
            :math:`\overline{C}_{\mathbf{n}}\{2j\}` for
            :math:`j=1,\ldots,k` and :math:`m=2k`
        
        Returns
        -------
        deriv : array 
            Derivatives (Gradient) of :math:`v_n\{m\}` wrt. correlators

        """        
        from . cumexpr import gcnm
        from . flowexpr import dvnm

        if self._vnm is None:
            self.value(means)
            
        gcnm_ = gcnm(means)
        dvnm_ = dvnm(self._m, self._vnm, self._cnm)
        return dvnm_ * gcnm_ 
        
    def update(self,c):
        r"""Event update.
        
        This method expects one vector of the integrated m-particle
        correlators in descending order.

        .. math::

            c = \{C_{\mathbf{n}}\{2k\},
                  C_{\mathbf{n}}\{2(k-i)\},
                  \ldots,C_{\mathbf{n}}\{2\}\}\\
        
        for :math:`m = 2k` and :math:`i=1,\ldots,k-1`

        Parameters
        ----------
        c : array 
             Integrated correlator :math:`C_{\mathbf{n}}`
        """
        from numpy import array
        assert len(c) == len(self._policy),\
            f'Inconistent size of correlator {len(c)}, expected {self._m}'

        v = array([cc.real for cc in c])
        w = array([cc.weight for cc in c])
        # print(f'VNM {self._n}{{{self._m}}}',v)
        self._update(v,w)

    def _update(self,v,w):
        self.fill(v,w)
        self._cnm = None
        self._dnm = None

    def todict(self):
        """Turn this into a dictionary"""
        return {'class': VNM._fullname(),
                'n': self.n,
                'm': self.m,
                'e': super(VNM,self).todict() }


    def fromdict(self,d):
        """Read state from dictionary

        Parameters
        ----------
        d : dict 
            Dictionary 
        """
        assert d['class'] == VNM._fullname()
        self._n = d['n']
        self._m = d['m']
        super(VNM,self).fromdict(d['e'])

    def merge(self,other):
        """Merge state from input 

        Parameters
        ----------
        other : VPNM
            Object to merge from 
        """
        assert isinstance(other,VNM)
        assert self._n == other._n
        assert self._m == other._m

        super(VNM,self).merge(other)
        
        
# --- Differential flow ----------------------------------------------        
class VPNM(VNM):
    r"""Calculate integrated flow :math:`v'_{n}\{m\}`

    Parameters
    ----------
    n : int 
        :math:`n` - Order of flow 
    m : int 
        :math:`m` - number of particles 
    """
    def __init__(self,n,m,policy=Derivatives):
        super(VPNM,self).__init__(n,2*m,policy)
        self._m    = m
        self._dnm  = None
        self._vpnm = None
        

    def update(self,dc):
        r"""This method expects two concatenated vectors of the differential 
        and integrated m-particle correlators in descending order.
        
        .. math::

            d &= \{C'_{\mathbf{n}}\{2k\},
                   C'_{\mathbf{n}}\{2(k-i)\},
                   \ldots,C'_{\mathbf{n}}\{2\}\}\\
            c &= \{C_{\mathbf{n}}\{2k\},
                   C_{\mathbf{n}}\{2(k-i)\},
                   \ldots,C_{\mathbf{n}}\{2\}\}\\

        
        for :math:`m = 2k` and :math:`i=1,\ldots,k-1`
        
        Parameters
        ----------
        dc : 
            Differential correlators :math:`C'_{\mathbf{n}}`
            concatenated with integrated correlator
            :math:`C_{\mathbf{n}}`

        """
        super(VPNM,self).update(dc)
        
    def value(self,means):
        r"""Calculate the value of :math:`v'_n\{m\}`. 
        
        .. math::

            v_n'\{m\} = \frac{d_n\{m\}}{c_n\{m\}}v_n\{m\}\quad,
        
        where, with :math:`m=2k`, 
        
        .. math::

            v_n\{2k\} 
            = \sqrt[{\textstyle 2k}]{(-1)^{k-1}\frac{1}{M_k}c_{n}\{2k\}}
            \quad,

        and 

        .. math::

            c_{n}\{2k\} &= f(\overline{C}_{\mathbf{n}}\{2k\},\ldots,
                            \overline{C}_{\mathbf{n}}\{2\})\\ 
            d_{n}\{2k\} &= f(\overline{C'}_{\mathbf{n}}\{2k\},\ldots,
                             \overline{C'}_{\mathbf{n}}\{2\},
                             \overline{C}_{\mathbf{n}}\{2(k-1)\},\ldots,
                             \overline{C}_{\mathbf{n}}\{2\})\\ 
        
        See also
        --------

        * :func:`correlations.stat.cumexpr.dnm` 
        * :func:`correlations.stat.flowexpr.vpnm`
        
        Parameters
        ----------
        means : 
            Mean values of correlators 
            :math:`\overline{C'}_{\mathbf{n}}\{2j\}` and 
            :math:`\overline{C}_{\mathbf{n}}\{2j\}` for 
            :math:`j=k,\ldots,1` and :math:`m=2k` 
        
        Returns
        --------
        :math:`v_n\{m\}` 
        """
        from . cumexpr import dnm
        from . flowexpr import vpnm
        
        p = self.nindep // 2  # Pivot
        d = means[:p]
        c = means[p:]
        
        super(VPNM,self).value(c)
        
        self._dnm  = dnm(d,c[1:])
        self._vpnm = vpnm(self._m, self._vnm, self._dnm, self._cnm);

        return self._vpnm;

    def derivatives(self,means,dx):
        r"""Get the derivatives of :math:`v'_n\{m\}` with respect to the
        correlators used.
        
        .. math::

            &\frac{\partial v'_n\{2k\}}{\partial\overline{C'}_{\mathbf{n}}\{2j}
            \\
            &\frac{\partial v'_n\{2k\}}{\partial\overline{C'}_{\mathbf{n}}\{2l}
            \quad,
        
        for :math:`m=2k` and :math:`j=1,\ldots k` and :math:`l=1,\ldots,k-1`.
        
        This member function then returns 
        
        .. math::

            \nabla v_{n}\{2k\} &=& 
            \begin{bmatrix}
            \frac{\partial v'_n\{2k\}}{
                  \partial\overline{C'}_{\mathbf{n}}\{2k\}}\\
            \ldots \\
            \frac{\partial v'_n\{2k\}}{
                  \partial\overline{C'}_{\mathbf{n}}\{2\}}\\
            \frac{\partial v'_n\{2k\}}{
                  \partial\overline{C}_{\mathbf{n}}\{2k)\}}\\
            \ldots \\
            \frac{\partial v'_n\{2k\}}{
                  \partial\overline{C}_{\mathbf{n}}\{2\}}\\
            \end{bmatrix}\\
            &=& 
            \begin{bmatrix}
            \frac{\partial v'_n\{2k\}}{\partial d_n\{2k\}}
            \frac{\partial d_n\{2k\}}{\partial\overline{C'}_\mathbf{n}\{2k\}}\\
            \ldots\\
            \frac{\partial v'_n\{2k\}}{
                  \partial d_n\{2k\}}
            \frac{\partial d_n\{2k\}}{
                  \partial\overline{C'}_\mathbf{n}\{2\}}\\
            \frac{\partial v'_n\{2k\}}{\partial c_n\{2k\}}
            \frac{\partial d_n\{2k\}}{
                  \partial\overline{C}_\mathbf{n}\{2k\}}\\
            \frac{\partial v'_n\{2k\}}{\partial d_n\{2k\}}
            \frac{\partial d_n\{2k\}}{
                  \partial\overline{C}_\mathbf{n}\{2(k-1)\}}
            +
            \frac{\partial v'_n\{2k\}}{\partial c_n\{2k\}}
            \frac{\partial c_n\{2k\}}{
                  \partial\overline{C}_\mathbf{n}\{2(k-1)\}}\\
            \ldots\\
            \frac{\partial v'_n\{2k\}}{\partial d_n\{2k\}}
            \frac{\partial d_n\{2k\}}{\partial\overline{C}_\mathbf{n}\{2\}}
            +
            \frac{\partial v'_n\{2k\}}{\partial c_n\{2k\}}
            \frac{\partial c_n\{2k\}}{\partial\overline{C}_\mathbf{n}\{2\}}
            \end{bmatrix}
            
        See also
        --------

        * :func:`correlations.flow.cumexpr.gdnm`  
          Gradient of :math:`c_{n}\{m\}`
        * :func:`correlations.flow.flowexpr.dvpnm`  
          Derivative of :math:`v_{n}\{m\}` 
        
        Parameters
        ----------
        means : array
            Mean values of correlators
            :math:`\overline{C'}_{\mathbf{n}}\{2j\}` and
            :math:`\overline{C}_{\mathbf{n}}\{2j\}` for
            :math:`j=k,\ldots,1` and :math:`m=2k`
        
        Returns
        --------
        Derivatives (Gradient) of :math:`v'_n\{m\}` wrt. correlators
        """
        from . cumexpr import gdnm, gcnm
        from . flowexpr import dvpnm
        from numpy import array

        p = self.nindep//2  # Pivot
        d = means[:p]
        c = means[p:]

        super(VPNM,self).value(c)

        dvpnm_ = dvpnm(self._m,self._vpnm,self._vnm,self._dnm,self._cnm)
        gdnm_  = gdnm(d,c[1:])
        gcnm_  = gcnm(c)

        g = [dvpnm_[0] * gdnm_[k] for k in range(p)] + \
            [dvpnm_[1] * gcnm_[k]
             + ((dvpnm_[0] * gdnm_[p+k-1]) if k > 0 else 0)
             for k in range(p)]

        return array(g);

    def todict(self):
        """Turn this into a dictionary"""
        return {'class': VPNM._fullname(),
                'v' : super(VPNM,self).todict() }

    def fromdict(self,d):
        """Read state from dictionary

        Parameters
        ----------
        d : dict 
            Dictionary 
        """
        assert d['class'] == VPNM._fullname()

        super(VPNM,self).fromdict(d['v'])

    def merge(self,other):
        """Merge state from input 

        Parameters
        ----------
        other : VPNM
            Object to merge from 
        """
        assert isinstance(other,VPNM)
        
        
#
# EOF
#
