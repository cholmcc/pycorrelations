# Flow sub-package 

Here is code for calculating flow using the generic framework
methods. 

## Flow expression 

The module [`flowexpr`](flowexpr.py) contains various formulas for
calculating various parts needed to do flow from multi particle
correlations. 

Binomial coefficient

```math
\binom{n}{k} = \frac{n!}{(n-k)!k!}
```

Flow normalisation factor 
```math
M_k = \begin{cases}
1 & k=1\\
\sum_{i=1}^{k-1}\binom{k}{i}\binom{k}{k-i}M_iM_{k-i} & k>1
\end{cases}
```

Integrated flow using $`m=2k`$ particles from the cumulant $`c_n\{m\}`$

```math
v_n\{m\} =\sqrt[2k]{(-1)^{k-1}\frac{1}{M_k}c_n\{2k\}}\quad m=2k
```

Derivative of integrated flow using $`m=2k`$ particles with respect to
the cumulant $`c_n\{m\}`$

```math
\frac{\partial v_n\{m\}}{\partial c_n\{m\}}
= \frac{1}{m c_n\{m\}}v_n\{m\}
```

Differential flow using $`m=2k`$ particles from the differential

$`d_n\{m\}`$ and integrated $`c_n\{m\}`$ cumulants
```math
v'_n\{m\} = \frac{d_n\{m\}}{c_n\{m\}}
```

Derivative of the differential flow using $`m=2k`$ particles with
respect to the differential $`d_n\{m\}`$ and integrated $`c_n\{m\}`$
cumulants 

```math
\frac{\partial v'_n\{m\}}{\partial c_n{m}}
=-\frac{v'_n\{m\}}{c_n\{m\}}+\frac{d_n\{m\}}{c_n\{m\}}
\frac{\partial v_n\{m\}}{\partial c_n\{m\}}
```

```math
\frac{\partial v'_n\{m\}}{\partial d_n{m}}
=\frac{1}{c_n\{m\}}v_n\{m\}
```

## Cumulant expression 

The module [`cumexpr`](cumexpr.py) contains various formulas for
calculating various parts needed to do flow from multi particle
correlations.

Binomial cumulant factor 

```math
f(n,k) = \binom{n}{k} \binom{n-1}{k}
```

Generic expression for the $`m=2k`$ cumulant with harmonics
$`\mathbf{n}`$ from the average $`m`$-particle correlators $`C'`$
(differential) and $`C`$ (integrated). 

```math
\gamma_{\mathbf{n}}\{2k\}(C',C) = 
\begin{cases}
\overline{C'}_{\mathbf{n}}\{2k\} 
- \sum_{p=1}^{k-1}f(k,p)\gamma_{\mathbf{n}}\{2(k-p)\}(C',C)
\overline{C}_{\mathbf{n}}\{2(k-p)\} & k>1\\
\overline{C'}_{\mathbf{n}}\{2\} & k=1
\end{cases}
```

By inserting the average integrated $`m`$ particle correlators for
$`C'`$ we recover the integrated cumulant, 

```math
c_{\mathbf{n}}\{m\} = \gamma_{\mathbf{n}}\{m\}(C,C)
```

The differential cumulant is found with the average $`m`$-particle
diferential and integrated correlators.

```math
d_{\mathbf{n}}\{m\} = \gamma_{\mathbf{n}}\{m\}(C',C)
```

We can calculate the derivatives of the above with respect to the
average correlators. 

```math
\frac{\partial\gamma_{\mathbf{n}}\{m\}(C',C)}{\partial C'_{\mathbf{n}}\{o\}}
= -\sum_{p=1}^{k-1}f(k,p)
\frac{\partial\gamma_{\mathbf{n}}\{2(k-p)\}(C',C)}{
	\partial C'_{\mathbf{n}}\{o-2p\}}
\overline{C}_{\mathbf{n}}\{2(k-p)\}+\begin{cases} 1 & o=2\\0\end{cases}
```

```math
\frac{\partial\gamma_{\mathbf{n}}\{m\}(C',C)}{\partial C_{\mathbf{n}}\{o\}}
= -\sum_{p=1}^{k-1}f(k,p)
\frac{\partial\gamma_{\mathbf{n}}\{2(k-p)\}(C',C)}{\partial 
	\partial C_{\mathbf{n}}\{o-2p\}}
\overline{C}_{\mathbf{n}}\{2(k-p)\}
+\begin{cases} c_{\mathbf{n}}\{2(k-p)\} & o==2(k-p)\\ 0\end{cases}
+\begin{cases} 1 & o=2\\0\end{cases}
```

Gradients of cumulants 
```math
\nabla c_{\mathbf{n}}\{m\}\quad\nabla d_{\mathbf{n}}\{m\}
```

## Flow calculations 

The two classes [`correlations.flow.VNM`](vnm.py) and
[`correlations.flow.VPNM`] calculates $`v_n\{m\}`$ and $`v'_n\{m\}`$,
respectively, over an ensemble of observations. 

- At the beginning of an event, the internal $`Q`$ vectors are
  zeroed. 
- Observations are filled into the $`Q`$-vector 
- After each event, the classes calculates the relevant correlators 
  $`C'_{\mathrm{n}}`$ or $`C_{\mathrm{n}}`$ and adds these to the
  average and covariance using _West's_ algorithm. 
- At the end of the analysis, relevant cumulants $`d_n\{m\}`$ or
  $`c_n\{m\}` are calculated, and from this, the flow results
  $`v'_n\{m\}`$ or $`v_n\{m\}`$.
- Depending on the choice of uncertainty policy, the classes also
  calculates the uncertainty on the flow results using either full
  uncertainty propagation via derivatives and covariances, or
  Jackknife or Bootstrap simulations. 
  
## Flow bins 

The classes [`correlations.flow.Integrated`](bin.py) and
[`correlations.flow.Differential`](bin.py) stores objects of
[`correlations.flow.VNM`](vnm.py) and
[`correlations.flow.VPNM`](vnm.py), respectively, as well as the
relevant $`Q`$-vectors.  These classes provide a high-level interface
for analyses of flow, in that proper treatment of correlators,
observations, etc. are managed by these. 
  
## Status 

- [x] [Flow expressions](flowexpr.py)
- [x] [Cumulants expression](cumexpr.py)
- [x] [Integrated and differential flow containers](vnm.py)
- [x] [Bins of flow](bin.py) 
- [/] [Flow in partitions](bin.py) 



