# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Various expressions used for calculating flow"""

def binom(n, k):
    r"""Calculate the binomial coefficent.

    .. math::

       \begin{pmatrix} n\\k\end{pmatrix} = \frac{n!}{(n-k)!k!}\quad.
    
    Since we will only use this for realitively small values of
    :math:`n` (and hence :math:`k`) we do not use any approximation
    for large numbers (e.g., via a :math:`\beta` function), and we
    do integer arithmetic.

    Alternatively, we could use scipy.special.binom 
    
    Parameters
    ----------
    n : int 
        :math:`n` 
    k : int 
        :math:`k` 
    
    Returns
    -------
    binom : int 
        :math:`\begin{pmatrix} n\\k\end{pmatrix}`
    """
    if k == 0:   return 1
    if k > n//2: return binom(n,n-k)
    return n * binom(n-1,k-1) // k

def mk(k):
    r"""Return the factor :math:`M_k` where :math:`m=2k`, from the
    sequence
    
    .. math::

        M=\{1,1,4,33,456,9460,274800,10643745,\ldots\}\quad,
    
    via 
    
    .. math::

        M_k &=& \sum_{i=1}^{k-1}\binom{k}{i}\binom{k}{k-i}M_{i}M_{k-i}\\
        M_1 &=& 1\quad.
  
    
    See also https://oeis.org/A002190
    
    On a 64-bit machine (most these days), the maximum :math:`k` is
    14, due to limited range of the int type.
    
    Parameters
    ----------
    k : int 
        :math:`k`
    
    Returns
    -------
    mk : int 
        :math:`M_k`
    """
    m = [1]
    for n in range(1,k):
        m.append(sum([binom(n,i)*binom(n,n-i-1)*m[i]*m[n-i-1]
                      for i in range(n)]))
    return m[k-1];

def vnm(m,cnm):
    r"""Calculate :math:`v_n\{m\}` as a function of to :math:`c_n\{m\}` 
    
    This calculates 
    
    .. math::
    
        v_n\{2k\} = 
        \sqrt[{\textstyle 2k}]{(-1)^{k-1}\frac{1}{M_k}c_{n}\{2k\}}
        \quad,
    
    
    where :math:`m = 2k` and :math:`M_k` is the :math:`k`-th element
    of the sequence
    
    .. math::
    
        M=\{1,1,4,33,456,9460,274800,10643745,\ldots\}\quad.
    
    Note, 
    
    * If :math:`k` is even (correspnding to :math:`m=4,8,12,\ldots`)
      and :math:`c_n\{m\}>0` then there is no real solutions, and we
      return 0.
    
    * Similarly, if :math:`k` is odd (corresponding to
      :math:`m=2,6,10,\ldots`) and :math:`c_n\{m\}<0` then there is
      also no real soltions and we return 0.

    Parameters
    ----------
    m : int 
        :math:`m` 
    cnm : complex 
        :math:`c_n\{m\}` 
    
    Returns
    -------
    vnm : float 
        :math:`v_n\{m\}`
    """
    k = m // 2
    s = 1 if k % 2 == 1 else -1
    i = s * cnm / mk(k)
    if i < 1e-9:
        return 0

    return i**(1/m)

def dvnm(m,vnm,cnm):
    r"""The deriviative of :math:`v_n\{m\}` with respeet to :math:`c_n\{m\}` 
    
    .. math::
    
         \frac{\partial v_n\{2k\}}{\partial c_n\{2k\}} 
         = \frac{1}{2k\,c_n\{2k\}}\sqrt[2k]{(-1)^{k+1}m_k c_n\{2k\}}
         = \frac{1}{m\,c_n\{m\}}v_n\{m\}\quad,
    
    
    where :math:`m=2k`, thus allowing to evaluate 
    
    .. math::
    
         \frac{\partial v_n\{m\}(c_n\{m\}(x))}{\partial x}
         = \frac{\partial v_n\{m\}}{\partial c_n\{m\}}
         \frac{\partial c_n\{m\}}{\partial x}\quad,
    
    
    when, for example, we a propagating uncertainties. 
    
    Parameters 
    ----------
    m   : int 
        :math:`m` 
    vnm : float 
        :math:`v_n\{m\}`
    cnm : float 
        :math:`c_n\{m\}` 
    
    Returns
    -------
    dvnm : float
        :math:`\frac{\partial v_n\{m\}}{\partial c_n\{m\}}`
    """
    return vnm / (m * cnm)

def vpnm(m,vnm,dnm,cnm):
    r"""Calculate :math:`v'_{n}\{m\}` as a function of :math:`d_{n}\{m\}` and 
    :math:`c_{n}\{m\}`
    
    .. math::
    
         v'_n\{2k\} = 
         \frac{1}{M_k^{1/(2k)}}
         \frac{(-1)^{k-1}d_n\{2k\}}
         {\left[(-1)^{k-1}c_{n}\{2k\}\right]^{1-1/(2k)}}
         = \frac{d_n\{m\}}{c_{n}\{m\}}v_n\{m\}\quad,
    
    
    where :math:`m = 2k` and :math:`M_k` is the :math:`k`-th element
    of the sequence
    
    .. math::
    
        M=\{1,1,4,33,456,9460,274800,10643745,\ldots\}\quad.
    
    Note, 
    
    * If :math:`|c_n\{m\}| <\epsilon`, we return 0
    
    * If :math:`k` is even (correspnding to :math:`m=4,8,12,\ldots`)
      and :math:`c_n\{m\}>0` then there is no real solutions, and we
      return 0.
    
    * Similarly, if :math:`k` is odd (corresponding to
      :math:`m=2,6,10,\ldots`) and :math:`c_n\{m\}<0` then there is
      also no real solutions and we return 0.
    
    Parameters
    ----------
    vnm  : float 
         :math:`v_{n}\{m\}` 
    dnm  : float 
         Differential cumulant :math:`d_n\{m\}` 
    cnm  : float 
         Cumulant :math:`c_n\{m\}` 
    
    Returns
    -------
    vpnm : float
        :math:`v'_n\{m\}`
    """
    if abs(cnm) < 1e-9 or vnm < 1e-9:
        return 0

    return dnm / cnm * vnm

def dvpnm(m,vpnm,vnm,dnm,cnm):
    r"""Calculcate the derivative of :math:`v'_{n}\{m\}` with respect to 
    :math:`d_{n}\{m\}` and :math:`c_{n}\{m\}`. 
    
    .. math::

        \frac{\partial v'_n\{m\}}{\partial d_n\{m\}} 
        &=& \frac{1}{c_{n}\{m\}}v_n\{m\}\\
        \frac{\partial v'_n\{m\}}{\partial c_n\{m\}}
        &=& 
        -\frac{v'_n\{m\}}{c_n\{m\}}+\frac{d_n\{m\}}{c_n\{m\}}
        \frac{\partial v_n\{m\}}{\partial c_n\{m\}}

    
    thus allowing to evaluate 
    
    .. math::

        \frac{\partial v'_n\{m\}(d_n\{m\}(x))}{\partial x}
        = \frac{\partial v'_n\{m\}}{\partial d_n\{m\}}
        \frac{\partial d_n\{m\}}{\partial x}\quad,
    
    and similarly for :math:`c_n\{m\}`, when, for example, we a
    propagating uncertainties.

    Parameters
    ----------
    m    : int 
        :math:`m` 
    vpnm : float 
        :math:`v'_{n}\{m\}` 
    vnm  : float 
        :math:`v_{n}\{m\}` 
    dnm  : float
        :math:`d_{n}\{m\}` 
    cnm  : float 
        :math:`c_{n}\{m\}` 
    
    Returns
    -------
    dvpnm : float 
    
        .. math::

            \begin{bmatrix}
            \partial v'_n\{m\}/\partial d_n\{m\}\\
            \partial v'_n\{m\}/\partial c_n\{m\}
            \end{bmatrix}
    """
    dvnm_ = dvnm(m,vnm,cnm);
    return [ vnm / cnm,
	     -vpnm / cnm + dnm / cnm * dvnm_ ]

#
# EOF
#


