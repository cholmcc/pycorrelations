# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Expressions for cumulants"""

from . flowexpr import binom

def binom_factor(n,k):
    r"""Return the binomial factor.
    
    .. math::
    
        \begin{pmatrix} n\\k\end{pmatrix}
        \begin{pmatrix} n-1\\k\end{pmatrix}\quad,
    
    as used in the cumulant expression 
    
    Parameters
    ----------
    n : int
        :math:`n`
    k : int
        :math:`k`
    
    Returns
    -------
    factor : int 
        binomial factor 
    """
    return binom(n,k) * binom(n-1,k)

def cumulant(d,c):
    r"""Calculate the :math:`m`-particle cumulant (both integrated and
    differential).
    
    .. math::
    
        d_n\{2k\} &= 
          \overline{C'}_{\mathbf{n}}\{2k\} - 
            \sum_{p=1}^{k-1}\begin{pmatrix} k\\p\end{pmatrix}
                            \begin{pmatrix} k-1\\p\end{pmatrix}
               d_n\{2(k-1)\}\overline{C}_{\mathbf{n}}\{2(k-p)\}\\
        d_n\{2\} &= \overline{C'}_{\mathbf{n}}\{2\}\quad,
       
    
    This method expects iterators over two vectors of average
    multi-particle correlators in decending order
    
    .. math::
    
        
         d &= \left\{\overline{C'}_{\mathbf{n}}\{2k\},
                     \overline{C'}_{\mathbf{n}}\{2(k-1)\},
                     \ldots
                     \overline{C'}_{\mathbf{n}}\{2\}\right\}\\
         c &= \left\{\overline{C}_{\mathbf{n}}\{2(k-1)\},
                     \ldots
                     \overline{C}_{\mathbf{n}}\{2\}\right\}\quad,\\
        
    
    with :math:`m=2k`, and where :math:`c` is one element shorter
    then :math:`d`.  If
    
    .. math::
    
        \overline{C'}_{\mathbf{n}}\{2(k-i)\}
        =\overline{C}_{\mathbf{n}}\{2(k-i)\}\quad,
    
    for all :math:`i`, then we are calculating the integrated
    cumulant, otherwise we calculate the differential cumulant. 
    
    Note, these assumptions are _not_ checked. 
    
    Parameters
    ----------
    d : array
        :math:`d` 
    c : array
        :math:`c` 
    
    Returns
    -------
    The cumulant 
    """
    k = len(d)
    t = sum([binom_factor(k,p) * cumulant(d[p:],c[p:])*c[-p]
             for p in range(1,k)])
    return d[0] - t

def cnm(c):
    r"""Calculates the integrated cumulant from the average
    multi-particle correlators given in descending order.
    
    .. math::
        
        c = \left\{\overline{C}_{\mathbf{n}}\{2k\},
                   \overline{C}_{\mathbf{n}}\{2(k-1)\},
                   \ldots
                   \overline{C}_{\mathbf{n}}\{2\}\right\}\quad.
        
    
    with :math:`m=2k`, as 
    
    .. math::
        
        c_n\{2n\} &=& \overline{C}_{\mathbf{n}}\{2n\} - 
             \sum_{k=1}^{n-1} \begin{pmatrix} n\\k\end{pmatrix}
                              \begin{pmatrix} n-1\\k\end{pmatrix}
                 c_n\{2(n-1)\}\overline{C}_{\mathbf{n}}\{2(n-k)\}\\
        c_n\{2\} &=& \overline{C}_{\mathbf{n}}\{2\}\quad.\\
        
    
    Parameters
    ----------
    c : array
        Vector :math:`c` of average multiparticle correlators 
    
    Returns
    -------
    cnm : complex
        :math:`c_n\{m\}`
    """
    return cumulant(c,c[1:])

def dnm(d,c):
    r"""Calculates the differential cumulant from the average
    multi-particle correlators given in descending order.
    
    .. math::
    
         d &=& \left\{\overline{C'}_{\mathbf{n}}\{2k\},
                     \overline{C'}_{\mathbf{n}}\{2(k-1)\},
                     \ldots
                     \overline{C'}_{\mathbf{n}}\{2\}\right\}\quad.\\
         c &=& \left\{\overline{C}_{\mathbf{n}}\{2(k-1)\},
                     \ldots
                     \overline{C}_{\mathbf{n}}\{2\}\right\}\quad.\\
        
    
    with :math:`m=2k`, as 
    
    .. math::
        
        d_n\{2n\} &=& \overline{C'}_{\mathbf{n}}\{2n\} - 
          \sum_{k=1}^{n-1}\begin{pmatrix} n\\k\end{pmatrix}
                          \begin{pmatrix} n-1\\k\end{pmatrix}
          d_n\{2(n-1)\}\overline{C}_{\mathbf{n}}\{2(n-k)\}\\
        d_n\{2\} &=& \overline{C'}_{\mathbf{n}}\{2\}\quad.
        
    
    Parameters
    ----------
    d : array
        Vector :math:`d` of average differential multi-particle 
        correlators  
    c : array
        Vector :math:`c` of average integrated multi-particle 
        correlators 
    
    Returns
    -------
    :math:`d_n\{m\}`
    """
    if c is None:
        m = len(dc)
        n = m//2+1
        return cumulant(d[:n],c[n:])
    
    return cumulant(d,c)

def dcumulant(d,c,di,ci):
    r"""Calculate the differential of a cumulant with resect to one
    average correlator.
    
    .. math::
    
         \frac{\partial d_n\{m\}}
             {\partial\overline{C'}_{\mathrm{n}}\{m\}}\quad.
    
    See the method `cumulant` for more on the arguments and formats. 
    
    Parameters
    ----------
    d : array
        :math:`d` 
    c : array
        :math:`c` 
    di : int
        Index into :math:`d` pointing to the correlator 
              to diffentiate relative to. If negative, assume
              that @a ci is a valid index into :math:`c`.
    ci : int
        Index into :math:`c` point to the correlator to 
        differentiate relative to
    
    Returns
    -------
    The derivative of the cumulant with respect to the
    chosen average correlator.
    """
    if di < 0 and ci < 1: return 0
    k = len(d)
    t = sum([binom_factor(k,p)
             * dcumulant(d[p:],c[p:],di-p,ci-p) * c[-p]
             + (cumulant(d[p:],c[p:]) if ci == k-p else 0)
             for p in range(1,k)])

    if di == 0 or ci == 0:
        return 1 - t

    return -1 * t

def dcnm(c,i):
    r"""Calculates the partial derivative of the integrated cumulant
    with respect to an average multi-particle correlator.
    
    .. math::
    
         \frac{\partial c_n\{2k\}}
                     {\partial\overline{C}_{\mathrm{n}}\{2(k-i)\}}
    
    See method `cnm` for more on arguments. 
    
    Parameters
    ----------
    c : array
        Vector :math:`c` of average multiparticle correlators 
    i : int
        Index :math:`i` into :math:`c` pointing to the average 
        correlator to differentian with respect to 
    
    Returns
    -------
    dcnm : float
        .. math::
        
            \frac{\partial c_n\{2k\}}
                {\partial\overline{C}_{\mathrm{n}}\{2(k-i)\}}
    """
    return dcumulant(c,c[1:],i,i)

def gcnm(c):
    r"""Calculate the gradient of the cumulant with respect to the
    average multi-particle correlators
    """
    from numpy import array
    return array([dcnm(c,i) for i in range(len(c))])

def ddnm(d,c,i,j=-2):
    r"""Calculates the partial derivative of the integrated cumulant
    with respect to an average multi-particle correlator.
    
    .. math::
            
        & \frac{\partial d_n\{2k\}}
             {\partial\overline{C'}_{\mathrm{n}}\{2(k-i)\}}&\\
        & \frac{\partial c_n\{2k\}}
             {\partial\overline{C}_{\mathrm{n}}\{2(k-j)\}}&\\
        
    
    See method `dnm` for more on arguments.  Note, to take the
    partial derivatives with respect to 
    :math:`\overline{C}_{\mathbf{n}}\{2(k-j)\}` one must pass -1 for
    i
    
    Parameters
    ----------
    d : array
        Vector :math:`d` of average differential multi-particle
        correlators.  If c is None, assume that this argument also
        contains the integrated multi-particle correlators.
    c : array
        Vector :math:`c` of average integrated multi-particle 
        correlators. If None, assume d also contains these
    di : int
        Index :math:`i` into :math:`d` pointing to the average 
        correlator to differentian with respect to 
    ci : int
        Index :math:`j` into :math:`c` pointing to the average
        correlator to differentian with respect to
    
    Returns
    -------
    Partial derivative of :math:`d_n\{m\}`

    """
    if c is None:
        m = len(d)
        n = m//2 + 1
        return dcumulant(d[:n],d[n:],
                         -1 if i > n else i,
                         i-n if i >= n else -1)
        
    return dcumulant(d,c,i,j+1)

def gdnm(d,c):
    r"""Calculate the gradient of the cumulant with respect to the
    average multi-particle correlators.

    Parameters
    ----------
    d : array 
        Differential correlators.  If c is None, then this is assumed
        to also contain the integrated correlators.
    c : array 
        Integrated correlators.  If None, then assume d contains these too

    """
    from numpy import array
    if c is None:
        return array([ddnm(d,i) for i in range(len(d))])
        
    return array([ddnm(d,c,i) for i in range(len(d))] + \
                 [ddnm(d,c,-1,i) for i in range(len(c))])


#
# EOF
#
