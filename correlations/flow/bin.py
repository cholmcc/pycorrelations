# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .. stat    import Derivatives
from .  vnm     import VNM, VPNM
from .. closed  import Closed 
from .. qvector import QStore

# --- Base class for bins --------------------------------------------
class Bin:
    r"""A single bin of flow.  Concrete derived classes are implement
    behaviour for

    * Reference flow 
    * OfInterest with external reference flow bin 
    * Full bin with both of-interest and reference flow 

    Note, an object of this class calculates integrated or
    differential flow coefficients, :math:`v_n\{m\}` or
    :math:`v'_n\{m\}`, *up to* harmonic :math:`\max(n)` and correlator
    :math:`\max(m)`.  That is, for :math:`\max(n)` and
    :math:`\max(m)`, for integrated flow, we calculate

    .. math::

         \forall n=1,\ldots,\max(n)\,\forall m=2,4,\ldots,\max(m):\ 
         v_n\{m\}
    
    and similar for differential flow.  We do this, because we anyway
    need to calculate lower harmonics and correlators for a given
    :math:`n` and :math:`m`.

    Parameters
    ----------
    maxN : int 
        Largest harmonic order of :math:`v_n\{m\}` to calculate 
    maxM : int 
        Largest number of particles to correlate for :math:`v_n\{m\}` 
    weights : bool 
        Whether to use weights 
    partitions : None, sequence of size 2*maxN 
        A list of partitions each harmonic belongs to. If not ``None``
        it must be a sequence of size ``2 * maxN``.  The elements of
        the sequence can be any hashable type, for example strings.
        When later filling the bin, it is important to use the same
        partition identifiers
    vnm : VNM or VPNM 
        Type of flow to calculate - one of 

        :class:`VNM` : integrated flow 
            Calculates :math:`v_n\{m\}` 
        :class:`VPNM` : differential flow 
            Calculates :math:`v_n'\{m\}` 
    
    policy : correlations.stat.Policy 
        How to calculate uncertainties

    """
    def __init__(self,
                 maxN,
                 maxM,
                 weights=True,
                 partitions=None,
                 vnm=VNM,
                 policy=Derivatives):
        self._maxN       = maxN
        self._maxM       = maxM
        maxK             = maxM//2+1
        self._partitions = partitions \
            if partitions is not None and len(partitions) == maxM \
               else None
        self._policy     = policy
        self._h          = {n: {2*k: self._hv(n,k,self._partitions)
                                 for k in range(1,maxK)} 
                            for n in range(maxN,0,-1) }
        self._v          = {n: {2*k: vnm(n,2*k,policy)
                                 for k in range(1,maxK) }
                            for n in range(1,maxN+1) }

        if partitions is not None and self._partitions is None:
            raise ValueError(f'Number of partitions {len(partitions)} '
                             f'({partitions}) is not equal to the maximum '
                             f'harmonic order m={maxM}.')
        
    @property
    def maxN(self):
        r"""Largest :math:`m` of :math:`v_n\{m\}` calculated"""
        return self._maxN
    
    @property
    def maxM(self):
        r"""Largest :math:`n` of :math:`v_n\{m\}` calculated"""
        return self._maxM

    @property
    def partitions(self):
        """List of partitions"""
        return self._partitions
    
    def _hv(self,n,k,p):
        r"""Make a harmonic vector, possibly with partitions

        Parameters 
        ----------
        n : int 
            Harmonic number 
        k : int 
            Half the length of the harmonic vector to create 
            :math:`m=2k` 

        Returns
        -------
        hv : list 
            A vector of harmonics  

            .. math::
        
                \(\underbrace{k}{-n,\ldots,-n},\underbrace{k}{n,\ldots,n}\)

        """
        from .. harmonic import HP
        return [n]*k+[-n]*k if p is None or len(p) < 2*k else \
            [HP(hh,pp) for hh,pp in (zip([n]*k+[-n]*k, p[:k]+p[-k:]))]
    
    @property 
    def h(self):
        r"""The storage of harmonics

        This is a dictionary of dictionaries.  The outer most
        dictionary maps harmonic order :math:`n`, while the inner
        dictionary maps correlator order :math:`m` to flow harmonics
        :math:`\mathbf{h}` 

        .. code-block::
       
            h = { '4': { '2': [-4,4],
                         ...
                         '6': [-4,-4,-4,4,4,4] },
                  ...
                  '1': { '2': [-1,1],
                         ...
                         '6': [-1,-1,-1,1,1,1] } }

        Thus, to get the harmonic for :math:`v_{n}\{m\}` we can access
        this with :math:`n` and :math:`m` in that order

        .. code-block:
        
             h[n][m]

        """
        return self._h

    @property
    def v(self):
        r"""The storage of flow calculators 
        
        This is a dictionary of dictionaries.  The outer most
        dictionary maps harmonic order :math:`n`, while the inner
        dictionary maps correlator order :math:`m` to flow calculator 
        :math:`v_n\{m\}`

        .. code-block::
       
            h = { '4': { '2': v42,
                         ...
                         '6': v46 },
                  ...
                  '1': { '2': v12,
                         ...
                         '6': v16 } }

        Thus, to get :math:`v_{n}\{m\}` we can access this with
        :math:`n` and :math:`m` in that order

        .. code-block:
        
             v[n][m]

        """
        return self._v

    def fill(self,phi,w,cls,part):
        r"""Fill in observations into the appropriate :math:`Q`-vectors. 
        
        All arguments should be NumPy arrays to allow for effective
        views and indexing.

        See also
        --------
        :meth:`correlations.qvector.QStore.fill`
            for more on partitions, class and so on.

        Parameters 
        ----------
        phi : array of float, shape=(M,)
            Azimuth angles :math:`\varphi` 
        weight : array of float, shape=(M,)
            Weights :math:`w` 
        cls : array of bit mask, shape=(M,) (optional)
            If given, then it must be an array of bit masks - one 
            for each :math:`\varphi`. The recognised values are 

            :attr:`QVector.REF` : ``0x1`` 
                The corresponding particle is a reference particle 
            :attr:`QVector.POI` : ``0x2`` 
                The corresponding particle is of-interest 
            :attr:`QVector.REF` ``|`` :attr:`QVector.POI` : ``0x3`` 
                The corresponding particle is *both* for reference
                *and* of-interest.

            If not given, assume all particles are reference
            particles.

        partition : array of identifiers, shape=(M,) (optional)
            Identifier of partition each particle belongs to. 
        
            If initially partitions ``'A'``, ``'B'``, and ``'C'`` where 
            set up, we can define if a particle belongs in any of these 
            by entering in one of these identifiers at the appropriate 
            place in this array.

        """
        raise NotImplementedError('Filling not defined')
    
    def reset(self):
        """Resets all internal :math:`Q`-vectors to null"""
        raise NotImplementedError('Resetting not defined')
    
    def update(self):
        """Perform end-of-event updates. 

        This will calculate the appropriate correlators and add them
        to our statistics.
        """
        raise NotImplementedError('Update not defined')

    def __call__(self):
        r"""Do the final calculations
        
        Returns
        -------
        vnm : dict 
            A dictionary of the results. 
        
            For example, for :math:`\max n=4` and :math:`\max m=6` 

            .. code-block::
            
                 {'2' : {'6': v2_6, '4': v2_4, '2': v2_2 },
                  '4' : {'6': v4_6, '4': v4_4, '2': v4_2 } }

            Thus, to access :math:`v_n\{m\}` we would access 

            .. code-block::

                vnm[n][m]
        """

        r = {n: {m : v() for m,v in vn.items()}
             for n, vn  in self.v.items() }
        return r;

    @property
    def meanCorr(self):
        """Return average correlators for each order 

            For example, for :math:`\max n=4` and :math:`\max m=6` 

            .. code-block::
            
                 {'2' : {'6': v2_6, '4': avg2_4, '2': avg2_2 },
                  '4' : {'6': v4_6, '4': avg4_4, '2': avg4_2 } }

            Thus, to access :math:`v_n\{m\}` we would access 

            .. code-block::

                meanCorr[n][m]
        """
        r = {n: {m : v.mean for m,v in vn.items()}
             for n, vn  in self.v.items() }
        return r;
        
        
    @classmethod
    def _fullname(cls):
        """Get full name of class"""
        return cls.__module__+'.'+cls.__qualname__
    
    def todict(self):
        """Convert to a dictionary
        
        Returns
        -------
        d : dict 
            Dictionary of state 
        """
        d = { 'class': Bin._fullname(),
              'maxN':  self._maxN,
              'maxM':  self._maxM,
              'part':  self._partitions,
              'v':    {n: {m: v.todict() for m,v in vn.items() }
                       for n,vn in self.v.items() }}
        if self._partitions is not None:
            d.update({'h': {n: {m: [hi.todict() for hi in hp]
                                for m,hp in hn.items() }
                            for n,hn in self.h.items() } })
        else:
            d.update({'h': self.h})

        return d
    

    def fromdict(self,d):
        """Read state from dictionary 

        Parameters 
        ----------
        d : dict 
            Dictionary to read state from 
        """
        assert d['class'] == Bin._fullname()

        self._maxN       = d['maxN']
        self._maxM       = d['maxM']
        self._partitions = d['part']
        for n, vn in self.v.items():
            dvn = d['v'].get(n,d['v'].get(str(n),{}))
            for m, v in vn.items():
                dvnm = dvn.get(m,dvn.get(str(m),{}))
                v.fromdict(dvnm)
                # try:
                #    v.fromdict(d['v'][n][m])
                # except:
                #    v.fromdict(d['v'][str(n)][str(m)])
                    

        def dict2h(d):
            return HP.fromdict(d) if self._partitions is not None else d
        
        for n, hn in self.h.items():
            for m, h in hn.items():
                try:
                    h = [dict2h(hi) for hi in d['h'][n][m]]
                except:
                    h = [dict2h(hi) for hi in d['h'][str(n)][str(m)]]

    def merge(self,other):
        """Merge state from other"""
        assert isinstance(other,Bin)
        assert self._maxM  == other._maxM
        assert self._maxN  == other._maxN
        assert self._partitions is other._partitions

        if self._partitions is not None:
            for p, op in zip(self._partitions,other._partitions):
                assert p == op

        for (n, hn), (on, ohn) in zip(self.h.items(),other.h.items()):
            assert n == on

            for (m, h), (om, oh) in zip(hn.items(),ohn.items()):
                assert m == om
                
                if self._partitions is not None:
                    assert h.p == oh.p
                    assert h.h == oh.h
                else:
                    assert h == oh
                    
        for n, vn in self.v.items():
            for m, v in vn.items():
                v.merge(other.v[n][m])
        
        
                 
        
# --- Reference flow bin --------------------------------------------
class Reference(Bin):
    r"""A single bin of reference flow.  That is, we have a single
    :math:`Q`-vector from which we calculate correlators.

    For :math:`\max(n)` and :math:`\max(m)`, for integrated flow, we
    calculate

    .. math::

         \forall n=1,\ldots,\max(n)\,\forall m=2,4,\ldots,\max(m):\ 
         v_n\{m\}

    Parameters
    ----------
    maxN : int 
        Largest harmonic order of :math:v_n\{m\} to calculate 
    maxM : int 
        Largest number of particles to correlate for :math:`v_n\{m\}` 
    weights : bool 
        Whether to use weights 
    partitions : None, sequence of size 2*maxN 
        A list of partitions each harmonic belongs to. If not ``None``
        it must be a sequence of size ``2 * maxN``.  The elements of
        the sequence can be any hashable type, for example strings.
        When later filling the bin, it is important to use the same
        partition identifiers
    policy : correlations.stat.Policy 
        How to calculate uncertainties 
    correlator : type 
        The type of correlator to use 

        :class:`Closed` 
             Closed form expression 
        :class:`Recursive` 
             Recursive expression 
        :class:`Recurrence` 
             Recursrence expression

    """
    def __init__(self,
                 maxN,
                 maxM,
                 weights=True,
                 partitions=None,
                 policy=Derivatives,
                 correlator=Closed):

        super(Reference,self).__init__(maxN,maxM,weights,partitions,
                                        VNM,policy)
        self._r  = QStore.make(False,maxN=maxN,maxM=maxM,weights=weights,
                               partitions=partitions)
        self._cc = correlator(self._r)
        self._c  = None
        self._correlator = correlator
        
    def fill(self,phi,w,cls,part):
        r"""Fill in observations of :math:`\varphi` and :math:`w`. 

        See also 
        --------
        :meth:`correlations.qvector.QStore.fill` 
            Fill method in QStore. 

        Parameters 
        ----------
        phi : array of float, shape=(M,)
            Azimuth angles :math:`\varphi` 
        weight : array of float, shape=(M,)
            Weights :math:`w` 
        cls : array of bit mask, shape=(M,) (optional)
            If given, then it must be an array of bit masks - one 
            for each :math:`\varphi`. The recognised values are 

            :attr:`QVector.REF` : ``0x1`` 
                The corresponding particle is a reference particle 
            :attr:`QVector.POI` : ``0x2`` 
                The corresponding particle is of-interest 
            :attr:`QVector.REF` ``|`` :attr:`QVector.POI` : ``0x3`` 
                The corresponding particle is *both* for reference
                *and* of-interest.

            If not given, assume all particles are reference
            particles.

        part : array of identifiers, shape=(M,) (optional)
            Identifier of partition each particle belongs to. 
        
            If initially partitions ``'A'``, ``'B'``, and ``'C'`` where 
            set up, we can define if a particle belongs in any of these 
            by entering in one of these identifiers at the appropriate 
            place in this array.

        """
        self._c = None

        if len(phi) < 1:
            return

        self._r.fill(phi,w,cls,part)

    def reset(self):
        """Resets all internal :math:`Q`-vectors to null"""
        self._r.reset()
     
    def update(self):
        """Do end-of-event calculations.  This calculates the correlators and
        updates the with these observations to the averages.

        The last calculated correlators are accessible through the
        property ``c``.

        """
        # from pprint import pprint 
        if self._c is not None:
            return

        self._c = {}  # Store correlators for this event 
        for n, mh in self.h.items():
            c  = [0]*len(mh)
            self._c[n] = {}
            for m, h in mh.items():
                i    = m//2-1
                c[i] = self._cc(h)
                self.v[n][m].update(c[i::-1])
                self._c[n][m] = c[i]

    @property
    def weights(self):
        """Whether to use weigts"""
        return self._r.weights
    
    @property 
    def c(self):
        """Last calculated correlators (can be ``None``)"""
        return self._c

    def todict(self):
        """Convert to a dictionary
        
        Returns
        -------
        d : dict 
            Dictionary of state 
        """
        return { 'class': Reference._fullname(),
                 'bin':   super(Reference,self).todict() }

    def fromdict(self,d):
        """Read state from dictionary 

        Parameters 
        ----------
        d : dict 
            Dictionary to read state from 
        """
        assert d['class'] == Reference._fullname()
        super(Reference,self).fromdict(d['bin'])
        
    
# --- Differential flow with external integrated flow ----------------
class OfInterest(Bin):
    r"""A single bin of of-interest flow with external reference flow bin.
    That is, here we store the :math:`p` and :math:`q`
    :math:`Q`-vectors, while we use an external reference
    :math:`Q`-vector.  This allows several bins to share reference
    flow calculations effectively.

    For :math:`\max(n)` and :math:`\max(m)`, for integrated flow, we
    calculate

    .. math::

         \forall n=1,\ldots,\max(n)\,\forall m=2,4,\ldots,\max(m):\ 
         v'_n\{m\}

    Parameters
    ----------
    maxN : int 
        Largest harmonic order of :math:v_n\{m\} to calculate 
    maxM : int 
        Largest number of particles to correlate for :math:`v_n\{m\}` 
    weights : bool 
        Whether to use weights 
    partitions : None, sequence of size 2*maxN 
        A list of partitions each harmonic belongs to. If not ``None``
        it must be a sequence of size ``2 * maxN``.  The elements of
        the sequence can be any hashable type, for example strings.
        When later filling the bin, it is important to use the same
        partition identifiers
    policy : correlations.stat.Policy 
        How to calculate uncertainties 
    correlator : type 
        The type of correlator to use 

        :class:`Closed` 
             Closed form expression 
        :class:`Recursive` 
             Recursive expression 
        :class:`Recurrence` 
             Recursrence expression

    """
    def __init__(self, reference):
        maxN       = reference.maxN
        maxM       = reference.maxM
        weights    = reference.weights
        partitions = reference.partitions
        policy     = reference._policy
        correlator = reference._correlator 
        super(OfInterest,self).__init__(maxN,maxM,weights,partitions,
                                        VPNM,policy)

        self._pq = QStore.make(True, maxN=maxN,maxM=maxM,weights=weights,
                               partitions=partitions,
                               r=reference._r.rvec)
        self._dc = correlator(self._pq)
        self._d  = None
        self._i  = reference


    def reset(self):
        """Resets all internal :math:`Q`-vectors to null"""
        self._pq.reset()
        
    def fill(self,phi,w,cls,part):
        r"""Fill in observations of :math:`\varphi` and :math:`w`. 

        See also 
        --------
        :meth:`correlations.qvector.QStore.fill` 
            Fill method in QStore. 

        Parameters 
        ----------
        phi : array of float, shape=(M,)
            Azimuth angles :math:`\varphi` 
        weight : array of float, shape=(M,)
            Weights :math:`w` 
        cls : array of bit mask, shape=(M,) (optional)
            If given, then it must be an array of bit masks - one 
            for each :math:`\varphi`. The recognised values are 

            :attr:`QVector.REF` : ``0x1`` 
                The corresponding particle is a reference particle 
            :attr:`QVector.POI` : ``0x2`` 
                The corresponding particle is of-interest 
            :attr:`QVector.REF` ``|`` :attr:`QVector.POI` : ``0x3`` 
                The corresponding particle is *both* for reference
                *and* of-interest.

            If not given, assume all particles are reference
            particles.

        part : array of identifiers, shape=(M,) (optional)
            Identifier of partition each particle belongs to. 
        
            If initially partitions ``'A'``, ``'B'``, and ``'C'`` where 
            set up, we can define if a particle belongs in any of these 
            by entering in one of these identifiers at the appropriate 
            place in this array.

        """
        self._d = None
        if len(phi) < 1:
            return
        
        self._pq.fill(phi,w,cls,part)

    def update(self):
        """Do end-of-event calculations.  This calculates the correlators and
        updates the with these observations to the averages.

        The last calculated correlators are accessible through the
        property ``c``.

        """
        # from pprint import pprint 
        self._d = {}  # Store correlators for this event 
        for n, mh in self.h.items():
            d  = [0]*len(mh)
            c  = [0]*len(mh)
            self._d[n] = {}
            for m, h in mh.items():
                i    = m//2-1
                d[i] = self._dc(h)
                c[i] = self._i.c[n][m]
                self.v[n][m].update(d[i::-1]+c[i::-1])
                self._d[n][m] = d[i]

    @property 
    def c(self):
        """Last calculated correlators (can be ``None``)"""
        return self._i.c

    @property 
    def d(self):
        """Last calculated correlators (can be ``None``)"""
        return self._d

    def todict(self):
        """Convert to a dictionary
        
        Returns
        -------
        d : dict 
            Dictionary of state 
        """
        return { 'class': OfInterest._fullname(),
                 'bin':   super(OfInterest,self).todict() }

    def fromdict(self,d):
        """Read state from dictionary 

        Parameters 
        ----------
        d : dict 
            Dictionary to read state from 
        """
        assert d['class'] == OfInterest._fullname()
        super(OfInterest,self).fromdict(d['bin'])


# --- Full flow bin ------------------------------------------
class Full(OfInterest):
    r"""A single of flow.  Here, we store three separate :math:`Q`-vectors:
    :math:`p,q,r`.

    For :math:`\max(n)` and :math:`\max(m)`, for integrated flow, we
    calculate

    .. math::

         \forall n=1,\ldots,\max(n)\,\forall m=2,4,\ldots,\max(m):\ 
         v'_n\{m\}

    Parameters
    ----------
    maxN : int 
        Largest harmonic order of :math:v_n\{m\} to calculate 
    maxM : int 
        Largest number of particles to correlate for :math:`v_n\{m\}` 
    weights : bool 
        Whether to use weights 
    partitions : None, sequence of size 2*maxN 
        A list of partitions each harmonic belongs to. If not ``None``
        it must be a sequence of size ``2 * maxN``.  The elements of
        the sequence can be any hashable type, for example strings.
        When later filling the bin, it is important to use the same
        partition identifiers
    policy : correlations.stat.Policy 
        How to calculate uncertainties 
    correlator : type 
        The type of correlator to use 

        :class:`Closed` 
             Closed form expression 
        :class:`Recursive` 
             Recursive expression 
        :class:`Recurrence` 
             Recursrence expression

    """
    def __init__(self,
                 maxN,
                 maxM,
                 weights=True,
                 partitions=None,
                 policy=Derivatives,
                 correlator=Closed):

        r = Reference(maxN,maxM,weights,
                      partitions=partitions,
                      policy=policy,
                      correlator=correlator)
        
        super(Full,self).__init__(r)

    def reset(self):
        """Resets all internal :math:`Q`-vectors to null"""
        self._i.reset()
        super(Full,self).reset()
        
    def fill(self,phi,w,cls,part):
        r"""Fill in observations of :math:`\varphi` and :math:`w`. 
        
        See also 
        --------
        :meth:`correlations.qvector.QStore.fill` 
            Fill method in QStore. 
        
        Parameters 
        ----------
        phi : array of float, shape=(M,)
            Azimuth angles :math:`\varphi` 
        weight : array of float, shape=(M,)
            Weights :math:`w` 
        cls : array of bit mask, shape=(M,) (optional)
            If given, then it must be an array of bit masks - one 
            for each :math:`\varphi`. The recognised values are 
            
            :attr:`QVector.REF` : ``0x1`` 
                The corresponding particle is a reference particle 
            :attr:`QVector.POI` : ``0x2`` 
                The corresponding particle is of-interest 
            :attr:`QVector.REF` ``|`` :attr:`QVector.POI` : ``0x3`` 
                The corresponding particle is *both* for reference
                *and* of-interest.
            
            If not given, assume all particles are reference
            particles.

        part : array of identifiers, shape=(M,) (optional)
            Identifier of partition each particle belongs to. 
            
            If initially partitions ``'A'``, ``'B'``, and ``'C'`` where 
            set up, we can define if a particle belongs in any of these 
            by entering in one of these identifiers at the appropriate 
            place in this array.

        """
        self._i.fill(phi,w,cls,part)
        super(Full,self).fill(phi,w,cls,part)

    def update(self):
        """Do end-of-event calculations.  This calculates the correlators and
        updates the with these observations to the averages.

        The last calculated correlators are accessible through the
        property ``c``.

        """
        if self._c is not None:
            return

        self._i.update()
        super(Full,self).update()

    def todict(self):
        """Convert to a dictionary
        
        Returns
        -------
        d : dict 
            Dictionary of state 
        """
        return { 'class': Full._fullname(),
                 'bin':   super(Full,self).todict() }

    def fromdict(self,d):
        """Read state from dictionary 

        Parameters 
        ----------
        d : dict 
            Dictionary to read state from 
        """
        assert d['class'] == Full._fullname()
        super(Full,self).fromdict(d['bin'])
        
#
# EOF
#

