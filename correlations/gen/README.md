# Data generator package 

Here we find code to generate data.

- [`Generator`](generator.py) is the base class for a data
  generator. It uses the service of a [`Sampler`](sampler.py) for how
  to generate the data and which data to generate.  Thus, this class
  mainly does the book-keeping of data generation. 
  
  - The sub-class `Momentum` balances the total momentum by adding
    mirror particles for all generated particles.  Mirror particles
    are particles where $`\varphi\rightarrow\varphi+\pi`$ and
    $`\eta\rightarrow-\eta`$ but with the same $`p_{\mathrm{T}}`$ and
    weight. 
	
  - The sub-class `JetLike` generates a base event but adds jet-like
    particles on top.  Given a leading particle, a jet is a set of
    particles that are largely collimated (in $`\varphi`$ and
    $`\eta`$) with leading particle.  To balance the total momentum of
    jets we add a mirror jet (i.e., all particles in a jet are
    mirrored). 
	
  Both of these sub-classes introduce _non-flow_ into the data. 
  
  We can ask the generator to simulate acceptance or the like.  If
  enabled, then we sample a uniform random distribution for each
  particle and only accept that particle if the random number $`r`$ is
  smaller than the corresponding weight of the particle. 
  
- [`Header`](header.py) contains information about a run, such as
  parameters of the sampler, and so on. 
  
- [`Sampler`](sampler.py) defines the interface for generating
  particles.  In this project we are only interested in 
  
  - $`\eta=-\log\tan^{-1}(\vartheta/2)`$ - the pseudorapidity (where
    $`\vartheta`$ is the polar angle),
  - $`p_{\mathrm{T}}`$ - the transverse momentum,
  - $`\varphi`$ - the azimuth angle, and 
  - $`w`$ - the particle weight 
  
  Thus, this class has hooks for generating of these four variables.
  Specific sub-classes implement different generation 
  
  - `PhiW` only generates $`(\varphi,w)`$
  - `EtaPhiW` generates $`(\eta,\varphi,w)`$
  - `PtPhiW` generates $`(p_{\mathrm{T}},\varphi,w)`$ 
  - `EtaPtPhiW` generates $`(\eta,p_{\mathrm{T}},\varphi,w)`$ 

## Standalone program 

The script [`correlations.progs.writer`](../progs/writer.py) writes
out a data file using the selected settings (generator and sampler).

The output format is as follows. 

- First, there are 8 comment lines (start with the character `#`) that
  outlines the content of the header. 
- Next, are 5 header lines.  These are, in turn 
  1. Number of events, least multiplicity, largest multiplicity 
  
        N_EVENTS MIN_MULTIPLICITY MAX_MULTIPLICITY 
	  
  2. Number of flow coefficients, followed by that many flow coefficents 
  
	    N_FLOW_COEFFICENTS V1 V2 ...
		
  3. Number of transverse momentum bin edges, followed by that many
     edge values 
	 
	     N_PT_BINS PT1 PT2 ...

  4. Number of transverse momentum modulation coefficients, followed
     by that many coefficients
	 
		  N_PT_COEFFICENTS A B C ...
		  
  5. Mean transverse momentum 
  
	      MEAN_PT 
		  
- Next are 10 comment lines (starting with the character `#`)
  outlining the event record format.
  
- After that `N_EVENTS` event record follows.  Each event record is
  formatted as follows 
  
  - First, a line that says how many particles are available in this
    event record. 
	
	    N_PARTICLES 
		
  - Then `N_PARTICLES` lines follows of per-particle information. Each
    line consist of four fields 
	
	1. Pseudorapidity 
	2. Transverse momentum 
	3. Azimuth angle 
	4. Weight 
	
	I.e., 
	
	      ETA PT PHI WEIGHT 
		  
The output format matches that of the
[`mcorrelations`](https://cern.ch/cholm/mcorrelations] library.  Thus,
files generated in that framework may be analysed by the code of this
package and vice versa. 
		  

## Status 

- [x] [Generators](generator.py)
  - [x] `Generator` - simple generator 
  - [x] `Momentum` - momentum balanced 
  - [x] `JetLike` - Momentum balanced jets 
- [x] [Samplers](sampler.py)
  - [x] `PhiW` generates random $`\varphi`$ and weights $`w`$
  - [x] `EtaPhiW` also generates random $`\eta`$ 
  - [x] `PtPhiW` generates random $`p_{\mathrm{T}}`$ in addition to
        `PhiW` 
  - [x] `EtaPtPhiW` does all four things 

<!--  LocalWords:  collimated
 -->
