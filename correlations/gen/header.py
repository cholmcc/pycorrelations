# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Header:
    """Run header 
        
    This contains information about the run 
    """
    def __init__(self):
        from numpy import empty
        self._nev    = 0
        self._minN   = 0
        self._maxN   = 0
        self._vn     = empty(0)
        self._ptbin  = empty(0)
        self._ptcoef = empty(0)
        self._meanpt = 0

    @property
    def nev(self):
        """Number of events"""
        return self._nev

    @nev.setter
    def nev(self,value): self._nev = value

    @property
    def minN(self):
        """Least multiplicity"""
        return self._minN

    @minN.setter
    def minN(self,value): self._minN = value

    @property
    def maxN(self):
        """Largest multiplicity"""
        return self._maxN

    @maxN.setter
    def maxN(self,value): self._maxN = value

    @property
    def vn(self):
        """Flow coefficients :math:`v_n`"""
        return self._vn

    @vn.setter
    def vn(self,value): self._vn = value

    @property
    def ptbin(self):
        r"""Transverse momentum :math:`p_{\mathrm{T}}` bins"""
        return self._ptbin

    @ptbin.setter
    def ptbin(self,value): self._ptbin = value

    @property
    def ptcoef(self):
        r"""Transverse momentum modulation coefficients"""
        return self._ptcoef

    @ptcoef.setter
    def ptcoef(self,value): self._ptcoef = value

    @property
    def meanpt(self):
        r"""Mean transverse momentum :math:`\overline{p_{\mathrm{T}}}`"""
        return self._meanpt

    @meanpt.setter
    def meanpt(self,value): self._meanpt = value

    def todict(self):
        return {'class':        Header.__module__+'.'+Header.__name__,
                'nev':          self.nev,
                'multiplicity': [self.minN, self.maxN],
                'flow_coef':    list(self.vn),
                'pt_bin':       list(self.ptbin),
                'pt_coef':      list(self.ptcoef),
                'mean_pt':      self.meanpt }

    def fromdict(self,d):
        assert d['class'] == Header.__module__+'.'+Header.__name__

        self.nev  = d['nev']
        self.minN, self.maxN = d['multiplicity']
        self.vn              = d['flow_coef']
        self.ptbin           = d['pt_bin']
        self.ptcoef          = d['pt_coef']
        self.meanpt          = d['mean_pt']

    @classmethod
    def make(cls,d):
        r = cls()
        r.fromdict(d)
        return r
    
    @classmethod
    def read(cls,file):
        """Read a header from a file 

        Parameters 
        ----------
        file : file 
            File to read from 

        Returns 
        -------
        header : Header 
            Read header 
        """
        h = cls()
        
        cls._read_comments(file,8,'header');

        h.nev, h.minN, h.maxN = (int(f) for f in file.readline().split())
        h.vn      = cls._read_array(file)
        h.ptbin   = cls._read_array(file)
        h.ptcoef  = cls._read_array(file)
        h.meanpt  = float(file.readline())

        cls._read_comments(file,10,'events')

        return h

    def write(self,file):
        """Write header to a file 

        Parameters
        ----------
        file : file 
            File to write to 
        """
        self._head_comment(file)

        print('\t'.join([f'{f}' for f in [self.nev,self.minN,self.maxN]]),
              file=file)
        Header._write_array(file,self.vn)
        Header._write_array(file,self.ptbin)
        Header._write_array(file,self.ptcoef)
        print(self.meanpt,file=file)

        self._event_comment(file)


    @classmethod
    def copy(cls,input,output,nev=0):
        """Copy header from input to output 

        Parameters 
        ----------
        input : file 
            File to read from 
        output : file 
            File to write to 
        nev : int 
            If not 0, override read number of events 
        
        Returns 
        -------
        nev : int 
            The number of events (read or overridden) 
        """
        h = cls.read(input)

        if nev > 0:
            h.nev = max(nev, h.nev)

        h.write(output)

        return h.nev
        
    def _head_comment(self,file):
        """Write header comment to file"""
        from textwrap import dedent as d
        print(d("""\
                # Next 5 data lines is the header.
                # The lines are:
                # - Number of events
                # - Number of particles range
                # - Flow coefficents: N V1 V2 ... VN
                # - pT bin edges:     N B1 B2 ... BN
                # - pT coefficents:   N C1 C2 ... CN
                # - Mean pT (in GeV/c)"""),file=file)

    def _event_comment(self,file):
        """Write event comment to file"""
        from textwrap import dedent as d
        print(d("""\
                # Following records are for each event.
                # 
                # Event records start with the number of particles in the event
                # followed by that many lines of particle information.
                # Each line contain the 4 fields (in order):
                # 
                # - Pseudorapidity (eta)
                # - Transverse momentum (pT)
                # - Azimuth angle (phi) in radians
                # - Weight (weight)"""),file=file)
            
    @classmethod
    def _read_comments(cls,file,lines,where):
        """Read comment lines from file 
        
        Parameters 
        ----------
        file : file 
            File to read from 
        lines : int 
            Number of lines 
        where : str 
            Calling context
        """
        for _ in range(lines):
            l = file.readline()
            if l[0] != '#':
                raise RuntimeError(f'Expected {where} comment line, '
                                   f'but got "{l}"')

    @classmethod
    def _read_array(cls,file):
        """Read an array from file 
        
        Parameters 
        ----------
        file : file 
            File to read from 

        Returns 
        -------
        data : array 
            Read array 
        """
        from numpy import array
        return array([float(f) for f in file.readline().split()[1:]])

    @classmethod
    def _write_array(cls,file,a):
        """Write an array to file 
        
        Parameters 
        ----------
        file : file 
            File to read from 
        a : array 
            Array to write  
        """
        print(f'{len(a)}\t'+'\t'.join([f'{x}' for x in a]),file=file)

#
# EOF
#

        
        
