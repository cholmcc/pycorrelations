# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from . sampler import PhiW

# --- Base class -----------------------------------------------------
class Generator:
    r"""Generator of events 
    
    Each event is a list of particles where each particle is
    characterised by
    
    #. Pseudorapidity :math:`\eta=-\log\tan(\vartheta/2)`
    #. Transverse momentum :math:`p_{\mathrm{T}}` 
    #. Azimuth angle :math:`\varphi`
    #. Weight :math:`w`

    Parameters 
    ----------
    rnd : numpy.random.Generator 
        Random number generator 
    sampler : Sampler 
        Sampler of distributions 
    minM : int 
        Least number of particles to create 
    maxM : int 
        Largest number of particles to create 
    hungry : bool 
        If true, always generate exactly the right number of 
        particles 
    ranpsi : bool 
        If true, generate random event plane
    acc : bool 
        Simulate acceptance by weights 

    """
    def __init__(self,rnd,sampler,
                 minM,maxM=0,hungry=False,ranpsi=False,acc=False):
        self._minM       = minM
        self._maxM       = maxM
        self._hungry     = hungry
        self._ranpsi     = ranpsi
        self._rnd        = rnd
        self._sampler    = sampler
        self._acceptance = acc
    def __call__(self):
        r"""Generate a single event.  

        The exact generation of the 4 quantities
        :math:`(\eta,p_{\mathrm{T}},\varphi,w)` for each particle is
        governed by sampler used.

        Returns
        -------
        eta : array 
            Pseudorapidity of particles 
        pt : array 
            Transverse momentum of particles 
        phi : array 
            Azimuth angles of particles 
        weights : array 
            Weights of particles

        """
        from numpy import pi, concatenate
        
        psi = self._rnd.uniform(0,2*pi) if self._ranpsi else 0
        m   = self._rnd.integers(self._minM,self._maxM) \
            if self._minM < self._maxM else self._minM

        eta,pt,phi,weights = self.bulk(m,psi)
        
        while self._hungry and len(eta) < m:
            eta1,pt1,phi1,weights1 = self.bulk(m-len(eta),psi)
            if self._acceptance:
                msk = self._rnd.uniform(size=phi.shape) <= 1/weights;
                eta1    = eta1   [msk] 
                pt1     = pt1    [msk]
                phi1    = phi1   [msk]
                weight1 = weight1[msk]
                
            eta     = concatenate((eta,    eta1))
            pt      = concatenate((pt,     pt1))
            phi     = concatenate((phi,    phi1))
            weights = concatenate((weights,weights1))
            
        return eta,pt,phi,weights

    def header(self,nev=0):
        """Generate a header 
        
        Parameters 
        ----------
        nev : int 
            Number of events to generate 

        Returns
        -------
        header : Header 
            Header for this run 
        """
        from . header import Header

        h = Header()
        h.nev  = nev
        h.minN = self._minM
        h.maxN = self._maxM
        self._sampler.fill_in(h)
        
        return h
    
    def bulk(self,m,psi):
        """Generate m particles with event plane psi 

        Parameters
        ----------
        m : int 
            Number of particles to generate 
        psi : float 
            Random event plane angle 
        
        Returns
        -------
        eta : array 
            Pseudorapidity of particles 
        pt : array 
            Transverse momentum of particles 
        phi : array 
            Azimuth angles of particles 
        weight : array 
            Weights of particles 
        """        
        return self._sampler(m,psi)

    def mirror(self,eta,pt,phi,weight):
        r"""Mirrors particles in pseudorapidy and azimuth angle

        That is, given a particle 

        .. math::
        
             (\eta,p_{\mathrm{T}},\varphi,w) 

        this will produce a particle 

        .. math::
        
             (-\eta,p_{\mathrm{T}},\varphi+\pi,w) 

        
        Parameters
        -------
        eta : array 
            Pseudorapidity of particles 
        pt : array 
            Transverse momentum of particles 
        phi : array 
            Azimuth angles of particles 
        weights : array 
            Weights of particles 

        Returns
        -------
        meta : array 
            Mirror pseudorapidity of particles 
        pt : array 
            Transverse momentum of particles 
        mphi : array 
            Mirror azimuth angles of particles 
        weights : array 
            Weights of particles 
        """
        from numpy import pi, fmod
        return -eta,pt,fmod(phi+pi,2*pi),weight

    @classmethod
    def make(cls,name,rnd,sampler,minM,maxM=0,hungry=False,ranpsi=False,
             acc=False,**kwargs):
        r"""Make a generator 

        Parameters
        ----------
        name : string 
            Type of generator.  Can be one of (not case sensitive)

            ``momentum`` : :class:`Momentum` 
                momentum balanced
            ``jetlike``  : :class:`JetLike` 
                jet-like
            other        : :class:`Generator`
                basic 
        
        rnd : numpy.random.Generator 
            Random number generator 
        sampler : :class:`Sampler` 
            Sampler object to use 
        minM : int 
            Least multiplicity 
        maxM : int 
            Largest multiplicity 
        hungry : bool 
            Generate exact number of particles 
        ranspi : bool 
            Generate random event plane :math:`\Psi`
        
        Returns
        -------
        gen : :class:`Generator` 
            The made generator object 
        """
        lname = name.lower()

        if 'mom' in lname:
            return Momentum(rnd,sampler,minM,maxM,hungry,ranpsi,acc,**kwargs)

        if "jet" in lname:
            return JetLike(rnd,sampler,minM,maxM,hungry,ranpsi,acc,**kwargs)

        return Generator(rnd,sampler,minM,maxM,hungry,ranpsi,acc,**kwargs)

# --- Momentum conservation ------------------------------------------
class Momentum(Generator):
    """Generator of events where explicit momentum is conserved by
    mirroring all particles in pseudorapidity and azimuth angle 

    Parameters 
    ----------
    rnd : numpy.random.Generator 
        Random number generator 
    sampler : Sampler 
        Sampler of distributions 
    minM : int 
        Least number of particles to create 
    maxM : int 
        Largest number of particles to create 
    hungry : bool 
        If true, always generate exactly the right number of 
        particles 
    ranpsi : bool 
        If true, generate random event plane

    """
    def __init__(self,rnd,sampler,minM,maxM=0,
                 hungry=False,ranpsi=False,acc=False):
        super(Momentum,self).__init__(rnd,sampler,minM,maxM,hungry,ranpsi,acc)

    def bulk(self,m,psi):
        """Generates m particles, explicitly balanced for mometum 
        by mirroring the pseudorapidity and azimuth angle 
        
        Parameters
        ----------
        m : int 
            Number of particles to generate 
        psi : float 
            Random event plane angle 
        
        Returns
        -------
        eta : array 
            Pseudorapidity of particles 
        pt : array 
            Transverse momentum of particles 
        phi : array 
            Azimuth angles of particles 
        weight : array 
            Weights of particles 
        """                
        from numpy import concatenate

        tgt                   = max(m//2,1)
        eta, pt, phi, weight  = super(Momentum,self).bulk(tgt,psi)
        meta,mpt,mphi,mweight = self.mirror(eta,pt,phi,weight)
        eta                   = concatenate((eta,   meta))
        phi                   = concatenate((phi,   mphi))
        pt                    = concatenate((pt,    mpt))
        weight                = concatenate((weight,mweight))

        return eta,pt,phi,weight

# --- Jets -----------------------------------------------------------
class JetLike(Generator):
    r"""Generator of events where explicit back-to-back jets are simulated
    by conserving momentum by mirroring some particles in
    pseudorapidity and azimuth angle

    The probability of a jet is given by 

    .. math::

        f(p_{\mathrm{T}})=f\frac{p_T^3}{p_{\mathrm{T,max}}}

    that is, a high-pT particle is more likely to trigger jet generation 

    Parameters 
    ----------
    rnd : numpy.random.Generator 
        Random number generator 
    sampler : Sampler 
        Sampler of distributions 
    minM : int 
        Least number of particles to create 
    maxM : int 
        Largest number of particles to create 
    hungry : bool 
        If true, always generate exactly the right number of 
        particles 
    ranpsi : bool 
        If true, generate random event plane
    fraction : float 
        Fraction of event that is jets
    """
    def __init__(self,rnd,sampler,minM,maxM=0,hungry=False,ranpsi=False,
                 acc=False,fraction=2,**kwargs):
        from numpy import deg2rad
        
        super(JetLike,self).__init__(rnd,sampler,minM,maxM,hungry,ranpsi,acc)
        self._fraction   = fraction
        self._jet_mean   = 20
        self._jet_width  = deg2rad(2)
        self._jet_spread = 0.1
        self._jet_power  = 3
        self._jet_pt     = 5**self._jet_power
    
    def bulk(self,m,psi):
        """Generates approximate m particles, some of which are in jets.  Jets
        are explicitly balanced in momentum by mirroring
        pseudorapidity and azimuth angle.

        Parameters
        ----------
        m : int 
            Number of particles to generate 
        psi : float 
            Random event plane angle 
        
        Returns
        -------
        eta : array 
            Pseudorapidity of particles 
        pt : array 
            Transverse momentum of particles 
        phi : array 
            Azimuth angles of particles 
        weight : array 
            Weights of particles

        """        
        from numpy import concatenate

        tgt               = max(2*m//3,1)
        eta,pt,phi,weight = super(JetLike,self).bulk(tgt,psi)
        jet_prob          = self._fraction*pt**self._jet_power / self._jet_pt
        jet               = self._rnd.uniform(size=eta.shape) < jet_prob
        
        if not jet.any():
            return eta, pt, phi, weight

        jeta,jpt,jphi,jweight = eta[jet],pt[jet],phi[jet],weight[jet]
        meta,mpt,mphi,mweight = self.mirror(jeta,jpt,jphi,jweight)

        eta    = concatenate((eta,   meta))
        phi    = concatenate((phi,   mphi))
        pt     = concatenate((pt,    mpt))
        weight = concatenate((weight,mweight))

        njet = self._rnd.poisson(self._jet_mean,size=jeta.shape)
        # print(f'{len(jeta)} jets: {njet.sum()}')
        for n,p,e in zip(njet,jphi,jeta):
            deta,dpt,dphi,dweight = super(JetLike,self).bulk(n,psi)
            deta                  = self._rnd.normal(deta,self._jet_spread)
            dphi                  = self._rnd.normal(dphi,self._jet_width)
            
            meta,mpt,mphi,mweight = self.mirror(deta,dpt,dphi,dweight)
            eta                   = concatenate((eta,   deta,   meta))
            phi                   = concatenate((phi,   dphi,   mphi))
            pt                    = concatenate((pt,    dpt,    mpt))
            weight                = concatenate((weight,dweight,mweight))

        return eta,pt,phi,weight

#
# EOF
# 

        
        
