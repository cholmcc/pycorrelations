# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --- Base class -----------------------------------------------------
class Sampler:
    r"""Constructor a sampler 

    A sampler generates particles.  This base implementation
    generates random :math:`\varphi` and :math:`w`.  The
    transverse momentum and pseudorapidity are always null.
    
    Parameters
    ----------
    rnd : numpy.random.Generator 
        Random number generator 
    vs : array of floats
        Flow coefficents

    """
    def __init__(self,rnd,vs):
        from nbi_stat import pdf_sampler
        from numpy import linspace, pi
        
        self._rnd      = rnd
        self._vn       = vs
        self._phi_dist = pdf_sampler(self._phi_pdf,linspace(0,2*pi,100))

    def fill_in(self,header):
        """Fill in information into header 

        Parameters 
        ----------
        header : Header 
            Header to fill information into 
        """
        header.vn = self._vn

    def __call__(self,m,psi):
        r"""Generate m particles with event plane :math:`\psi`

        Parameters
        ----------
        m : int 
            Number of particles to generate 
        psi : float 
            Random event plane angle :math:`\psi`
        
        Returns
        -------
        eta : array 
            Pseudorapidity of particles :math:`\eta`
        pt : array 
            Transverse momentum of particles :math:`p_{\mathrm{T}}`
        phi : array 
            Azimuth angles of particles :math:`\varphi`
        weights : array 
            Weights of particles :math:`w`
        """
        from numpy import fmod, pi
        
        eta    = self._gen_eta(m)
        pt     = self._gen_pt(eta)
        phi    = fmod(self._gen_phi(eta,pt)+psi,2*pi)
        weight = self._gen_weight(eta,pt,phi)
        return eta,pt,phi,weight

    def _gen_eta(self,m):
        r"""Generate psuedorapidities :math:`\eta`

        Parameters
        ----------
        m : int 
            Number of particles to generate 

        Returns
        -------
        eta : array 
            Pseudorapidity of particles 
        """
        from numpy import zeros

        return zeros(m)

    def _gen_pt(self,eta):
        r"""Generate transverse momentum :math:`p_{\mathrm{T}}`

        Parameters
        ----------
        eta : array 
            Pseudorapidity of particles :math:`\eta`

        Returns
        -------
        pt : array 
            Transverse momentum of particles
        """
        from numpy import zeros_like

        return zeros_like(eta)

    def _gen_phi(self,eta,phi):
        r"""Generate azimuth angles :math:`\varphi`

        Parameters
        ----------
        eta : array 
            Pseudorapidity of particles :math:`\eta`
        pt : array 
            Transverse momentum of particles :math:`p_{\mathrm{T}}`

        Returns
        -------
        phi : array 
            Azimuth angle of particles
        """
        return self._phi_dist(self._rnd.uniform(size=eta.shape))

    def _gen_weight(self,eta,pt,phi):
        r"""Generate weights :math:`w`

        Parameters
        ----------
        eta : array 
            Pseudorapidity of particles :math:`\eta`
        pt : array 
            Transverse momentum of particles :math:`p_{\mathrm{T}}`
        phi : array 
            Azimuth angle of particles :math:`\varphi`

        Returns
        -------
        weight : array 
            Weight of particles
        """
        return self._rnd.uniform(1,1+0.144,size=eta.shape)

    def _phi_pdf(self,phi):
        r"""Azimuth angle PDF

        .. math::

            f_{\mathrm{v}}(\varphi) = 1 + \sum_{n} 2v_n\cos(n\phi)

        Parameters
        ----------
        phi : float or array 
            Angle(s)
        
        Returns
        -------
        p : array 
            Probability of each angle (not normalized)
        """
        from numpy import cos, arange, atleast_2d, newaxis
        nphi = arange(1,len(self._vn)+1) * atleast_2d(phi).T
        return 1 + 2*(self._vn * cos(nphi)).sum(axis=-1)

    @classmethod
    def make(cls,name,rnd,vs,**kwargs):
        r"""Generate a sampler 

        Parameters
        ----------
        name : str 
            Type of sampler. Options are 

            ``eta``   : :class:`EtaPhiW` 
                Random :math:`\eta,\varphi,w`
            ``pt``    : :class:`PtPhiW` 
                Random :math:`p_{\mathrm{T}},\varphi,w`
            ``pteta`` : :class:`PtPhiW` 
                Random :math:`p_{\mathrm{T}},\eta,\varphi,w`
            other : ``PhiW`` (really :class:`Sampler`)
                Random :math:`\varphi,w`
         
        rnd : numpy.random.Generator 
            Random number generator 
        vs : array 
            Flow coefficients 
        kwargs : dict 
            Additional arguments for sampler.  Possibilities are 
        
            slope : float 
                Pseudorapidity distribution slope 
                (:class:`EtaPhiW`, :class:`EtaPtPhiW`)
            ptbin : array 
                Transverse momentum bins 
                (:class:`PtPhiW`, :class:`EtaPtPhiW`)
            ptcoef : array 
                Transverse momentum modulation coefficients 
                (:class:`PtPhiW`, :class:`EtaPtPhiW`)
            meanpt : float 
                Mean transverse momentum 
                (:class:`PtPhiW`, :class:`EtaPtPhiW`)
        
        Returns 
        -------
        sampler : Sampler 
            Sampler of the chosen kind 
        """
        lname = name.lower()
        if "pt" in lname:
            if "eta" in lname:
                return EtaPtPhiW(rnd,vs,**kwargs)
            return PtPhiW(rnd,vs,**kwargs)
        if "eta" in lname:
            return EtaPhiW(rnd,vs,**kwargs)

        return PhiW(rnd,vs)

# --- Alias ----------------------------------------------------------
PhiW = Sampler


# --- With eta -------------------------------------------------------
class EtaPhiW(Sampler):
    r"""Constructor a sampler 

    A sampler generates particles.  
    
    This generates 
    
    * Pseudorapidity :math:`\eta=-\log\tan(\vartheta/2)`
    * Azimuthal angle :math:`\varphi`
    * Weight :math:`w` 

    The pseudorapidity is sampled from an inverse triangular distribution 
    
    .. math::
    
        f_{a,b}(\eta) = a |\eta|+b 

    over the range :math:`-1<\eta<1` with :math:`a` as a parameter and 
    :math:`b=(1-a)/2` to normalise the integral. 
    
    Parameters
    ----------
    rnd : numpy.random.Generator 
        Random number generator 
    vs : array of floats
        Flow coefficents 
    slope : float 
        Slope of pseudorapidity distribution 
    """
    def __init__(self,rnd,vs,slope=.1,**kwargs):
        from nbi_stat import pdf_sampler
        from numpy import linspace,abs
        
        super(EtaPhiW,self).__init__(rnd,vs)
        self._eta_dist = pdf_sampler(lambda x:slope*abs(x)+(1-slope)/2,
                                     linspace(-1,1,30))

    def _gen_eta(self,m):
        """Generate psuedorapidities :math:`\eta`

        Parameters
        ----------
        m : int 
            Number of particles to generate 

        Returns
        -------
        eta : array 
            Pseudorapidity of particles 
        """
        return self._eta_dist(self._rnd.uniform(size=m))


# --- With pT --------------------------------------------------------
class PtPhiW(PhiW):
    r"""Constructor a sampler 

    A sampler generates particles 
    
    This generates 

    * Transverse momentum :math:`p_{\mathrm{T}}`
    * Azimuthal angle :math:`\varphi`
    * Weight :math:`w` 

    The transverse momentum is sampled from an exponential distribution 
    
    .. math::
    
        f_{\overline{p_{\mathrm{T}}}}(p_{\mathrm{T}}) 
        = e^{-p_{\mathrm{T}}/\overline{p_{\mathrm{T}}}}

    where :math:`\overline{p_{\mathrm{T}}}` is the average
    transverse momentum chosen.

    The azimuth angle :math:`\varphi` is sampled from a Fourier
    expansion with given flow parameters

    .. math::

        f_{\mathrm{v}}(\varphi) 
        = A(p_{\mathrm{T}})\left[1 + \sum_{n} 2v_n\cos(n\phi)\right]\quad,

    where 

    .. math::
    
         A(p_{\mathrm{T}}) = \sum_{i=1} a_i p_{\mathrm{T}}^{i}

    is a modulation based on the transverse momentum.  By default
    the parameters :math:`\mathbf{a}=\{0,1.77,-0.31\}` tuned to
    result from Pb-Pb at :math:`\sqrt{s_{\mathrm{NN}}} =
    2.76\,\mathrm{T\!e\!V}` (Phys.Rev.Lett. 105 (2010) 252302) so
    that the flow is roughly recovered at the mean transverse
    momentum.

    Parameters
    ----------
    rnd : numpy.random.Generator 
        Random number generator 
    vs : array of floats
        Flow coefficents 
    ptbin : array 
        Transverse momentum bins 
    ptcoef : array 
        Transverse momentum coefficients for modulation of 
        azimuth angle distribution 
    meanpt : float 
        Mean transverse momentum

    """
    def __init__(self,rnd,vs,ptbin=None,ptcoef=None,meanpt=2,**kwargs):
        from nbi_stat import pdf_sampler
        from numpy import linspace, pi, array
        
        super(PtPhiW,self).__init__(rnd,vs)
        if ptbin is None:
            ptbin = array([0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
			   1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.5, 3.0,
			   3.5, 4.0, 4.5, 5.0])
        if ptcoef is None:
            ptcoef = array([0, 1.77,-0.31])
            
        self._ptbin    = ptbin
        self._ptcoef   = ptcoef
        self._meanpt   = meanpt
        self._phi_dist = [pdf_sampler(lambda phi:self._ptphi_pdf(phi,pt),
                                      linspace(0, 2*pi, 100))
                          for pt in ptbin]

    def _gen_pt(self,eta):
        """Generate transverse momentum

        Parameters
        ----------
        eta : array 
            Pseudorapidity of particles 

        Returns
        -------
        pt : array 
            Transverse momentum of particles
        """
        return self._rnd.exponential(1/self._meanpt,size=eta.shape)

    def _gen_phi(self,eta,pt):
        """Generate azimuth angles

        Parameters
        ----------
        eta : array 
            Pseudorapidity of particles 
        pt : array 
            Transverse momentum of particles

        Returns
        -------
        phi : array 
            Azimuth angle of particles
        """
        from numpy import digitize,minimum,array

        bins = minimum(digitize(pt,self._ptbin),len(self._ptbin)-1)
        rnd  = self._rnd.uniform(size=eta.shape)
        return array([self._phi_dist[bin](u).item() for bin,u in zip(bins,rnd)])
    
    def fill_in(self,header):
        """Fill in information into header 

        Parameters 
        ----------
        header : Header 
            Header to fill information into 
        """
        super(PtPhiW,self).fill_in(header)
        header.ptbin  = self._ptbin
        header.ptcoef = self._ptcoef
        header.meanpt = self._meanpt

    def _ptphi_pdf(self,phi,pt):
        from numpy import arange
        f = (self._ptcoef * pt**arange(len(self._ptcoef))).sum()
        return 1 + f*(self._phi_pdf(phi)-1)
        

# --- With eta and pT ------------------------------------------------
class EtaPtPhiW(PtPhiW):
    r"""Constructor a sampler 

    A sampler generates particles 

    This generates 
 
    * Pseudorapidity :math:`\eta=-\log\tan(\vartheta/2)`
    * Transverse momentum :math:`p_{\mathrm{T}}`
    * Azimuthal angle :math:`\varphi`
    * Weight :math:`w` 

    The pseudorapidity is sampled from an inverse triangular distribution 
    
    .. math::
    
        f_{a,b}(\eta) = a |\eta|+b 

    over the range :math:`-1<\eta<1` with :math:`a` as a parameter and 
    :math:`b=(1-a)/2` to normalise the integral. 

    The transverse momentum is sampled from an exponential distribution 
    
    .. math::
    
        f_{\overline{p_{\mathrm{T}}}} (p_{\mathrm{T}}) 
        = e^{-p_{\mathrm{T}}/\overline{p_{\mathrm{T}}}}

    where :math:`\overline{p_{\mathrm{T}}}` is the average
    transverse momentum chosen.

    The azimuth angle :math:`\varphi` is sampled from a Fourier
    expansion with given flow parameters

    .. math::

        f_{\mathrm{v}}(\varphi) 
        = A(p_{\mathrm{T}})\left[1 + \sum_{n} 2v_n\cos(n\phi)\right]\quad,

    where 

    .. math::
    
         A(p_{\mathrm{T}}) = \sum_{i=1} a_i p_{\mathrm{T}}^{i}

    is a modulation based on the transverse momentum.  By default
    the parameters :math:`\mathbf{a}=\{0,1.77,-0.31\}` tuned to
    result from Pb-Pb at :math:`\sqrt{s_{\mathrm{NN}}} =
    2.76\,\mathrm{T\!e\!V}` (Phys.Rev.Lett. 105 (2010) 252302) so
    that the flow is roughly recovered at the mean transverse
    momentum.
    
    Parameters
    ----------
    rnd : numpy.random.Generator 
        Random number generator 
    vs : array of floats
        Flow coefficents 
    ptbin : array 
        Transverse momentum bins 
    ptcoef : array 
        Transverse momentum coefficients for modulation of 
        azimuth angle distribution 
    meanpt : float 
        Mean transverse momentum
    slope : float 
        Slope of psuedorapidity distribution
    """
    def __init__(self,rnd,vs,ptbin=None,ptcoef=None,meanpt=2,slope=.1,**kwargs):
        from nbi_stat import pdf_sampler
        from numpy import linspace,abs

        super(EtaPtPhiW,self).__init__(rnd,vs,ptbin,ptcoef,meanpt)
        
        self._eta_dist = pdf_sampler(lambda x:slope*abs(x)+(1-slope)/2,
                                     linspace(-1,1,30))

    def _gen_eta(self,m):
        """Generate psuedorapidities

        Parameters
        ----------
        m : int 
            Number of particles to generate 

        Returns
        -------
        eta : array 
            Pseudorapidity of particles 
        """
        return self._eta_dist(self._rnd.uniform(size=m))
        
#
# EOF
#
