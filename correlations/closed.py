# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from . correlator import FromQVector

# --- Closed form correlations ---------------------------------------
class Closed(FromQVector):
    """Calculate correlators using closed-form expression 
    
    Note, the number of particles one can correlate is restricted to 6. 

    This implementation is probably one of the fastest, but it is
    not very flexible and it taked a very long time to compile.  A
    better strategy would probably be to use pre-defined functions
    with recurssion - at least up to some order.

    The internal array meths contains references to methods for
    calculating parts of the correlator given :math:`\mathbf{h}`
    and :math:`p`

    * For integrated measurements, either of 
 
      .. math::
    
          N_{\mathbf{h}} \quad\mathrm{or}
          \quad{D_{\mathbf{h}}=N_{\mathbf{0}}\quad,
 
    * and for differential measurements 
 
      .. math::
    
           N'_{\mathbf{h}} \quad\mathrm{or}
           \quad{D'_{\mathbf{h}}=N'_{\mathbf{0}}\quad.

    Here, 

    * :math:`m` is how many particles to correlate
    * :math:`\mathbf{h}` is the harmonic of each term


    Parameters
    ----------
    qstore : QStore
        Storage of Q-vectors

    """
    def __init__(self,qstore):
        super(Closed,self).__init__(qstore)

        self._meths = [self._qc1,
                       self._qc2,
                       self._qc3,
                       self._qc4,
                       self._qc5,
                       self._qc6]

    @property
    def maxSize(self):
        """Largest number of particles we can correlate"""
        return len(self._meths)

    def qc(self,harmonic_vector):
        """Evaluate the :math:`m`-particle correlator 

        Parameters 
        ----------
        harmonic_vector : sequence of int or PartitionHarmonic 
            The harmonics to use 
        
        Returns 
        -------
        qc : complex 
            The (complex) correlator 
        """
        return self._meths[len(harmonic_vector)-1](*harmonic_vector)

    def _qc1(self,n1,*args):
        """ Generic 1-particle correlation.
        
        Parameters
        ----------
        n1 : int or HarmonicPartition
              
        Returns
        -------
        qc : complex 
            The correlator
        """
        return self._p(n1,1)
    def _qc2(self,n1,n2,*args):
        """ Do the 2-particle calculation.
        Parameters
        ----------
        n1 : int or HarmonicPartition
        n2 : int or HarmonicPartition
          
        Returns
        -------
        qc : complex 
            the correlator
        """
        return self._p(n1,1) * self._r(n2,1) - self._q(n1+n2,2)

    def _qc3(self,n1,n2,n3,*args):
        """ Do the 3-particle calculation.

        Parameters
        ----------
        n1 : int or HarmonicPartition
        n2 : int or HarmonicPartition
        n3 : int or HarmonicPartition
              

        Returns
        -------
        qc : complex 
            the correlator
        """
        k2 = 2
        r = self._r
        p = self._p
        q = self._q
        return (p(n1,1)           * r(n2,1) * r(n3,1)
	        -      q(n1+n2,2) * r(n3,1)
	        -      q(n1+n3,2) * r(n2,1)
	        -      p(n1,1)    * r(n2+n3,2)
	        + k2 * q(n1+n2+n3,3))

    def _qc4(self,n1,n2,n3,n4,*args):
        """ Do the 4-particle calculation.

        Parameters
        ----------
        n1 : int or HarmonicPartition
        n2 : int or HarmonicPartition
        n3 : int or HarmonicPartition
        n4 : int or HarmonicPartition
              
        Returns
        -------
        qc : complex 
            the correlator
        """
        k2 = 2
        k6 = 6
        r = self._r
        p = self._p
        q = self._q
        return (p(n1,1)*r(n2,1)*r(n3,1)*r(n4,1)
	        -      q(n1+n2,2)    * r(n3,1)       * r(n4,1)
	        -      r(n2,1)       * q(n1+n3,2)    * r(n4,1)
	        -      p(n1,1)       * r(n2+n3,2)    * r(n4,1)
	        + k2 * q(n1+n2+n3,3) * r(n4,1)
	        -      r(n2,1)       * r(n3,1)       * q(n1+n4,2)
	        +      r(n2+n3,2)    * q(n1+n4,2)
	        -      p(n1,1)       * r(n3,1)       * r(n2+n4,2)
	        +      q(n1+n3,2)    * r(n2+n4,2)
	        + k2 * r(n3,1)       * q(n1+n2+n4,3)
	        -      p(n1,1)       * r(n2,1)       * r(n3+n4,2)
	        +      q(n1+n2,2)    * r(n3+n4,2)
	        + k2 * r(n2,1)       * q(n1+n3+n4,3)
	        + k2 * p(n1,1)       * r(n2+n3+n4,3)
	        - k6 * q(n1+n2+n3+n4,4))

    def _qc5(self,n1,n2,n3,n4,n5,*args):
        """ Do the 5-particle calculation.

        Parameters
        ----------
        n1 : int or HarmonicPartition
        n2 : int or HarmonicPartition
        n3 : int or HarmonicPartition
        n4 : int or HarmonicPartition
        n5 : int or HarmonicPartition
              
        Returns
        -------
        qc : complex 
            The correlator
        """
        k2  = 2
        k6  = 6
        k24 = 24
        r = self._r
        p = self._p
        q = self._q
        return (p(n1,1)*r(n2,1)*r(n3,1)*r(n4,1)*r(n5,1)
	        -      q(n1+n2,2)    * r(n3,1)    * r(n4,1)    * r(n5,1)
	        -      r(n2,1)       * q(n1+n3,2) * r(n4,1)    * r(n5,1)
	        -      p(n1,1)       * r(n2+n3,2) * r(n4,1)    * r(n5,1)
	        + k2 * q(n1+n2+n3,3) * r(n4,1)    * r(n5,1)
	        -      r(n2,1)       * r(n3,1)    * q(n1+n4,2) * r(n5,1)
	        +      r(n2+n3,2)    * q(n1+n4,2) * r(n5,1)
	        -      p(n1,1)       * r(n3,1)    * r(n2+n4,2) * r(n5,1)
	        +      q(n1+n3,2)    * r(n2+n4,2) * r(n5,1)
	        + k2 * r(n3,1)       * q(n1+n2+n4,3)* r(n5,1)
	        -      p(n1,1)       * r(n2,1)* r(n3+n4,2)     * r(n5,1)
	        +      q(n1+n2,2)    * r(n3+n4,2) * r(n5,1)
	        + k2 * r(n2,1)       * q(n1+n3+n4,3)* r(n5,1)
	        + k2 * p(n1,1)       * r(n2+n3+n4,3)* r(n5,1)
	        - k6 * q(n1+n2+n3+n4,4)* r(n5,1)
	        -      r(n2,1)       * r(n3,1)    * r(n4,1)    * q(n1+n5,2)
	        +      r(n2+n3,2)    * r(n4,1)    * q(n1+n5,2)
	        +      r(n3,1)       * r(n2+n4,2) * q(n1+n5,2)
	        +      r(n2,1)       * r(n3+n4,2) * q(n1+n5,2)
	        - k2 * r(n2+n3+n4,3) * q(n1+n5,2)
	        -      p(n1,1)       * r(n3,1)    * r(n4,1)    * r(n2+n5,2)
	        +      q(n1+n3,2)    * r(n4,1)    * r(n2+n5,2)
	        +      r(n3,1)       * q(n1+n4,2) * r(n2+n5,2)
	        +      p(n1,1)       * r(n3+n4,2) * r(n2+n5,2)
	        - k2 * q(n1+n3+n4,3) * r(n2+n5,2)
	        + k2 * r(n3,1)       * r(n4,1)    * q(n1+n2+n5,3)
	        - k2 * r(n3+n4,2)    * q(n1+n2+n5,3)
	        -      p(n1,1)       * r(n2,1)    * r(n4,1)    * r(n3+n5,2)
	        +      q(n1+n2,2)    * r(n4,1)    * r(n3+n5,2)
                +      r(n2,1)       * q(n1+n4,2) * r(n3+n5,2)
	        +      p(n1,1)       * r(n2+n4,2) * r(n3+n5,2)
	        - k2 * q(n1+n2+n4,3) * r(n3+n5,2)
	        + k2 * r(n2,1)       * r(n4,1)    * q(n1+n3+n5,3)
	        - k2 * r(n2+n4,2)    * q(n1+n3+n5,3)
	        + k2 * p(n1,1)       * r(n4,1)    * r(n2+n3+n5,3)
	        - k2 * q(n1+n4,2)    * r(n2+n3+n5,3)
	        - k6 * r(n4,1)       * q(n1+n2+n3+n5,4)
	        -      p(n1,1)       * r(n2,1)    * r(n3,1)    * r(n4+n5,2)
	        +      q(n1+n2,2)    * r(n3,1)    * r(n4+n5,2)
	        +      r(n2,1)       * q(n1+n3,2) * r(n4+n5,2)
	        +      p(n1,1)       * r(n2+n3,2) * r(n4+n5,2)
	        - k2 * q(n1+n2+n3,3) * r(n4+n5,2)
	        + k2 * r(n2,1)       * r(n3,1)    * q(n1+n4+n5,3)
	        - k2 * r(n2+n3,2)    * q(n1+n4+n5,3)
	        + k2 * p(n1,1)       * r(n3,1)    * r(n2+n4+n5,3)
	        - k2 * q(n1+n3,2)    * r(n2+n4+n5,3)
	        - k6 * r(n3,1)       * q(n1+n2+n4+n5,4)
	        + k2 * p(n1,1)       * r(n2,1)    * r(n3+n4+n5,3)
	        - k2 * q(n1+n2,2)    * r(n3+n4+n5,3)
	        - k6 * r(n2,1)       * q(n1+n3+n4+n5,4)
	        - k6 * p(n1,1)       * r(n2+n3+n4+n5,4)
	        + k24* q(n1+n2+n3+n4+n5,5))

    def _qc6(self,n1,n2,n3,n4,n5,n6,*args):
        """ Do the 6-particle calculation.

        Parameters
        ----------
        n1 : int or HarmonicPartition
        n2 : int or HarmonicPartition
        n3 : int or HarmonicPartition
        n4 : int or HarmonicPartition
        n5 : int or HarmonicPartition
        n6 : int or HarmonicPartition
              

        Returns
        -------
        qc : complex 
            The correlator
        """
        k2   = 2
        k4   = 4
        k6   = 6
        k24  = 24
        k120 = 120
        r = self._r
        p = self._p
        q = self._q
        return \
            (p(n1,1)*r(n2,1)*r(n3,1)*r(n4,1)*r(n5,1)*r(n6,1)
	     -       q(n1+n2,2)    * r(n3,1)    * r(n4,1) * r(n5,1) * r(n6,1)
	     -       r(n2,1)       * q(n1+n3,2) * r(n4,1) * r(n5,1) * r(n6,1)
	     -       p(n1,1)       * r(n2+n3,2) * r(n4,1) * r(n5,1) * r(n6,1)
	     + k2  * q(n1+n2+n3,3) * r(n4,1)    * r(n5,1)       * r(n6,1)
	     -       r(n2,1)       * r(n3,1) * q(n1+n4,2) * r(n5,1) * r(n6,1)
	     +       r(n2+n3,2)    * q(n1+n4,2) * r(n5,1)       * r(n6,1)
	     -       p(n1,1)       * r(n3,1) * r(n2+n4,2) * r(n5,1) * r(n6,1)
	     +       q(n1+n3,2)    * r(n2+n4,2) * r(n5,1)       * r(n6,1)
	     + k2  * r(n3,1)       * q(n1+n2+n4,3) * r(n5,1)       * r(n6,1)
	     -       p(n1,1)       * r(n2,1) * r(n3+n4,2) * r(n5,1) * r(n6,1)
	     +       q(n1+n2,2)    * r(n3+n4,2) * r(n5,1)       * r(n6,1)
	     + k2  * r(n2,1)       * q(n1+n3+n4,3) * r(n5,1)    * r(n6,1)
	     + k2  * p(n1,1)       * r(n2+n3+n4,3) * r(n5,1)    * r(n6,1)
	     - k6  * q(n1+n2+n3+n4,4)            * r(n5,1)       * r(n6,1)
	     -       r(n2,1)       * r(n3,1) * r(n4,1) * q(n1+n5,2) * r(n6,1)
	     +       r(n2+n3,2)    * r(n4,1)    * q(n1+n5,2)    * r(n6,1)
	     +       r(n3,1)       * r(n2+n4,2) * q(n1+n5,2)    * r(n6,1)
	     +       r(n2,1)       * r(n3+n4,2) * q(n1+n5,2)    * r(n6,1)
	     - k2  * r(n2+n3+n4,3) * q(n1+n5,2) * r(n6,1)
	     -       p(n1,1)       * r(n3,1) * r(n4,1) * r(n2+n5,2) * r(n6,1)
	     +       q(n1+n3,2)    * r(n4,1)    * r(n2+n5,2)    * r(n6,1)
	     +       r(n3,1)       * q(n1+n4,2) * r(n2+n5,2)    * r(n6,1)
	     +       p(n1,1)       * r(n3+n4,2) * r(n2+n5,2)    * r(n6,1)
	     - k2  * q(n1+n3+n4,3) * r(n2+n5,2) * r(n6,1)
	     + k2  * r(n3,1)       * r(n4,1)    * q(n1+n2+n5,3) * r(n6,1)
	     - k2  * r(n3+n4,2)    * q(n1+n2+n5,3)               * r(n6,1)
	     -       p(n1,1)       * r(n2,1) * r(n4,1) * r(n3+n5,2) * r(n6,1)
	     +       q(n1+n2,2)    * r(n4,1)    * r(n3+n5,2)    * r(n6,1)
	     +       r(n2,1)       * q(n1+n4,2) * r(n3+n5,2)    * r(n6,1)
	     +       p(n1,1)       * r(n2+n4,2) * r(n3+n5,2)    * r(n6,1)
	     - k2  * q(n1+n2+n4,3) * r(n3+n5,2) * r(n6,1)
	     + k2  * r(n2,1)       * r(n4,1)    * q(n1+n3+n5,3) * r(n6,1)
	     - k2  * r(n2+n4,2)    * q(n1+n3+n5,3)               * r(n6,1)
	     + k2  * p(n1,1)       * r(n4,1)    * r(n2+n3+n5,3) * r(n6,1)
	     - k2  * q(n1+n4,2)    * r(n2+n3+n5,3)               * r(n6,1)
	     - k6  * r(n4,1)       * q(n1+n2+n3+n5,4)            * r(n6,1)
	     -       p(n1,1)       * r(n2,1) * r(n3,1) * r(n4+n5,2) * r(n6,1)
	     +       q(n1+n2,2)    * r(n3,1)    * r(n4+n5,2)    * r(n6,1)
	     +       r(n2,1)       * q(n1+n3,2) * r(n4+n5,2)    * r(n6,1)
	     +       p(n1,1)       * r(n2+n3,2) * r(n4+n5,2)    * r(n6,1)
	     - k2  * q(n1+n2+n3,3) * r(n4+n5,2) * r(n6,1)
	     + k2  * r(n2,1)       * r(n3,1)    * q(n1+n4+n5,3) * r(n6,1)
	     - k2  * r(n2+n3,2)    * q(n1+n4+n5,3)               * r(n6,1)
	     + k2  * p(n1,1)       * r(n3,1)    * r(n2+n4+n5,3) * r(n6,1)
	     - k2  * q(n1+n3,2)    * r(n2+n4+n5,3) * r(n6,1)
	     - k6  * r(n3,1)       * q(n1+n2+n4+n5,4) * r(n6,1)
	     + k2  * p(n1,1)       * r(n2,1)    * r(n3+n4+n5,3) * r(n6,1)
	     - k2  * q(n1+n2,2)    * r(n3+n4+n5,3)               * r(n6,1)
	     - k6  * r(n2,1)       * q(n1+n3+n4+n5,4)            * r(n6,1)
	     - k6  * p(n1,1)       * r(n2+n3+n4+n5,4)            * r(n6,1)
	     + k24 * q(n1+n2+n3+n4+n5,5) * r(n6,1)
	     -       r(n2,1)       * r(n3,1) * r(n4,1) * r(n5,1) * q(n1+n6,2)
	     +       r(n2+n3,2)    * r(n4,1)    * r(n5,1)       * q(n1+n6,2)
	     +       r(n3,1)       * r(n2+n4,2) * r(n5,1)       * q(n1+n6,2)
	     +       r(n2,1)       * r(n3+n4,2) * r(n5,1)       * q(n1+n6,2)
	     - k2  * r(n2+n3+n4,3) * r(n5,1)    * q(n1+n6,2)
	     +       r(n3,1)       * r(n4,1)    * r(n2+n5,2)    * q(n1+n6,2)
	     -       r(n3+n4,2)    * r(n2+n5,2) * q(n1+n6,2)
	     +       r(n2,1)       * r(n4,1)    * r(n3+n5,2)    * q(n1+n6,2)
	     -       r(n2+n4,2)    * r(n3+n5,2) * q(n1+n6,2)
	     - k2  * r(n4,1)       * r(n2+n3+n5,3)               * q(n1+n6,2)
	     +       r(n2,1)       * r(n3,1)    * r(n4+n5,2)    * q(n1+n6,2)
	     -       r(n2+n3,2)    * r(n4+n5,2) * q(n1+n6,2)
	     - k2  * r(n3,1)       * r(n2+n4+n5,3)               * q(n1+n6,2)
	     - k2  * r(n2,1)       * r(n3+n4+n5,3)               * q(n1+n6,2)
	     + k6  * r(n2+n3+n4+n5,4) * q(n1+n6,2)
	     -       p(n1,1)       * r(n3,1) * r(n4,1) * r(n5,1) * r(n2+n6,2)
	     +       q(n1+n3,2)    * r(n4,1)    * r(n5,1)       * r(n2+n6,2)
	     +       r(n3,1)       * q(n1+n4,2) * r(n5,1)       * r(n2+n6,2)
	     +       p(n1,1)       * r(n3+n4,2) * r(n5,1)       * r(n2+n6,2)
	     - k2  * q(n1+n3+n4,3) * r(n5,1)    * r(n2+n6,2)
	     +       r(n3,1)       * r(n4,1)    * q(n1+n5,2)    * r(n2+n6,2)
	     -       r(n3+n4,2)    * q(n1+n5,2) * r(n2+n6,2)
	     +       p(n1,1)       * r(n4,1)    * r(n3+n5,2)    * r(n2+n6,2)
	     -       q(n1+n4,2)    * r(n3+n5,2) * r(n2+n6,2)
	     - k2  * r(n4,1)       * q(n1+n3+n5,3)               * r(n2+n6,2)
	     +       p(n1,1)       * r(n3,1)    * r(n4+n5,2)    * r(n2+n6,2)
	     -       q(n1+n3,2)    * r(n4+n5,2) * r(n2+n6,2)
	     - k2  * r(n3,1)       * q(n1+n4+n5,3)               * r(n2+n6,2)
	     - k2  * p(n1,1)       * r(n3+n4+n5,3)               * r(n2+n6,2)
	     + k6  * q(n1+n3+n4+n5,4)            * r(n2+n6,2)
	     + k2  * r(n3,1)       * r(n4,1)    * r(n5,1)     * q(n1+n2+n6,3)
	     - k2  * r(n3+n4,2)    * r(n5,1)    * q(n1+n2+n6,3)
	     - k2  * r(n4,1)       * r(n3+n5,2) * q(n1+n2+n6,3)
	     - k2  * r(n3,1)       * r(n4+n5,2) * q(n1+n2+n6,3)
	     + k4  * r(n3+n4+n5,3) * q(n1+n2+n6,3)
	     -       p(n1,1)       * r(n2,1) * r(n4,1) * r(n5,1) * r(n3+n6,2)
	     +       q(n1+n2,2)    * r(n4,1)    * r(n5,1)       * r(n3+n6,2)
	     +       r(n2,1)       * q(n1+n4,2) * r(n5,1)       * r(n3+n6,2)
	     +       p(n1,1)       * r(n2+n4,2) * r(n5,1)       * r(n3+n6,2)
	     - k2  * q(n1+n2+n4,3) * r(n5,1)    * r(n3+n6,2)
	     +       r(n2,1)       * r(n4,1)    * q(n1+n5,2)    * r(n3+n6,2)
	     -       r(n2+n4,2)    * q(n1+n5,2) * r(n3+n6,2)
	     +       p(n1,1)       * r(n4,1)    * r(n2+n5,2)    * r(n3+n6,2)
	     -       q(n1+n4,2)    * r(n2+n5,2) * r(n3+n6,2)
	     - k2  * r(n4,1)       * q(n1+n2+n5,3) * r(n3+n6,2)
	     +       p(n1,1)       * r(n2,1)    * r(n4+n5,2)    * r(n3+n6,2)
	     -       q(n1+n2,2)    * r(n4+n5,2) * r(n3+n6,2)
	     - k2  * r(n2,1)       * q(n1+n4+n5,3)               * r(n3+n6,2)
	     - k2  * p(n1,1)       * r(n2+n4+n5,3)               * r(n3+n6,2)
	     + k6  * q(n1+n2+n4+n5,4) * r(n3+n6,2)
	     + k2  * r(n2,1)       * r(n4,1)    * r(n5,1)     * q(n1+n3+n6,3)
	     - k2  * r(n2+n4,2)    * r(n5,1)    * q(n1+n3+n6,3)
	     - k2  * r(n4,1)       * r(n2+n5,2) * q(n1+n3+n6,3)
	     - k2  * r(n2,1)       * r(n4+n5,2) * q(n1+n3+n6,3)
	     + k4  * r(n2+n4+n5,3) * q(n1+n3+n6,3)
	     + k2  * p(n1,1)       * r(n4,1)    * r(n5,1)     * r(n2+n3+n6,3)
	     - k2  * q(n1+n4,2)    * r(n5,1)    * r(n2+n3+n6,3)
	     - k2  * r(n4,1)       * q(n1+n5,2) * r(n2+n3+n6,3)
	     - k2  * p(n1,1)       * r(n4+n5,2) * r(n2+n3+n6,3)
	     + k4  * q(n1+n4+n5,3) * r(n2+n3+n6,3)
	     - k6  * r(n4,1)       * r(n5,1)    * q(n1+n2+n3+n6,4)
	     + k6  * r(n4+n5,2)    * q(n1+n2+n3+n6,4)
	     -       p(n1,1)       * r(n2,1) * r(n3,1) * r(n5,1) * r(n4+n6,2)
	     +       q(n1+n2,2)    * r(n3,1)    * r(n5,1)       * r(n4+n6,2)
	     +       r(n2,1)       * q(n1+n3,2) * r(n5,1)       * r(n4+n6,2)
	     +       p(n1,1)       * r(n2+n3,2) * r(n5,1)       * r(n4+n6,2)
	     - k2  * q(n1+n2+n3,3) * r(n5,1)    * r(n4+n6,2)
	     +       r(n2,1)       * r(n3,1)    * q(n1+n5,2)    * r(n4+n6,2)
	     -       r(n2+n3,2)    * q(n1+n5,2) * r(n4+n6,2)
	     +       p(n1,1)       * r(n3,1)    * r(n2+n5,2)    * r(n4+n6,2)
	     -       q(n1+n3,2)    * r(n2+n5,2) * r(n4+n6,2)
	     - k2  * r(n3,1)       * q(n1+n2+n5,3)               * r(n4+n6,2)
	     +       p(n1,1)       * r(n2,1)    * r(n3+n5,2)    * r(n4+n6,2)
	     -       q(n1+n2,2)    * r(n3+n5,2) * r(n4+n6,2)
	     - k2  * r(n2,1)       * q(n1+n3+n5,3)               * r(n4+n6,2)
	     - k2  * p(n1,1)       * r(n2+n3+n5,3)               * r(n4+n6,2)
	     + k6  * q(n1+n2+n3+n5,4)                             * r(n4+n6,2)
	     + k2  * r(n2,1)       * r(n3,1)    * r(n5,1)     * q(n1+n4+n6,3)
	     - k2  * r(n2+n3,2)    * r(n5,1)    * q(n1+n4+n6,3)
	     - k2  * r(n3,1)       * r(n2+n5,2) * q(n1+n4+n6,3)
	     - k2  * r(n2,1)       * r(n3+n5,2) * q(n1+n4+n6,3)
	     + k4  * r(n2+n3+n5,3) * q(n1+n4+n6,3)
	     + k2  * p(n1,1)       * r(n3,1)    * r(n5,1)     * r(n2+n4+n6,3)
	     - k2  * q(n1+n3,2)    * r(n5,1)    * r(n2+n4+n6,3)
	     - k2  * r(n3,1)       * q(n1+n5,2) * r(n2+n4+n6,3)
	     - k2  * p(n1,1)       * r(n3+n5,2) * r(n2+n4+n6,3)
	     + k4  * q(n1+n3+n5,3) * r(n2+n4+n6,3)
	     - k6  * r(n3,1)       * r(n5,1)    * q(n1+n2+n4+n6,4)
	     + k6  * r(n3+n5,2)    * q(n1+n2+n4+n6,4)
	     + k2  * p(n1,1)       * r(n2,1)    * r(n5,1)     * r(n3+n4+n6,3)
	     - k2  * q(n1+n2,2)    * r(n5,1)    * r(n3+n4+n6,3)
	     - k2  * r(n2,1)       * q(n1+n5,2) * r(n3+n4+n6,3)
	     - k2  * p(n1,1)       * r(n2+n5,2) * r(n3+n4+n6,3)
	     + k4  * q(n1+n2+n5,3) * r(n3+n4+n6,3)
	     - k6  * r(n2,1)       * r(n5,1)    * q(n1+n3+n4+n6,4)
	     + k6  * r(n2+n5,2)    * q(n1+n3+n4+n6,4)
	     - k6  * p(n1,1)       * r(n5,1)    * r(n2+n3+n4+n6,4)
	     + k6  * q(n1+n5,2)    * r(n2+n3+n4+n6,4)
	     -       p(n1,1)       * r(n2,1) * r(n3,1) * r(n4,1) * r(n5+n6,2)
	     +       q(n1+n2,2)    * r(n3,1)    * r(n4,1)       * r(n5+n6,2)
	     +       r(n2,1)       * q(n1+n3,2) * r(n4,1)       * r(n5+n6,2)
	     +       p(n1,1)       * r(n2+n3,2) * r(n4,1)       * r(n5+n6,2)
	     - k2  * q(n1+n2+n3,3) * r(n4,1)    * r(n5+n6,2)
	     +       r(n2,1)       * r(n3,1)    * q(n1+n4,2)    * r(n5+n6,2)
	     -       r(n2+n3,2)    * q(n1+n4,2) * r(n5+n6,2)
	     +       p(n1,1)       * r(n3,1)    * r(n2+n4,2)    * r(n5+n6,2)
	     -       q(n1+n3,2)    * r(n2+n4,2) * r(n5+n6,2)
	     - k2  * r(n3,1)       * q(n1+n2+n4,3)               * r(n5+n6,2)
	     +       p(n1,1)       * r(n2,1)    * r(n3+n4,2)    * r(n5+n6,2)
	     -       q(n1+n2,2)    * r(n3+n4,2) * r(n5+n6,2)
	     - k2  * r(n2,1)       * q(n1+n3+n4,3) * r(n5+n6,2)
	     - k2  * p(n1,1)       * r(n2+n3+n4,3) * r(n5+n6,2)
	     + k6  * q(n1+n2+n3+n4,4)                             * r(n5+n6,2)
	     + k2  * r(n2,1)       * r(n3,1)    * r(n4,1)     * q(n1+n5+n6,3)
	     - k2  * r(n2+n3,2)    * r(n4,1)    * q(n1+n5+n6,3)
	     - k2  * r(n3,1)       * r(n2+n4,2) * q(n1+n5+n6,3)
	     - k2  * r(n2,1)       * r(n3+n4,2) * q(n1+n5+n6,3)
	     + k4  * r(n2+n3+n4,3) * q(n1+n5+n6,3)
	     + k2  * p(n1,1)       * r(n3,1)    * r(n4,1)     * r(n2+n5+n6,3)
	     - k2  * q(n1+n3,2)    * r(n4,1)    * r(n2+n5+n6,3)
	     - k2  * r(n3,1)       * q(n1+n4,2) * r(n2+n5+n6,3)
	     - k2  * p(n1,1)       * r(n3+n4,2) * r(n2+n5+n6,3)
	     + k4  * q(n1+n3+n4,3) * r(n2+n5+n6,3)
	     - k6  * r(n3,1)       * r(n4,1)    * q(n1+n2+n5+n6,4)
	     + k6  * r(n3+n4,2)    * q(n1+n2+n5+n6,4)
	     + k2  * p(n1,1)       * r(n2,1)    * r(n4,1)      * r(n3+n5+n6,3)
	     - k2  * q(n1+n2,2)    * r(n4,1)    * r(n3+n5+n6,3)
	     - k2  * r(n2,1)       * q(n1+n4,2) * r(n3+n5+n6,3)
	     - k2  * p(n1,1)       * r(n2+n4,2) * r(n3+n5+n6,3)
	     + k4  * q(n1+n2+n4,3) * r(n3+n5+n6,3)
	     - k6  * r(n2,1)       * r(n4,1)    * q(n1+n3+n5+n6,4)
	     + k6  * r(n2+n4,2)    * q(n1+n3+n5+n6,4)
	     - k6  * p(n1,1)       * r(n4,1)    * r(n2+n3+n5+n6,4)
	     + k6  * q(n1+n4,2)    * r(n2+n3+n5+n6,4)
	     + k2  * p(n1,1)       * r(n2,1)    * r(n3,1)      * r(n4+n5+n6,3)
	     - k2  * q(n1+n2,2)    * r(n3,1)    * r(n4+n5+n6,3)
	     - k2  * r(n2,1)       * q(n1+n3,2) * r(n4+n5+n6,3)
	     - k2  * p(n1,1)       * r(n2+n3,2) * r(n4+n5+n6,3)
	     + k4  * q(n1+n2+n3,3) * r(n4+n5+n6,3)
	     - k6  * r(n2,1)       * r(n3,1)    * q(n1+n4+n5+n6,4)
	     + k6  * r(n2+n3,2)    * q(n1+n4+n5+n6,4)
	     - k6  * p(n1,1)       * r(n3,1)    * r(n2+n4+n5+n6,4)
	     + k6  * q(n1+n3,2)    * r(n2+n4+n5+n6,4)
	     - k6  * p(n1,1)       * r(n2,1)    * r(n3+n4+n5+n6,4)
	     + k6  * q(n1+n2,2)    * r(n3+n4+n5+n6,4)
	     + k24 * r(n5,1)       * q(n1+n2+n3+n4+n6,5)
	     + k24 * r(n4,1)       * q(n1+n2+n3+n5+n6,5)
	     + k24 * r(n3,1)       * q(n1+n2+n4+n5+n6,5)
	     + k24 * r(n2,1)       * q(n1+n3+n4+n5+n6,5)
	     + k24 * p(n1,1)       * r(n2+n3+n4+n5+n6,5)
	     - k120* q(n1+n2+n3+n4+n5+n6,6))

#
# EOF
#

    
        
    
