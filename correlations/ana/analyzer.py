# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --- Base class -----------------------------------------------------
class Analyzer:
    """Construct analyzer object

    This is a base class for defining analysis classes.  Derived
    classes should at a minimum define the methods event and end.

    Derived classes should implement 

    :meth:`Analyzer.event` 
        Process a single event 

    In addition, if the analyser needs to do some final calculation at
    the end of a pass over data, it can define

    :meth:`Analyzer.end` 
        Final calculations 

    If the analyser should allow for merging states from other jobs, then 
    the analyser can define the method 
    
    :meth:`Analyzer.merge` 
        Merge state from file into current state 
    
    Suppose some analyser class ``MyAnalysis`` has been run 5
    different times - i.e., code like the above have been executed 5 times 

    .. code-block::
   
        g = correlations.gen.Generator.make('',arguments) 
        a = MyAnalysis(arguments) 
        a(100,fini=False,generator=g)

    Assume that the intermediate state was written by
    ``MyAnalysis.end(fini=False)`` to file, and the five output files
    are named ``output_``__i__``.dat``.  Then, we can merge these in a
    new job, and run the final processing (``fini=True``) on the
    merged result

    .. code-block::
    
        a = MyAnalysis(arguments)
        for i in range(5):
             with open(f'output_{i}.dat','r') as file:
                 a(0,file=file,read,fini=(i == 4))

    """
    def __init__(self):
        self._out    = None
        self._header = None

    def pre(self):
        """Pre-event hook

        This method is executed before processing a single event. 
        """
        pass
    
    def event(self,eta,pt,phi,weight):
        r"""Process a single event 

        Parameters 
        ----------
        eta : array 
            Pseudorapidity (:math:`\eta=-\log\tan(\vartheta/2)`) of particles 
        pt : array 
            Transverse momentum (:math:`p_{\mathrm{T}}`) of particles 
        phi : array 
            Azimuth angle (:math:`\varphi`) of particles 
        weights : array 
            Weight (:math:`w`) of particles         
        """
        raise NotImplementedError('Event processing not defined')

    def post(self):
        """Post event hook
        
        This method is executed after event processing"""
        pass 
        

    def end(self,out=None):
        """End of processing 

        Parameters 
        ----------
        out : Output stream 
            Where to put output
        """
        pass

    def __call__(self,nev,
                 fini=True,
                 generator=None,
                 file=None,
                 read=False,
                 out=None):
        """Run the analysis 
        
        Either generator or file must be given 

        Parameters 
        ----------
        nev : int 
            Number of events to process.  If file is given and this is
            less than 1, all events are processed.
        fini : bool 
            If true, finalize calculations 
        generator : Generator 
            Event generator to use 
        file : file 
            File to read from 
        read : bool 
            If true, then read in state from file and merge with current state. 
        """
        self._out = out
        if generator is not None:
            self._from_gen(generator,nev,fini)
        else:
            self._from_file(file,nev,read,fini)

        from sys import stdout
        out = stdout if self._out is None else self._out
        
        if not fini:
            self.dump(out)
        else:
            self.end(out)

    def _from_gen(self,generator,nev,fini):
        """Process from generator 
        
        Parameters 
        ----------
        generator : Generator 
            Event generator to use 
        nev : int 
            Number of events to process.  If file is given and this is
            less than 1, all events are processed.
        fini : bool 
            If true, finalize calculations 
        """
        from sys import stdout
        from . progress import Progress
        
        self._header = generator.header(nev)
        self.dump_header(self._header)
        
        bar = Progress(20)
        for i in range(nev):
            bar(i+1,nev)
            data = generator()
            self.pre()
            self.event(*data)
            self.post()

    def _from_file(self,file,maxev,read,fini):
        """Process events from file 
        
        Parameters
        ----------
        file : file 
            File to read from 
        maxev : int 
            Number of events to process.  If file is given and this is
            less than 1, all events are processed.
        fini : bool 
            If true, finalize calculations 
        read : bool 
            If true, then read in state from file and merge with current state. 
        """
        from .. gen.header import Header
        from . reader import Reader 
        from . progress import Progress
        from sys import stdout, stderr 
        
        if read:
            self.load(file,self._header is not None)
            return 

        self._header     = Header.read(file)
        nev              = self._header.nev
        maxev            = min(maxev,nev) if maxev > 0 else nev
        self._header.nev = maxev

        r   = Reader()
        iev = 0
        bar = Progress(20)
        while maxev == 0 or iev < maxev:
            bar(iev+1,maxev)
            data = r(file)
            if data[0] is None:
                break
            self.pre()
            self.event(*data)
            self.post()
            iev += 1

    @classmethod
    def _fullname(cls):
        return cls.__module__+'.'+cls.__qualname__

    def todict(self):
        """Turn own state into a dictionary.

        Derived classes should overload to convert state into a
        dictionary. For example

        .. code-block::
        
            def todict(self):
                return { 'class': Derived._fullname(),
                         'obj1':  self._obj1.todict() 
                         ... }
        """
        pass

    def fromdict(self,d):
        """Read state from dictionary. 

        Derived classes should overload to convert state into a
        dictionary. For example

        .. code-block::
        
            def fromdict(self,d):
                assert d['class'] == Derived._fullname(),

                self._obj1.fromdict(d['obj1']
                ...

        See also
        --------
        :meth:`todict` 
            Method to turn this into a dictionary 

        Parameters
        ----------
        d : dict 
            Dictionary to decode from 
        """
        pass 

    def load(self,file,append=False):
        """Load state from file

        Parameters
        ----------
        file : File 
            File to read from 
        append : bool 
            If ``True`` append state to current state. Otherwise replace. 
        """
        from .. gen.header import Header
        from json import load
        # from pprint import pprint 

        d = load(file)
        h = Header.make(d['header'])
        
        if append:
            from copy import deepcopy

            old_out = self._out
            self._out = None
            cpy = deepcopy(self)
            cpy.fromdict(d['analyzer'])
            cpy._header = h

            self._out = old_out
            self.merge(cpy)
            
            return
            
        self.fromdict(d['analyzer'])
        self._header = h

    def dump(self,file):
        """Save state to file"""
        from json import dump
        
        d = self.todict()
        if d is None:
            return

        dd = {'analyzer': d,
              'header':   self._header.todict() }

        dump(dd,file,indent=2)

    def write(self,results,file,*,other=None):
        """Write results to a JSON file. 

        Parameters 
        ----------
        results : dict 
            Dictionary of results 
        file : file 
            File to write to 
        other : dict 
            Additional information to add to output
        """
        from json import dump

        d = { 'header':  self._header.todict(),
              'results': results }
        if other is not None:
            d.update(other)
            
        dump(d,file,indent=2)

    def merge(self,other):
        """Merge state from instance

        Exactly what state is and how it is merged is defined by sub-classes
        
        Parameters 
        ----------
        file : file 
            File to read state from 
        """
        self._header = other._header 

    def dump_header(self,header):
        pass 
    
#
# EOF
#
