# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Profile:
    """A profile of some variables.  

    That is, we calculate the density of :math:`x` in each event (a
    histogram normalized to the bin-widths), and then calculate the
    mean density over all events.
    
    Parameters
    ----------
    bins : array 
        Bin edges

    """
    def __init__(self,bins) -> None:
        from nbi_stat import Welford
        
        self._bins = bins
        self._stat = Welford(len(bins)-1,False)


    def fill(self,x):
        """Fill in observations of :math:`x`

        Parameters 
        ----------
        x : array 
            Observations of :math:`x` 
        """
        from nbi_stat import histogram

        w = histogram(x,self._bins,normalize=False)
        self._stat.update(w)

    def todict(self) -> dict:
        """Generate dictionary of distributions"""
        return {'class': Profile.__module__+'.'+Profile.__qualname__,
                'bins':  list(self._bins),
                'means': list(self._stat.mean),
                'uncer': list(self._stat.sem) }

    def fromdict(self,d : dict) -> None:
        """Read from dictionary"""
        assert d['class'] == Profile.__module__+'.'+Profile.__qualname__
        pass

#
# EOF
#

        
        

        
