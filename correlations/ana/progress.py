# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Progress:
    """Progress bar 

    Parameters
    ----------
    barlength : int 
        Length (in characters) of bar
    """
    def __init__(self,barlength,out=None):
        from time import time
        from sys import stderr

        self._b = barlength
        self._l = 0
        self._s = time()
        self._o = stderr if out is None else out

    def _format_time(self,t):
        """Format time

        Parameters 
        ----------
        t : float 
            Elapsed time in seconds 

        Returns
        -------
        txt : str 
            Formatted time 
        """
        hour = int(t // 3600)
        mins = int((t - hour * 3600) // 60)
        secs = (t - mins * 60)
        return f'{hour}:{mins:02d}:{secs:06.3f}'
        
    def __call__(self,current,total):
        """Update progress bar 

        Parameters 
        ---------- 
        current : int 
            Current step 
        total : int 
            Total steps 
        """
        from time import time 
        progress = current / total
        if progress < 1 and progress < self._l + 0.01:
            return

        self._l  = progress
        elap     = time()-self._s
        rem      = elap * (1/progress - 1)
        status   = f"{current:6d} ETA: {self._format_time(rem)}"
        if progress >= 1:
            status = f"{current:6d} took {self._format_time(elap)}\r\n"
            progress = 1

        block = int(round(self._b*progress))
        print(f'\r{"#"*block}{"-"*(self._b-block)} '
              f'{int(100*progress):3d}% {status}',end='',
              file=self._o)


#
# EOF
#
