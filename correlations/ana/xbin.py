# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class XBin:
    r"""A wrapper around a Bin (mostly Full and OfInterest)
    which selects which observations to take into account based values
    of an independent variable (or values of independent variables).  

    Derived classes should overload :meth:`XBin.select` to select
    which observations to include.

    This makes it relatively trivial to define a differential bin.
    For example, for a :math:`p_{\mathrm{T}}` bin one can do

    .. code-block:
        
        class Pt(XBin):
            def __init__(self,integrated,low,high):
                super(Pt,self).__init__(OfInterest(integrated))
                self._low  = low
                self._high = high 

            def select(self,pt):
                from numpy import logical_and 
                
                return logical_and(pt >= low, pt < high)
    
            def todict(self):
                return {'class': Pt._fullname(),
                        'low':   self._low, 
                        'high':  self._high
                        'xbin':  super(X,self).todict() }

            def fromdict(self,d):
                assert d['class'] == Pt._fullname()
        
                self._low  = d['low']
                self._high = d['high']
                super(X,self).fromdict(d)

    which we can then use in an analysis like 

    .. code-block::
    
        def event(self,eta,pt,phi,weight):
    
            self._ibin(phi, weight)
            self._ptbin(pt, phi, weight)

    This will then only update ``_ptbin`` with observations that has
    :math:`p_{\mathrm{T}}` in the range ``low`` up to (but not
    including) ``high``.

    See also
    --------
    XBinned : 
        Several flow bins in disjunctive bins of an independent
        variable :math:`x`

    Parameters
    ----------
    bin : correlations.flow.Reference, correlations.flow.Full, correlations.flow.OfInterest 
        Flow bin to use 
    mark : :attr:`QStore.POI` or :attr:`QStore.REF` or ``0`` 
        Class to assign to selected observations

    """
    def __init__(self,bin,mark=None):
        self._bin  = bin
        self._mark = mark
        self._n    = 0
        self._sumw = 0
        if self._mark is None:
            from .. qvector import QVector
            self._mark = QVector.POI


    @property
    def bin(self):
        """The wrapped bin"""
        return self._bin

    @property
    def n(self):
        """Number of observations"""
        return self._n

    @property
    def w(self):
        """Sum of weights of observations"""
        return self._sumw
    
    def fill(self,x,phi,weight,cls=None,part=None):
        r"""Fill in observations of :math:`\varphi` with weights. 

        Observations to fill in are selected by the method
        :meth:`XBin.select` based on the value of the parameter ``x``.
        Selected observations are labelled as by predefined *mark*
        (e.g., :attr:`Qvector.POI`) in addition to any other previous
        labels (e.g., :attr:`Qvector.REF`).

        The partition argument (if given) is not modified further. 

        Parameters
        ----------
        x : array 
            Independent variable to select on 
        phi : array 
            :math:`\varphi` of each observation 
        weight : array 
            :math:`w` of each observation
        cls : array (optional)
            Class of observation (bit-mask of zero, one, or two of 
            :attr:`Qvector.POI` and :attr:`Qvector.REF`. 
        part : array (optional)
           Partition identifiers of each observation 
        """
        msk = self.select(x)
        if cls is not None:
            poi = (msk * self._mark | cls)[msk]
        else:
            poi = None

        w          =  weight[msk] if weight is not None else None
        p          =  part  [msk] if part   is not None else None
        self._n    += sum(msk)
        self._sumw += sum(w) if w is not None else self._n
        self.bin.fill(phi[msk],w,poi,p)
        
    def select(self,x):
        """Select which observations to accept. 

        Based on the independent variable values ``x``, this method
        must return an array of booleans, of the same size of ``x``,
        with the value ``True`` for each observation to include (the
        default implementation accepts all observations, suitable, for
        example, for integrated flow).

        For example, to filter on ``x`` values between ``xlow`` and
        ``xhigh``, one can do

        .. code-block::
        
            from numpy import logical_and 
            return logical_and(x > xlow, x <= xhigh)

        If we want to select on an exact ``value``, one can do 

        .. code-block::

            return x == value 

        or possible ``values`` (list or array)

        .. code-block::
        
            from numpy import isin
            return isin(x,values)

        Parameters
        ----------
        x : array shape=(N,...)
             Array of independent variable values of size 
        
        Returns
        -------
        mask : array of bool shape=(N,)
            Array of booleans with ``True`` for selected entries.

        """
        from numpy import full_like
        return full_like(x,True,dtype=bool)

    def update(self):
        """End-of-event update"""
        self.bin.update()

    def reset(self):
        """Before-event reset"""
        self.bin.reset()
        self._n    = 0
        self._sumw = 0

    def __call__(self):
        """Compute final result"""
        return self.bin.__call__()

    @classmethod
    def _fullname(cls):
        return cls.__module__+'.'+cls.__name__
    
    def todict(self):
        """Convert this to a dictionary.  Note, derived classes should
        overload this to store selection parameters and then call the
        super class for appropriate inclusion.  For example

        .. code-block:
        
            class X(XBin):
                ...
                def todict(self):
                    return {'class': X._fullname(),
                            'low':   self._low, 
                            'high':  self._high
                            'xbin':  supert(X,self).todict() }
        
        Of course, this needs to be balanced by a similar
        XBin.fromdict method.
        
        Returns
        -------
        d : dict 
            Dictionary representation
        """
        return { 'class': XBin._fullname(),
                 'bin': self.bin.todict() }

    def fromdict(self,d):
        """Assign from a dictionary.  Note, derived classes shoud overload
        this to decode selection parameters and then call the super
        class for appropriate inclussion.  For example

        .. code-block:
        
            class X(XBin):
                ...
                def fromdict(self,d):
                    assert d['class'] == X._fullname()
        
                    self._low  = d['low']
                    self._high = d['high']
                    super(X,self).fromdict(d)
        
        Parameters
        ----------
        d : dict 
            Dictionary to decode from 
        """
        assert d['class'] == XBin._fullname()

        self.bin.fromdict(d['bin'])

    def merge(self,other):
        """Merge state of other XBin object into this"""
        assert isinstance(other,XBin)
        self.bin.merge(other.bin)

    def format(self,*,x=None,fw=8,prec=4,header=False,n=None,dn=None):
        if header:
            v = lambda m,n : f'V{n}{m}'
            d = lambda m,n : f'DV{n}{m}'
            s = ''
            if n is not None and dn is not None:
                s += f'{"N":>{fw}s} {"DN":>{fw}s} '

            s += ' '.join([f'{v(n,m):>{fw}s} {d(n,m):>{fw}s}'
                           for m in range(2,self.bin.maxM+2,2)
                           for n in range(1,self.bin.maxN+1)])
            return s

        res = self()
        def v(m,n):
            v = res[n][m]
            return f'{v[0]:{fw}.{prec}f} {v[1]:{fw}.{prec}f}'

        s = ''
        if n is not None and dn is not None:
            s = f'{n:{fw}.{prec}f} {dn:{fw}.{prec}f} '
        s += ' '.join([v(m,n)
                       for m in range(2,self.bin.maxM+2,2)
                       for n in range(1,self.bin.maxN+1)])
        return s
        
    def print(self,out=None,*,x=None,fw=8,prec=4,n=None,dn=None):
        from sys import stderr

        if out is None:
            out = stderr

        print('\n'.join([self.format(x=x,fw=fw,prec=prec,n=n,dn=dn,header=True),
                         self.format(x=x,fw=fw,prec=prec,n=n,dn=dn,header=False)]),
              file=out)
        
        
        
# --- Bin between boundaries -----------------------------------------
class LowHigh(XBin):
    """A bin in the independent variable between low and high values

    Parameters 
    ----------
    low : float 
        Lower (inclusive) edge of bin :math:`l`
    high : float 
        Upper (inclusive if ``right=True``, exclusive otherwise) 
        edge of the bin :math:`h`
    bin : Bin 
        Encapsulated bin 
    right : bool 
        If ``True``, then the ``high`` edge is included in the bin 
    """
    def __init__(self,low,high,bin,right=False):
        from numpy import less, less_equal
        super(LowHigh,self).__init__(bin)
        self._low   = low
        self._high  = high
        self._right = less_equal if right else less

    @property 
    def low(self):
        r"""Lower edge :math:`l`"""
        return self._low

    @property
    def high(self):
        r"""High edge :math:`h`"""
        return self._high

    @property
    def mid(self):
        """Middle of bin"""
        return (self.high + self.low) / 2

    @property
    def width(self):
        """Width of bin"""
        return (self.high - self.low)

    def select(self,x):
        r"""Selects observations where 

        .. math::
        
            l \le x \lt h

        or 

        .. math::
        
            l \le x \le h 

        if `right` was `True` when creating this object 
        
        Parameters 
        ----------
        x : array shape=(N,)
            observations of the independent variable 

        Returns 
        -------
        mask : array shape(N,) bool
            Mask of entries that meets the above condition 
        """
        from numpy import logical_and, less_equal

        return logical_and(less_equal(self._low,x),
                           self._right(x,self._high))

    def todict(self):
        """Turn this into a dictionary"""
        return {'class': LowHigh._fullname(),
                'low':   self._low,
                'high':  self._high,
                'xbin':  super(LowHigh,self).todict() }
    
    def fromdict(self,d):
        """Read state from dictionary

        Parameters
        ----------
        d : dict 
            Dictionary 
        """
        assert d['class'] == LowHigh._fullname()
        
        self._low  = d['low']
        self._high = d['high']
        super(LowHigh,d).fromdict(d['xbin'])

        
    def format(self,*,x=None,fw=8,prec=4,header=False,n=None,dn=None):
        if header:
            s = f'{x:>{fw}s} {"D"+x:>{fw}s} '
        else:
            s = f'{self.mid:{fw}.{prec}f} {self.width:{fw}.{prec}f} '
            
        s += super(LowHigh,self).format(x=x,fw=fw,prec=prec,header=header,
                                        n=n,dn=dn)
        return s

#
# EOF
#
