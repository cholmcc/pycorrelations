# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .  analyzer   import Analyzer
from .. flow.bin   import Reference
from .  xbin       import XBin
from .. correlator import Correlator
from .. stat       import Policy

class Integrated(Analyzer):
    r"""Analyse all observations for integrated flow

    That is, we accept all particles (with weights if enabled) and
    calculate the :math:`Q`-vector for each event.  Then, using the
    chosen correlator algorithm, we calculate all needed correlators
    :math:`C_{\mathbf{n}}\{m\}` - where :math:`m` varies from 2 to the
    chosen maximum, and :math:`\mathbf{n}=\{-n,\ldots,n\}` up to the
    chosen maximum - and update our means and covariances of these
    (using West's algorithm of online updates as implemented in
    :class:`nbi_stat.West`).

    At the end of the analysis, we calculate the needed cumulants
    :math:`c_{\mathbf{n}}\{m\}` and from that the requested flow
    coefficents :math:`v_{n}\{m\}`.

    .. image:: ../static/gen_phi_medium_intg.png

    The plot above shows an example with input 

    .. math::
    
        v_1 &=& 0.02\\
        v_2 &=& 0.07\\
        v_3 &=& 0.03 

    and using full error propagation
    (:class:`correlations.stat.Derivatives`).

    This uses the service class :class:`correlations.ana.XBin` to
    store the calculations.  This in turn uses the class
    :class:`correlations.flow.Reference` to tie :math:`Q`-vector,
    correlator, and estimator :class:`correlations.flow.VNM` together
    in a simple interface.

    Parameters
    ----------
    maxN : int 
         Largest :math:`n` to calculate flow for 
    maxM : int 
         Largest :math:`n` to calculate flow for 
    weights : bool 
         Whether to use weights or not 
    policy : Policy 
         Policy *type* instance for uncertainty calculations 
    partitions : sequence of identifiers 
         List of partition identifiers 
    correlator : Correlator 
         Correlator *type* instance for correlator calculations 
    """
    def __init__(self,
                 maxN       : int,
                 maxM       : int,
                 weights    : bool        = True,
                 *,
                 policy     : Policy      = None,
                 partitions               = None,
                 correlator : Correlator  = None) -> None:
        super(Integrated,self).__init__()

        pol     = Policy    .type(policy)
        corr    = Correlator.type(correlator)
        self._b = XBin(Reference(maxN,maxM,weights,
                                 partitions=partitions,
                                 policy=pol,
                                 correlator=corr))
        self._o = {'policy':     policy,
                   'correlator': correlator }
        
    def pre(self) -> None:
        """Pre-event processing.  Clears internal structures"""
        self._b.reset()

    def event(self,eta,pt,phi,weight) -> None:
        r"""Process an event

        Parameters 
        ----------
        eta : array 
            Pseudorapidity (:math:`\eta=-\log\tan(\vartheta/2)`) of particles 
        pt : array 
            Transverse momentum (:math:`p_{\mathrm{T}}`) of particles 
        phi : array 
            Azimuth angle (:math:`\varphi`) of particles 
        weights : array 
            Weight (:math:`w`) of particles         
        """
        self._b.fill(phi,phi,weight)

    def post(self) -> None:
        """Post event processing.  Updates statistics of mean 
        correlators"""
        self._b.update()

    def todict(self) -> dict:
        """Get dictionary of state"""
        d = { 'class': Integrated._fullname(),
              'state': self._b.todict() }
        # from pprint import pprint
        # pprint(d)
        return d

    def fromdict(self,d) -> None:
        """Initialize state from dictionary"""
        assert d['class'] == Integrated._fullname()

        self._b.fromdict(d['state'])

    def merge(self,other) -> None:
        """Merge with other analyzer"""
        assert isinstance(other,Integrated)
        super(Integrated,self).merge(other)
        
        self._b.merge(other._b)
        
    def end(self,file=None) -> None:
        """Writes results to JSON file"""
        self.write(self._b(),file, other=self._o)

#
# EOF
#

        

        

        
