# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from . analyzer    import Analyzer
from .. qvector    import QVector, QStore
from .. correlator import Correlator
from .. harmonic   import HP

class Benchmark(Analyzer):
    r"""Benchmark correlator
    
    .. image:: ../static/gen_phi_small_corr.png 

    The plot above compares results from using all 3 different kinds
    of correlator algorithms (left) and the time complexity (right).

    Parameters
    ----------
    base : str 
        Base of output filenames 
    rnd : numpy.random.Generator 
        Random number generator 
    maxM : int 
        Largest number of particles to correlate 
    harmonic_vector : array 
        Array of harmonics to use 
    weights : bool 
        If true, use weights 
    q : QVector 
        :math:`Q`-vector to use 
    correlator : Correlator 
        Correlator implementation

    """
    def __init__(self,
                 mode,
                 rnd,
                 maxM=5,
                 harmonics=None,
                 partitions=None,
                 weights=True,):
        from nbi_stat import Welford
        from numpy import unique
        
        super(Benchmark,self).__init__()

        self._mode = mode
        if harmonics is None:
            harmonics = rnd.integers(-6,6,size=maxM)

        if partitions is not None and len(part) != len(harmonics):
            partitions = rnd.choice(['A','B','C'])

        if partitions is not None:
            harmonics = [HP(h,p) for h,p in zip(harmonics,partitions)]

        self._qstore = QStore.make(partitions is not None,
                                   harmonics=harmonics,
                                   partitions=partitions,
                                   weights=weights)
        self._correlator = Correlator.make(mode,self._qstore)
        self._times      = Welford(len(harmonics))
        self._rnd        = rnd
        self._harmonics  = harmonics
        self._partitions = partitions
        self._weights    = weights
        self._results    = None

    def pre(self):
        self._qstore.reset()
        
    def event(self,eta,pt,phi,weight):
        """Process a single event 

        Parameters 
        ----------
        eta : array 
            Pseudorapidity of particles 
        pt : array 
            Transverse momentum of particles 
        phi : array 
            Azimuth angles of particles 
        weight : array 
            Weights of particles         
        """
        self.calculate(self.fill(phi,weight))

    def fill(self,phi,weight):
        """Fill in observations 
        
        This keeps track of how long it takes to fill the data in.
        
        Parameters
        ----------
        phi : array 
            Azimuth angles 
        weight : array 
            Weights 

        Returns
        -------
        fillt : float 
            How long it took to fill in the data 
        """
        from time import time
        start = time()
        self._qstore.reset()

        fphi    = phi
        fweight = weight
        fcls    = None if self._partitions is None else \
            rnd.choice([0x1,0x2,0x3],len(weight))
        fpart   = None if self._partitions is None else \
            rnd.choice(self._partitions,size=len(weight))
        if self._weights:
            mask    = self._rnd.uniform(size=len(weight)) < 1/weight
            fphi    = phi   [mask]
            fweight = weight[mask]
            fcls    = fcls  [mask] if fcls  is not None else None
            fpart   = fpart [mask] if fpart is not None else None

        self._qstore.fill(fphi,fweight,fcls,fpart)

        return time()-start
        
    def calculate(self,fillt):
        """Do the calculations after filling in data.

        This calculates correlators upto (and including) the specified
        maximum number of particles.  It keeps track of how long each
        calculation took.

        Parameters
        ----------
        fillt : float 
            How long it took to fill in the data (in seconds)
        """
        from time import time
        from numpy import array
        
        def one(m):
            start = time();
            return self._correlator(self._harmonics,m).eval(), time()-start

        res           = [one(m) for m in range(2,len(self._times)+1)]
        self._results = [0+0j]+[e[0] for e in res]
        self._times.fill(array([fillt]+[e[1] for e in res]))

    def todict(self):
        ns = [0]+list(range(2,2+len(self._results)))
        return { 'mode': self._mode,
                 'times': {n: {'real':r.real,
                               'imag':r.imag,
                               't':   t,
                               'dt':  dt }
                           for n,r,t,dt in zip(ns,
                                               self._results,
                                               self._times.mean,
                                               self._times.sem)} }

    def tostring(self):
        ws = 12
        ns = [0]+list(range(2,2+len(self._results)))
        s  = [f'{"N":>{ws}}{"C":>{2*ws+2}}{"T":>{ws}}{"DT":>{ws}}'] + \
            [f'{n:{ws}d}{r:{2*ws+2}f}{t:{ws}f}{dt:{ws}f}' 
             for n,r,t,dt in zip(ns,
                                 self._results,
                                 self._times.mean,
                                 self._times.sem)]
        return '\n'.join(s)
        
    def end(self,file=None):
        """End of run.  Output the benchmark"""
        self.write(self.todict(),file)
        from sys import stderr

        print(self.tostring(),file=stderr)

#
# EOF
#
