# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Package of analysis related classes and functions"""

from . reader       import Reader
from . analyzer     import Analyzer
from . xbin         import XBin, LowHigh
from . xbinned      import XBinned
from . writer       import Writer
from . benchmark    import Benchmark
from . integrated   import Integrated
from . differential import Differential
from . profile      import Profile
import correlations.ana.tools 

#
# EOF
#
