# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .  analyzer   import Analyzer
from .. flow.bin   import Reference 
from .. flow.bin   import OfInterest
from .  xbinned    import XBinned
from .  xbin       import LowHigh
from .. correlator import Correlator
from .. stat       import Policy

class Differential(Analyzer):
    r"""Example of a differential analysis. 

    This analyzer will do differential analysis versus the transverse
    momentum.

    That is, we accept particles (with weights if enabled) with a
    transverse momentum in some range and calculate the
    :math:`Q`-vectors :math:`p,r,q` for each event.  Then, using the
    chosen correlator algorithm, we calculate all needed correlators
    :math:`C'_{\mathbf{n}}\{m\}` - where :math:`m` varies from 2 to
    the chosen maximum, and :math:`\mathbf{n}=\{-n,\ldots,n\}` up to
    the chosen maximum - and update our means and covariances of these
    (using West's algorithm of online updates as implemented in
    :class:`nbi_stat.West`).

    At the end of the analysis, we calculate the needed cumulants
    :math:`d_{\mathbf{n}}\{m\}` and from that the requested flow
    coefficents :math:`v'_{n}\{m\}`.

    .. image:: ../static/gen_pt_medium_diff.png

    The plot above shows an example with input 

    .. math::
    
        v_1 &=& 0.02\\
        v_2 &=& 0.07\\
        v_3 &=& 0.03 

    and transverse momentum modulation 

    .. math::
    
        v'_n(p_{\mathrm{T}}) 
        = v_n\left(1.77p_{\mathrm{T}} - 0.31p_{\mathrm{T}}^2\right)

    and using full error propagation
    (:class:`correlations.stat.Derivatives`).

    This uses the service class :class:`correlations.ana.XBinned` to
    store the calculations.  This in turn uses the class
    :class:`correlations.flow.Full` to tie :math:`Q`-vector,
    correlator, and estimator :class:`correlations.flow.VPNM` together
    in a simple interface.

    Parameters
    ----------
    maxN : int 
         Largest :math:`n` to calculate flow for 
    maxM : int 
         Largest :math:`n` to calculate flow for 
    weights : bool 
         Whether to use weights or not 
    ptbins : array 
         Bin boundaries for each flow bin
    policy : Policy 
         Policy *type* instance for uncertainty calculations 
    correlator : Correlator 
         Correlator *type* instance for correlator calculations

    """
    def __init__(self,
                 maxN      : int,
                 maxM      : int,
                 weights   : bool        = True,
                 ptbins                  = None,
                 *,
                 policy     : Policy     = None,
                 partitions              = None,
                 correlator : Correlator = None) -> None:
        super(Differential,self).__init__()

        pol     = Policy    .type(policy)
        corr    = Correlator.type(correlator)
        self._i = LowHigh(ptbins[0], ptbins[-1],
                             Reference(maxN,maxM,weights,
                                       policy=pol,
                                       partitions=partitions,
                                       correlator=corr))
        self._b = XBinned.make(ptbins,OfInterest,reference=self._i.bin)
        self._o = {'policy':     policy,
                   'correlator': correlator }

    def pre(self) -> None:
        """Pre-event processing.  Resets internal data"""
        self._i.reset()
        self._b.reset()

    def event(self,eta,pt,phi,weight) -> None:
        """Event processing.  Classifies particles based on their 
        transverse momentum"""
        from .. qvector import QVector
        from numpy import full_like
        
        cls = full_like(pt,QVector.REF,dtype=int)

        self._i.fill(pt,phi,weight,cls)
        self._b.fill(pt,phi,weight,cls)

    def post(self) -> None:
        """Post event processing.  Calculates correlators and updates
        statistics.""" 
        self._i.update()
        self._b.update()

    def todict(self) -> dict:
        """Get state as a dictionary"""
        return { 'class': Differential._fullname(),
                 'int'  : self._i.todict(),
                 'diff' : self._b.todict() }

    def fromdict(self,d) -> None:
        """Initialize state from dictionary"""
        assert d['class'] == Differential._fullname()

        self._i.fromdict(d['int'])
        self._d.fromdict(d['diff'])


    def merge(self,other) -> None:
        """Merge with other instance"""
        assert isinstance(other,Differential)
        super(Integrated,self).merge(other)

        self._i.merge(other._i)
        self._b.merge(other._b)

    def end(self,file=None) -> None:
        self.write(self._b(),file, other=self._o)
        self._i.print(x='ALL',n=-1,dn=-1)
        self._b.print(x='PT')
        
#
# EOF
#




    
    
