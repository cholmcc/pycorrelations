# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Reader:
    """Read data from a file"""
    def __init__(self):
        pass

    def __call__(self,file):
        """Read a single event from file 

        Parameters
        ----------
        file : file object 
            File to read from 

        Returns
        -------
        eta : array 
            Pseudorapidity of all particles 
        pt : array 
            Transverse momentum of all particles 
        phi : array 
            Azimuth angle of all particles (radians) 
        weight : array 
            Weight of all particles 
        
        If no more data is available, None is returned for each of the
        four fields above.
        """
        from numpy import array

        l = file.readline();
        if len(l) <= 0:
            return None,None,None,None
        
        n    = int(l)
        data = array([[float(f) for f in file.readline().split()]
                      for _ in range(n)])
        
        return data[:,0],data[:,1],data[:,2],data[:,3]
    
#
# EOF
#
