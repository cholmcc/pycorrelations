# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .  integrated   import Integrated   as IntgBase
from .  differential import Differential as DiffBase
from .. stat         import Policy
from .. correlator   import Correlator

# --- Utility to partition events ------------------------------------
class Partitioner:
    def __init__(self,maxM,deltaEta):
        k           = maxM//2
        self._delta = deltaEta
        self._ids   = ['-']*k + ['+']*k

    @property 
    def ids(self):
        return self._ids

    def __call__(self,eta,pt,phi,weight):
        """Partition event 
        
        We select observations with 

        .. math::

            |\eta|>\Delta\eta/2

        and then assign partitions as 

        .. math::
        
            \begin{cases}
            - & \eta < -\Delta\eta/2
            + & \eta > +\Delta\eta/2 
            \end{cases}

        Parameters 
        ----------
        eta : array 
            Pseudorapidity (:math:`\eta=-\log\tan(\vartheta/2)`) of particles 
        pt : array 
            Transverse momentum (:math:`p_{\mathrm{T}}`) of particles 
        phi : array 
            Azimuth angle (:math:`\varphi`) of particles 
        weights : array 
            Weight (:math:`w`) of particles         

        Returns
        -------
        eta : array 
            Pseudorapidity (:math:`\eta=-\log\tan(\vartheta/2)`) of
            selected particles
        pt : array 
            Transverse momentum (:math:`p_{\mathrm{T}}`) of selected
            particles
        phi : array 
            Azimuth angle (:math:`\varphi`) of selected particles 
        weights : array 
            Weight (:math:`w`) of selected particles
        part : array 
            Assigned partition of selected particles 
        """
        from numpy import logical_or, where, full_like
        
        msk  = logical_or(eta < -self._delta/2,
                          eta >  self._delta/2)
        part = where(eta[msk] < self._delta/2, '-', '+')
        
        return eta[msk], pt[msk], phi[msk], weight[msk], part

# --- Integrated flow ------------------------------------------------    
class Integrated(IntgBase):
    r"""Partitioned integrated flow.  We partition the events into two
    sub-events:

    * Observations with :math:`\eta < -\Delta\eta/2`
    * Observations with :math:`\eta > +\Delta\eta/2` 

    We labelled the partitions ``-`` and ``+`` respectively. 

    Parameters
    ----------
    deltaEta : float 
         Separation between sub-events 
    maxN : int 
         Largest :math:`n` to calculate flow for 
    maxM : int 
         Largest :math:`n` to calculate flow for 
    weights : bool 
         Whether to use weights or not 
    policy : Policy 
         Policy *type* instance for uncertainty calculations 
    partitions : sequence of identifiers 
         List of partition identifiers 
    correlator : Correlator 
         Correlator *type* instance for correlator calculations 
    """
    def __init__(self,
                 deltaEta   : float,
                 maxN       : int,
                 maxM       : int,
                 weights    : bool        = True,
                 *,
                 policy     : Policy      = None,
                 correlator : Correlator  = None) -> None:
        self._partitioner  = Partitioner(maxM,deltaEta)
        super(Integrated,self).__init__(maxN,maxM,weights,
                                        policy=policy,
                                        partitions=self._partitioner.ids,
                                        correlator=correlator)
        self._o['eta_gap'] = deltaEta

    def event(self,eta,pt,phi,weight) -> None:
        r"""Process an event

        Parameters 
        ----------
        eta : array 
            Pseudorapidity (:math:`\eta=-\log\tan(\vartheta/2)`) of particles 
        pt : array 
            Transverse momentum (:math:`p_{\mathrm{T}}`) of particles 
        phi : array 
            Azimuth angle (:math:`\varphi`) of particles 
        weights : array 
            Weight (:math:`w`) of particles         
        """
        peta, ppt, pphi, pweight, part = self._partitioner(eta,pt,phi,weight)

        self._b.fill(pphi,pphi,pweight,None,part)
        # print(self._b.bin._r.rvec['-'])
        # print(self._b.bin._r.rvec['+'])


# --- Differential flow ------------------------------------------------    
class Differential(DiffBase):
    r"""Partitioned differential flow.  We partition the events into two
    sub-events:

    * Observations with :math:`\eta < -\Delta\eta/2`
    * Observations with :math:`\eta > +\Delta\eta/2` 

    We labelled the partitions ``-`` and ``+`` respectively. 

    Parameters
    ----------
    deltaEta : float 
         Separation between sub-events 
    maxN : int 
         Largest :math:`n` to calculate flow for 
    maxM : int 
         Largest :math:`n` to calculate flow for 
    weights : bool 
         Whether to use weights or not 
    policy : Policy 
         Policy *type* instance for uncertainty calculations 
    partitions : sequence of identifiers 
         List of partition identifiers 
    correlator : Correlator 
         Correlator *type* instance for correlator calculations 
    """
    def __init__(self,
                 deltaEta   : float,
                 maxN       : int,
                 maxM       : int,
                 weights    : bool       = True,
                 ptbins                  = None,
                 *,
                 policy     : Policy      = None,
                 correlator : Correlator  = None) -> None:
        self._partitioner  = Partitioner(maxM,deltaEta)
        super(Differential,self).__init__(maxN,maxM,weights,ptbins,
                                          policy=policy,
                                          partitions=self._partitioner.ids,
                                          correlator=correlator)
        self._o['eta_gap'] = deltaEta

    def event(self,eta,pt,phi,weight) -> None:
        r"""Process an event

        Parameters 
        ----------
        eta : array 
            Pseudorapidity (:math:`\eta=-\log\tan(\vartheta/2)`) of particles 
        pt : array 
            Transverse momentum (:math:`p_{\mathrm{T}}`) of particles 
        phi : array 
            Azimuth angle (:math:`\varphi`) of particles 
        weights : array 
            Weight (:math:`w`) of particles         
        """
        from .. qvector import QVector
        from numpy import full_like
        
        peta, ppt, pphi, pweight, part = self._partitioner(eta,pt,phi,weight)
        cls = full_like(ppt,QVector.REF,dtype=int)
        
        self._i.fill(ppt,pphi,pweight,cls,part)
        self._b.fill(ppt,pphi,pweight,cls,part)


#
# EOF
#




    
    
