# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from .  analyzer   import Analyzer

class Distributions(Analyzer):
    """Analyze data for distributions. 

    We build the distributions 

    .. math::
    
        \frac{\mathrm{d}N}{\mathrm{d}\eta}\\
        \frac{\mathrm{d}N}{\mathrm{d}p_{\mathrm{T}}}\\
        \frac{\mathrm{d}N}{\mathrm{d}\varphi}\\
        \frac{\mathrm{d}N}{\mathrm{d}w}

    """
    def __init__(self,eta_bins,pt_bins,phi_bins,w_bins):
        from numpy import linspace, pi 
        super(Distributions,self).__init__()
        
        self._dndeta = self._profile(eta_bins,linspace(-1,1,11))
        self._dndpt  = self._profile(pt_bins, linspace(0,5,25))
        self._dndphi = self._profile(phi_bins,linspace(0,2*pi,30))
        self._dndw   = self._profile(w_bins,  linspace(0,1,100))
        
    def _profile(self,bins,default):
        from . profile import Profile
        from numpy import linspace, atleast_1d

        b = atleast_1d(bins)
        if bins is None:
            b = default
        elif len(b) == 1:
            b = linspace(default[0],default[-1],int(b[0]+1))
        elif len(b) == 2:
            b = linspace(b[0],b[1],len(default))
        elif len(b) == 3:
            b = linspace(b[0],b[1],int(b[2]+1))

        return Profile(b)
    
    def event(self,eta,pt,phi,weight):
        self._dndeta.fill(eta)
        self._dndpt .fill(pt)
        self._dndphi.fill(phi)
        self._dndw  .fill(weight)


    def todict(self):
        return {'dndeta': self._dndeta.todict(),
                'dndpt':  self._dndpt.todict(),
                'dndphi': self._dndphi.todict(),
                'dndw':   self._dndw.todict() }

    def end(self,file=None):
        self.write(self.todict(),file)

#
# EOF
#

                

        
