# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class XBinned:
    r"""Analysis using bins

    An independent variable :math:`x` is binned in disjunctive bins,
    each with an associated :class:`correlations.flow.Bin` (either
    :class:`correlations.flow.Reference`,
    :class:`correlations.flow.Full`, or (more effective)
    :class:`correlations.flow.OfInterest`).

    See also
    --------
    :class:`XBin` : 
        A wrapper around :class:`correlations.flow.Bin` for a single bin. 

    Parameters 
    ----------
    binedges : array shape=(N+1,)
        Array of bin edges (e.g., ``np.linspace(0,5,11)`` for 10 bins
        from 0 to 5)
    inners : array shape=(N,1)
        Inner bins (e.g., :class:`correlations.flow.Full`)

    """
    def __init__(self,binedges,inners):
        from . xbin import LowHigh
        from nbi_stat import Welford

        assert len(binedges)-1 == len(inners)
        
        self._bins = [LowHigh(l,h,i,right=h is binedges[-1])
                      for l, h, i in zip(binedges[:-1],binedges[1:],inners)]
        self._binedges = binedges
        self._profile  = Welford(len(self._bins), False)

    @classmethod
    def make(cls,binedges,bintype,*args,**kwargs):
        """Create a binned object using an inner bin type and arguments 

        Parameters
        ----------
        binedges : array shape=(N+1,)
            Array of bin edges (e.g., ``np.linspace(0,5,11)`` for 10
            bins from 0 to 5)
        bintype : type
            Type of inner bin (e.g.,
            :class:`correlations.flow.OfInterest`)
        *args : tuple 
            Arguments for each call to ``bintype`` constructor 
        **kwargs : dict 
            Keyword arguments to ``bintype`` constructor 
        
        Returns
        -------
        binned : XBinned 
            A new binned object with N bins 
        """
        return cls(binedges,[bintype(*args,**kwargs) for _ in binedges[:-1]])

    def reset(self):
        """Reset all bins"""
        for b in self._bins: b.reset()

    def fill(self,x,phi,weight,cls=None,part=None):
        r"""Fill observations of :math:`\varphi` with possible weights
        :math:`w`.  

        In addition, each observation can be classified as reference
        (:attr:`correlations.QVector.REF`), of-interest 
        (:attr:`correlations.QVector.POI`), both,
        or nothing via the argument ``cls``.

        Furthermore, we can label each observation by a partition
        identifier in the argument ``part``

        Parameters
        ----------
        x : array, shape=(N,1)
            Independent variable values.  Bins are selected based on
            this value
        phi : array shape=(N,1)
            :math:`\varphi` observations 
        weight : array, shape=(N,1)
            Weights :math:`w` of :math:`\varphi` observations. 
        cls : array, shape=(N,1) (optional)
            Classification of each observation 
        part : array, shape=(N,1) (optional) 
            Partition identifiers of each observation 
        """
        for b in self._bins: b.fill(x,phi,weight,cls,part)

    def update(self):
        """Do end-of-event updates"""
        from numpy import array

        cnt = array([b.w for b in self._bins])
        self._profile.fill(cnt)
        for b in self._bins:
            b.update()

    def __call__(self):
        """Get final results.  This returns both the profile of number of
        observations in each bin, as well as the bin values.

        Returns
        ------- 
        result : dict 
            A dictionary of results 

            .. code-block::
        
                { 'profile': { 'mean': list-of-means,
                               'uncer': list-of-standard-error-on-mean },
                  'edges': binedges,
                  'values': list-of-bin-values }
        
        """
        return { 'profile': { 'mean':  list(self._profile.mean),
                              'uncer': list(self._profile.sem) },
                 'edges': list(self._binedges),
                 'values': [b() for b in self._bins] }
    
    @classmethod
    def _fullname(cls):
        """Full class name"""
        return cls.__module__+'.'+cls.__name__
    
    def todict(self):
        """Convert this to a dictionary"""
        from .. stat.io import west2dict
        
        return {'class':   XBinned._fullname(),
                'edges':   list(self._binedges),
                'profile': welford2dict(self._profile),
                'bins':    [b.todict() for b in self._bins] }

    def fromdict(self,d):
        """Read state from dictionary"""
        from .. stat.io import dict2west

        assert d['class'] == XBinned._fullname()

        self._binedges       = array(d['edges'])
        self._profile._state = dict2welford(d)._state
        self._bins           = [b.fromdict(bd)
                                for b, bd in zip(self._bins, d['bins'])]

    def format(self,*,x=None,fw=8,prec=4,header=True,n=None,dn=None):
        s = ''
        if header:
            s = self._bins[0].format(x=x,fw=fw,prec=prec,header=True,
                                     n=True,dn=True)+'\n'

        s += '\n'.join([b.format(x=x,fw=fw,prec=prec,header=False,
                                 n=n,dn=dn)
                        for b,n,dn in zip(self._bins,
                                          self._profile.mean,
                                          self._profile.sem)])
        return s
                                          
    def print(self,x,out=None,fw=8,prec=4):
        from sys import stderr

        if out is None:
            out = stderr

        print(self.format(x=x,fw=fw,prec=prec,header=True),file=out)
        

#
# EOF
#
