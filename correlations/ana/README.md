# Analysis sub-package 

Here we find code for various kinds of analyses performed within this
project. 

- [`Analyzer`](analyzer.py) is the base class for most analyses 
  - The class `Progress` provides a nice little progress bar with ETA
- [`Reader`](reader.py) is a class for reading data files 
- [`Writer`](writer.py) is a class for writing data files 
- [`Integrated`](integrated.py) does integrated flow over all
  particles 
- [`Differential`](differential.py) does $`p_{\mathrm{T}}`$
  differential flow. 
- To come: $`p_{\mathrm{T}}`$ differential flow with $`\eta`$
  gap. I.e., particles-of-interest are picked from within a range of
  transverse momentum _and_ from say $`\eta>\Delta\eta`$ while
  reference particles are chosen with say $`\eta<-\Delta\eta`$. 
- [`Corr`](corr.py) Stress test and benchmark of calculations of
  correlators. 
- [`XBin`](xbin.py) a wrapper around a flow calculator bin that
  provides a mechanism for selecting particles to analyze. 
- [`XBinned`](xbinned.py) a simple wrapper around multiply bins of
  flow calculator bins.  This class automatically selects the bin to
  update for a single particle and keeps track of how many particles
  where processed by each.  This is useful for differential analyses. 
- [`LowHigh`](xbin.py) A bin that corresponds to a range of low to
  high values. 
- [`Progress`](progress.py) A progress bar. 

Note, these analyses are _examples_ of how to perform various analyses
using the framework.  Clearly, in for other analyses one may need
other approaches. 
  
## Input

All analyses are fed data in the form of _NumPy_ arrays where each row
is 

- $`\eta`$ pseudorapidity of the particles 
- $`p_{\mathrm{T}}`$ transverse momentum of the particles 
- $`\varphi`$ azimuth angle of the particles (in radians)
- $`w`$ weight of a particle (acceptance or tracking efficiency, for
  example). 
  
The utility function [`correlations.ana.run_main`](tools.py) sets up
the input to be either directly from a generator (see also the
[`correlations.gen`](../gen) package, or from an input file. 

All analyses also provide an incremental method for analysis.
Essentially, one may run the same analysis  $`N`$ times _but without
final calculations_ to store the state.  These states can then be
_merged_ and the final result calculated on this merged state.  This
allows us to run the analyses in parallel using for example [GNU
parallel](https://gnu.org/software/parallel). 

## Output 

The analyses always outputs results (or state) as JSON formatted
data. The choice of output format is motivated by the simplicity and
readability of JSON data.  Also, since final results (or intermittent
states) are small, we never really get into a situation of large,
unwieldy, plain text files. 

## Visualisation 

The package [`correlations.scipts`](../scripts) provides some scripts
to visualise the results of the calculations performed by the analyses
above. 
  
## Status 

- [x] [Base class `Analyzer`](analyzer.py)
- [x] [Reader](reader.py)
- [x] [Writer](writer.py)
- [ ] Flow analyses
  - [x] [Integrated flow](integrated.py)
  - [x] Transverse momentum [differential flow](differential.py)
  - [ ] Transverse momentum differential flow in partitions (i.e., 
        with a pseudorapidity gap)

## Tests 

