# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class CleanOutput:
    def __init__(self,out):
        self._o = out
        
    def __enter__(self):
        return self

    def __exit__(self,etype,eval,tb):
        if etype is None or self._o is None:
            return

        from os import unlink
        self._o.close()
        unlink(self._o.name)

        return False

# --- Run an analyzer ------------------------------------------------
def run_ana(ana,rnd,**kwargs):
    """Run an analyzer 

    Parameters 
    ----------
    ana : Analyzer 
        The analyzer to run 
    rnd : numpy.random.Generator 
        Random number generator
    kwargs : dict 
        Keyword arguments. Recognised keywords are 

        nev : int 
            Number of events to process per input (file or generator).
            If less than zero, process all events from file(s)
        final : bool 
            If ``True``, run final calculations and output result.  
            Otherwise, the state is written out. 

        The following options only makes sense for file input data 

        input : file or list of files 
            File or list of files to process.  More than one file can
            be processed. However, final calculations will only be
            done after processing all files.
        merge : bool 
            If ``True`` then input files are assumed to contain state
            after processing event data.  The states are read in and
            merge into the current analyzer.  No event data is
            processed.

        The following keywords only makes sense with generated event
        data. First, for the generator 

        generator : str ('Momentum', 'Jetlike', other)
            The type of generator to use 
        min_multiplicty : int 
            Least number of particles to produce 
        max_multiplicty : int 
            Largest number of particles to produce 
        fixed_multiplicity : bool 
            If ``True`` always make exactly ``min_multiplicity``
            particles
        random_psi : bool 
            If ``True`` make random event plane angle 

        Then for the sampler 

        sampler : str ('PhiW','EtaPhiW','PtPhiW','EtaPtPhiW') 
            The type of sampler to use
        flow_coefficients : sequence of float 
            Flow coefficients 
        eta_slope : float 
            Slope of pseudorapidity distribution
        pt_bins : sequence of float 
            Bin boundaries for transverse momentum generation 
        pt_coefficients : sequence of float 
            Coefficients of transverse momentum modulation 
        mean_pt : float 
            Mean transverse momentum 
    """
    from .. gen import Generator, Sampler
    from numpy import linspace

    nev  =     kwargs.get('events',   100)
    inp  =     kwargs.get('input',    None)
    out  =     kwargs.get('output',   None)
    fini = not kwargs.get('no_final', False)

    l = max([len(k) for k in kwargs])
    for k,v in kwargs.items():
        if inp is not None:
            if k in ['generator',
                     'min_multiplicity',
                     'max_multiplicity',
                     'fixed_multiplicity',
                     'random_psi',
                     'acceptance',
                     'sampler',
                     'flow_coefficients',
                     'eta_slope',
                     'pt_coefficients',
                     # 'pt_bins',
                     'mean_pt']:
                continue
        else:
            if k in ['input']:
                continue

        if k in ['input'] and v is not None:
            print(f'{k+":":{l+1}} {[vv.name for vv in v]}')
            continue 
        if k in ['output'] and v is not None:
            print(f'{k+":":{l+1}} {v.name}')
            continue 
        
        print(f'{k+":":<{l+1}s} {v}')
        
    
    if inp is not None:
        linp = list(inp)
        for ii in linp:
            ana(nev,
                file=ii,
                read=kwargs.get('merge',False),
                fini=fini and ii is linp[-1],
                out=out)

        return

    samp = Sampler.make(kwargs.get('sampler','EtaPtPhiW'),  rnd,
                        kwargs.get('flow_coefficients',     [0.02,0.07,0.03]),
                        slope =kwargs.get('eta_slope',      .1),
                        ptbin =kwargs.get('pt_bins',        linspace(0,10,11)),
                        ptcoef=kwargs.get('pt_coefficients',[0, 1.77, -0.31]),
                        meanpt=kwargs.get('mean_pt',        2))
    gen  = Generator.make(kwargs.get('generator', None), rnd, samp,
                          kwargs.get('min_multiplicity',	80),
                          kwargs.get('max_multiplicity',	100),
                          kwargs.get('fixed_multiplicity',	False),
                          kwargs.get('random_psi',		False),
                          kwargs.get('acceptance',              False))

    ana(nev,generator=gen,fini=fini,out=out)

# --- Define comment command line arguments --------------------------
def arg_setup(desc='Run program',flow=False):
    from argparse import ArgumentParser, FileType

    ap = ArgumentParser(desc)

    gt =  ['default','Momentum','Jetlike']
    gr = ap.add_argument_group('Generator',
                               'Arguments that effect the generator used')
    gr.add_argument('-G','--generator',type=str,default='default',
                    choices=gt,  help='Generator to use')    
    gr.add_argument('-r','--min-multiplicity',type=int,default=8,
                    help='Minimum multiplicity of particles')
    gr.add_argument('-R','--max-multiplicity',type=int,default=10,
                    help='Maximum multiplicity of particles')
    gr.add_argument('-F','--fixed-multiplicity',action='store_false',
                    help='Fix number of particles to min-multiplicity')
    gr.add_argument('-Y','--random-psi',action='store_true',
                    help='Make random event plane')
    gr.add_argument('-A','--acceptance',action='store_false',
                    help='Whether to simulate acceptance according to weight')

    st =  ['PhiW','EtaPhiW','PtPhiW','EtaPtPhiW']
    gr = ap.add_argument_group('Sampler',
                               'Arguments that effect the sampler used')
    gr.add_argument('-S','--sampler',type=str,default='EtaPtPhiW',
                    choices=st, help='Sampler to use')
    gr.add_argument('-v','--flow-coefficients',type=float, nargs='*',
                    default=[0.02,0.07,0.03],
                    help='Flow coefficients v1, v2, v3, ...')
    gr.add_argument('-E','--eta-slope',type=float,default=.1,
                    help='Slope of pseudorgridity distribution')
    gr.add_argument('-V','--pt-coefficients',type=float, nargs='*',
                    default=[0, 1.77, -0.31],
                    help='Flow coefficients')
    gr.add_argument('-t','--pt-bins',type=float,nargs='*',
                    default=[0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
			     1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.5, 3.0,
			     3.5, 4.0, 4.5, 5.0],
                    help='Transverse momentum bin boundaries')
    gr.add_argument('-T','--mean-pt',type=float, default=2,
                    help='Mean transverse momentum')

    
    gr = ap.add_argument_group('General',
                               'Arguments that effect in general')    
    gr.add_argument('-i','--input',type=FileType('r'),default=None,
                    help='Input file(s)',nargs='*')
    gr.add_argument('-o','--output',type=FileType('w'),default=None,
                    help='Output file')
    gr.add_argument('-e','--events',type=int,default=0,
                    help='Maximum number of events')
    gr.add_argument('-s','--seed',type=int,default=0,
                    help='Random number seed')
    gr.add_argument('-l','--merge',action='store_true',
                    help='Merge input files with state')
    gr.add_argument('-f','--no-final',action='store_true',
                    help='Do not perform final calculations but output state')
    

    if not flow:
        return ap

    corr_names =  ['Closed','Recurrence','Recursive']
    pol_names  =  ['Derivatives', 'Jackknife', 'Bootstrap']
    gr = ap.add_argument_group('Flow options')
    gr.add_argument('-n','--max-harmonic',type=int,default=4,
                    help='Maximum harmonic order (n) for v_n{m}')
    gr.add_argument('-m','--max-particles',type=int,default=4,
                    help='Maximum correlator size (m) for v_n{m}')
    gr.add_argument('-w','--use-weights',action='store_false',
                    help='Use and filter on weights')
    gr.add_argument('-c','--correlator',type=str,default=corr_names[0],
                    choices=corr_names,help='Type of correlator algorithm')
    gr.add_argument('-u','--uncertainty',type=str,default=pol_names[0],
                    choices=pol_names,help='Type of uncertainty calculator')

    return ap

#
# EOF
#
