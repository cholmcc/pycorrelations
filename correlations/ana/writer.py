# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from . analyzer import Analyzer

class Writer(Analyzer):
    """Writes generated data to file

    For example 

    .. image:: ../static/gen_phi_medium_dist.png 

    or 

    .. image:: ../static/gen_pt_medium_dist.png 

    """
    def __init__(self):
        super(Writer,self).__init__()

    def dump_header(self,h):
        from sys import stdout

        out = self._out
        if out is None:
            out = stdout

        h.write(out)
        
    def _from_file(self,file,maxev,read,fini):
        """Cannot run from file"""
        raise ValueError('A generator object must be given')

    def event(self,eta,pt,phi,weight):
        r"""Process a single event 

        Parameters 
        ----------
        eta : array 
            Pseudorapidity (:math:`\eta=-\log\tan(\vartheta/2)`) of particles 
        pt : array 
            Transverse momentum (:math:`p_{\mathrm{T}}`) of particles 
        phi : array 
            Azimuth angle (:math:`\varphi`) of particles 
        weights : array 
            Weight (:math:`w`) of particles         
        """
        from sys import stdout 
        out = stdout if self._out is None else self._out
        
        print(len(eta),file=out)
        for e,p,a,w in zip(eta,pt,phi,weight):
            print('\t'.join([str(x) for x in [e,p,a,w]]),file=out)

    def end(self,file):
        """End of job - does nothing"""
        pass

#
# EOF
#

        
