# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --- Q-vector -------------------------------------------------------
class QVector:
    r"""Construct a :math:`Q`-vector 

    It provides member functions for reseting, filling and
    accessing the individual :math:`Q`-vector components.

    Parameters
    ----------
    max_harmonic : int 
        Largest harmonic 
    max_particles : int 
        Largest number of particles to correlate 
    use_weights : bool 
        If true, use weights 
    dtype : type 
        Complex type to use

    """

    REF = 0x1
    POI = 0x2
    
    def __init__(self,
                 max_harmonic,
                 max_particles,
                 use_weights=True,
                 dtype=complex):
        from numpy import zeros, complex64, complex128, complex256, \
            arange, newaxis

        assert dtype in [complex,complex64,complex128,complex256], \
            'Passed dtype={dtype} not complex'
        
        self._maxN       = max_harmonic
        self._maxM       = max_particles
        self._useWeights = use_weights
        self._ps         = arange(self.sizeM)
        self._ns         = (arange(0,2*self.sizeN-1)[:,newaxis])
        self._q          = zeros((2*self.sizeN-1,self.sizeM),dtype=dtype)
        
    @property
    def maxN(self): return self._maxN

    @property
    def maxM(self): return self._maxM

    @property
    def sizeN(self): return self._maxM // 2 * self._maxN + 1

    @property
    def sizeM(self): return self._maxM + 1

    @property
    def weights(self): return self._useWeights
    
    def reset(self):
        """Reset the :math:`Q`-vector to null"""
        self._q[:,:] = 0

    def fill(self,phis,weights):
        r"""Fill in observations 

        For a single observation, we first calculate the vector 

        .. math::

            \vec\phi = \vec{n}\phi\quad\text{or}\quad \phi^n = n\phi\quad.

        Then, we calculate the complex vector 

        .. math::

            \vec z = \cos\vec\phi + i\sin\vec\phi
            \quad\text{or}\quad z^n = \cos n\phi + i\sin n\phi\quad.

        We calculate the weight power vector 

        .. math::

            \vec w = w^{\vec p}\quad\text{or}\quad w^p = w^p\quad,

        and take the outer product 

        .. math::
        
            U = \vec w^T \vec \phi\quad\text{or}\quad U^{np} = w^p z^n\quad,

        which is a matrix we add to the current :math:`Q`-vector 

        If ``phi`` and ``weights`` are multiple observations, then
        first calculates the matrix
        
        .. math::
        
            N = \vec{\varphi}^T\vec{h}\quad,
     
        which is the outer product of all angles with the possible
        harmonics (i.e, a vector from 0 to the maximum harmonic stored
        :math:`H`).  That is, the :math:`(i,n)` element is given by
     
        .. math::
        
            \Phi^{in} = n\phi_i\quad,
     
        where :math:`i` labels the observation. 
     
        Then, we take the cosine and sine of all these angles
     
        .. math::
        
            C = \cos(\Phi)\quad S = \sin(\Phi)\quad,
     
        and form the complex matrix 
     
        .. math::
     
            Z = C + iS\quad,
     
        of size :math:`(H+1)\times M` where :math:`M` is the number of
        :math:`\varphi` observations. Again, the :math:`(i,n)` element
        is given by

        .. math::
        
            Z^{in} = \cos(N^{in})+i\sin(N^{in})\quad.
     
        Next, we calculate the matrix 

        .. math::

            W = \vec{w}^{\vec{p}}\quad,

        That is, we take all weights and raise them to all possible
        powers (from 0 to :math:`P`).  This matrix is of size
        :math:`M\times(P+1)`, and the :math:`(i,p)` element is given by 
     
        .. math::
        
            W^{ip} = w_i^p\quad.
     
        Finally, we calculate the matrix product 
     
        .. math::
        
            U = ZW\quad\text{or}\quad U^{np} = Z_{ni}W^{ip}\quad,
     
        of size :math:`(H+1)\times(P+1)`, which is our update to the 
        :math:`Q`-vector.

        Parameters 
        ----------
        phis : array 
            Azimuthal angles :math:`\varphi`
        weights : array 
            Weights :math:`w`

        """
        from numpy import atleast_1d, newaxis, ones, exp
        
        # wp is each weight to all powers, weights by row
        wa = atleast_1d(weights)
        wp = (wa[:,newaxis])**self._ps if self._useWeights else \
            ones((len(wa),self.sizeM))

        # np is each angle multiplied by all n's, angles by row
        np = self._ns*atleast_1d(phis)

        # z is complex of the angles
        z  = exp(1j*np)
        
        # Now calculate update as matrix product
        u = (z @ wp)

        self._q += u

    def __getitem__(self,idx):
        r"""Get the :math:`(h,p)` element.  Note :math:`h` may be negative

        Get 
        
        .. math::
        
            Q_{h,p} = \sum_{i=1}^{M} w_i^{p}e^{i\varphi_i h}\quad.

        Note, :math:`Q_{-h,p} = Q_{h,p}^*`.  :math:`h` is typically a
        sum of :math:`p` elements of an harmonic vector
        :math:`\mathbf{h}=\{h_1,\ldots,h_p\}`

        .. math::
        
            h = \sum_{i=1}^{p} h_i = \|\mathbf{h}\|_1\quad.

        Parameters
        ----------
        idx : tuple (h,p)
            Harmonic (h) and power (p) to get 

        Returns
        -------
        q_hp : complex 
            Q-vector element :math:`Q_{h,p}`

        """
        from numpy import conjugate, abs
        n, p = idx
        return self._q[-n,p].conj() if n < 0 else self._q[n,p]
        # return conjugate(self._q[an,p],where=n<0)

    def __str__(self):
        """Return string representation of this Q-vector"""
        ws = 8
        pr = 3
        s = '[' + \
            ',\n'.join([' ['+
                        ','.join([f'{self[n,p].real:{ws}.{pr}f}+'
                                  f'{self[n,p].imag:{ws}.{pr}f}j'
                                  for p in range(0,self.sizeM)]) +
                        f'] # {n:3d}'
                        for n in range(-self.sizeN+1,self.sizeN)]) + ']'
        return s
        
    @classmethod
    def calcMaxN(cls,harmonics):
        """Calculate the maximum harmonic from list of harmonics"""
        from numpy import abs, sum
        return sum(abs([getattr(h,'h',h) for h in harmonics]))

    @classmethod
    def make(cls,*,harmonics=None,maxN=None,maxM=None,weights=True):
        r"""Make a Q-vector given harmonics to use

        Parameters
        ----------
        kwargs : dict 
            Possible fields 

            maxN, maxM : int, int 
                Largest harmonic, largest order (:math:`n` and
                :math:`m` of :math:`C_{n}\{m\}`)
            harmonics : array of int 
                Vector of harmonics to use.  ``maxN`` and ``maxM`` is
                extracted as the largest harmonic and length of the
                array.
            weights : bool 
                Whether to use weights or not. 
        """
        if harmonics is not None:
            maxN = cls.calcMaxN(harmonics)
            maxM = len(harmonics)

        return cls(maxN,maxM,weights)
    
# --- Storage of Q-vectors -------------------------------------------
class QStore:
    r"""Construct a storage from one or three :math:`Q`-vectors
    
    Parameters
    ----------
    r : QVector
        Reference Q-vector
    p : QVector 
        Particle of interest Q-vector. 
    q : QVector (optional)
        Overlap Q-Vector
    no_fill_r : bool 
        If ``True`` then do not fill the reference Q-vector(s). These
        are assumed to be filled externally, for example if the same
        reference Q-vector is used in multiple places. This can save
        some potentially expensive calculations
    """
    def __init__(self,r,p=None,q=None,no_fill_r=False):
        # For integrated flow
        if p is None:
            if q is not None:
                raise ValueError('p and q must both be given')

            p = r
            q = r

        self._no_fill_r = no_fill_r
        try:
            len(r)
            self._r = r
            self._p = p
            self._q = q
        except:
            self._r = [r]
            self._p = [p]
            self._q = [q]


        if len(self._r) != len(self._p) or len(self._p) != len(self._q):
            raise ValueError("Inconsistent sizes of r,p,q: "\
                             f"{len(self._r)},{len(self._r)}{len(self._r)}")

        if type(self._r) is not type(self._p) or \
           type(self._p) is not type(self._q):
            raise ValueError("Inconsistent types of containers of r,p,q: "\
                             f"{type(self._r)},{type(self._p)},{type(self._q)}")
            
        self._k = list(range(len(self._r)))
        if isinstance(self._r,dict):
            self._k = list(self._r.keys())
            

    def _qc(self,harmonic,power,container):
        """Get the appropriate :math:`Q`-vector given partitions"""
        p = getattr(harmonic,'p',0)
        n = getattr(harmonic,'h',harmonic)
        if p is None:
            return complex()

        return container[p][n,power]

    def reset(self):
        for k in self._k:
            self._r[k].reset()

            if self._p is self._r:
                continue

            self._p[k].reset()
            self._q[k].reset()

    def fill(self,phi,weight,cls,partition):
        r"""Fill in observations into the appropriate :math:`Q`-vectors. 
        
        All arguments should be NumPy arrays to allow for effective
        views and indexing.

        For differential flow calculations, we have three
        :math:`Q`-vectors (per partition), and the argument ``cls``
        selects which each particle should contribute to (reference,
        of-interest, or both).

        For integrated flow calculations, we only have one
        :math:`Q`-vector (per partition), and the ``cls`` argument is
        can be left out.  In that case, all particles are assumed to
        be of reference type.

        If we have defined partitions, i.e., by passing multiple
        :math:`Q`-vectors for each class, then the argument
        ``partition`` specifies which partition each particle belongs
        to.  Note, the partition identifiers must be the same.

        For example, if we defined two partitions, ``'A'`` and
        ``'B'``, like, for integrated correlators

        .. code-block::
          
            r  = {'A': QVector(...), 'B': QVector(...) }
            s  = QStore(r)

        or for differential correlators 

        .. code-block::
          
            r  = {'A': QVector(...), 'B': QVector(...) }
            p  = {'A': QVector(...), 'B': QVector(...) }
            q  = {'A': QVector(...), 'B': QVector(...) }
            s  = QStore(r,p,q)
        
        Then we must take care to fill particles in using these
        identifiers. For example 

        .. code-block::

            eta, pt, phi, weight = get_datat()

            poi  = where(pt > 5,  QVector.POI, 0)
            ref  = where(pt > .2, QVector.REF, 0)
            cls  = poi | ref
            part = where(eta > 0,'A','B')

            s.fill(phi,weights,cls,part)

        We do not need to use labels or such for the partitions.
        Instead, we can use indexes.  The same as above, for integral
        correlators

        .. code-block::

            r  = [QVector(...), QVector(...)]
            s  = QStore(r)

        For differential correlators 

        .. code-block::

            r  = [QVector(...), QVector(...)]
            p  = [QVector(...), QVector(...)]
            q  = [QVector(...), QVector(...)]
            s  = QStore(r,p,q)

        and for both, we use indexes as identifiers 
        
            eta, pt, phi, weight = get_data()

            poi  = where(pt > 5,  QVector.POI, 0)
            ref  = where(pt > .2, QVector.REF, 0)
            cls  = poi | ref
            part = where(eta > 0,0,1)

            s.fill(phi,weights,cls,part)

        Parameters 
        ----------
        phi : array of float, shape=(M,)
            Azimuth angles :math:`\varphi` 
        weight : array of float, shape=(M,)
            Weights :math:`w` 
        cls : array of bit mask, shape=(M,) (optional)
            If given, then it must be an array of bit masks - one 
            for each :math:`\varphi`. The recognised values are 

            :attr:`QVector.REF` : ``0x1`` 
                The corresponding particle is a reference particle 
            :attr:`QVector.POI` : ``0x2`` 
                The corresponding particle is of-interest 
            :attr:`QVector.REF` ``|`` :attr:`QVector.POI` : ``0x3`` 
                The corresponding particle is *both* for reference
                *and* of-interest.

            If not given, assume all particles are reference
            particles.

        partition : array of identifiers, shape=(M,) (optional)
            Identifier of partition each particle belongs to. 
        
            If initially partitions ``'A'``, ``'B'``, and ``'C'`` where 
            set up, we can define if a particle belongs in any of these 
            by entering in one of these identifiers at the appropriate 
            place in this array.

        """
        for k in self._k:
            # Select partition
            mphi = phi    if partition is None else phi   [partition==k]
            mw   = weight if partition is None else weight[partition==k]

            if self._p is self._r:
                if cls is not None:
                    mc   = cls if partition is None else cls[partition==k]
                    ref  = (mc & QVector.REF) > 0
                    mphi = mphi[ref]
                    mw   = mw[ref]

                self._r[k].fill(mphi,mw)
                continue

            mc   = cls    if partition is None else cls   [partition==k]
            poi  = (mc & QVector.POI) > 0
            cmn  = (mc & (QVector.REF|QVector.POI)) > 0

            self._p[k].fill(mphi[poi],mw[poi])
            self._q[k].fill(mphi[cmn],mw[cmn])
            if not self._no_fill_r:
                ref = mc & QVector.REF
                self._r[k].fill(mphi[ref],mw[ref])
        
    def r(self,harmonic,power):
        """Get reference :math:`Q`-vector component"""
        return self._qc(harmonic,power,self.rvec)

    def p(self,harmonic,power):
        """Get particle-of-interest :math:`Q`-vector component"""
        return self._qc(harmonic,power,self.pvec)

    def q(self,harmonic,power):
        """Get overlap :math:`Q`-vector component"""
        return self._qc(harmonic,power,self.qvec)

    @property 
    def rvec(self):
        """Get reference :math:`Q`-vector"""
        return self._r

    @property 
    def pvec(self):
        """Get particle-of-interest :math:`Q`-vector"""
        return self._p

    @property 
    def qvec(self):
        """Get overlap :math:`Q`-vector"""
        return self._q         

    @property
    def maxN(self):
        """Largest harmonic"""
        return self.rvec[self._k[0]].maxN

    @property
    def maxM(self):
        """Largest order"""
        return self.rvec[self._k[0]].maxM

    @property
    def weights(self):
        """Using weights or not"""
        return self.rvec[self._k[0]].weights

    @property
    def partitions(self):
        """Partition keys"""
        return self._k if len(self._k) > 1 else None

    def __str__(self):
        def _one(n,q):
            return f' {n}:\n'+\
                f' {q}'.replace('\n','\n  ')
        s = ""
        for k in self._k:
            s += f'{k}:\n'+_one('r',self.rvec[k])
            if self._r is self._p:
                continue

            s += _one('p',self.pvec[k])
            s += _one('q',self.qvec[k])

        return s
    
        
    @classmethod
    def makeQs(cls,harmonics=None,maxN=None,maxM=None,
               partitions=None,weights=False):
        if partitions is None:
            return [QVector.make(harmonics=harmonics,
                                 maxN=maxN,
                                 maxM=maxM,
                                 weights=weights)]
        elif isinstance(partitions,int):
            return [QVector.make(harmonics=harmonics,
                                 maxN=maxN,
                                 maxM=maxM,
                                 weights=weights)
                    for _ in range(partitions)]

        return {p:QVector.make(harmonics=harmonics,
                               maxN=maxN,
                               maxM=maxM,
                               weights=weights)
                for p in partitions}

    @classmethod
    def make(cls,diff,*,
             harmonics=None,maxN=None,maxM=None,
             partitions=None,weights=None,r=None):
        r"""Generate a storage for :math:`Q`-vector(s)
    
        Parameters
        ----------
        diff : bool 
            If true, generate storage suitable for differential correlators 
        harmonics : array 
            The harmonics that will be used.  Passing this will
            automatically deduce ``maxN`` and ``maxM`` as the maximum
            absolute element of ``harmonics`` and the length of
            ``harmonics``, respectively.
        maxN : int 
            Largest absolute harmonic to calculate 
        maxM : int 
            Largest correlator order to use 
        partitions : int, sequence (optional)
            If given it is either 
    
            n : int 
                The number of partitions.  In this case, the partitions
                are identified by index from 0 up to (but not including)
                ``n``,
            keys : list 
                Each element of the list identifies the partition.  The
                keys must be hashable
    
        weights : bool 
            If true, take weights into account 
        r : QVector, sequence of QVector
            If passed, then this will be used as the :math:`Q`-vector(s)
            for reference observations.  This allows us to reuse the same
            reference :math:`Q`-vector for various differential correlators 
        """
        if not diff:
            return cls(cls.makeQs(harmonics=harmonics,maxN=maxN,maxM=maxM,
                                  partitions=partitions,weights=weights) \
                       if r is None else r)
        
        ext_r = r is not None
        if isinstance(partitions,list):
            if sum([i != p for i, p in enumerate(partitions)]) == 0:
                partitions = len(partitions)

        r     = cls.makeQs(harmonics,maxN,maxM,partitions,weights) \
            if not ext_r else r
        p     = cls.makeQs(harmonics,maxN,maxM,partitions,weights)
        q     = cls.makeQs(harmonics,maxN,maxM,partitions,weights)
        
        return cls(r,p,q,ext_r)
    
#
# EOF
#
