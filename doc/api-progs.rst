The ``correlations.progs`` sub-package
======================================

.. automodapi:: correlations.progs
   :include-all-objects:
   :no-heading:
