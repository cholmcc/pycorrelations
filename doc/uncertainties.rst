Estimatimation of uncertainties
===============================

The framework provides three ways of estimating the statistical
uncertainties  on the flow calculations:

* `Full propagation`__ of uncertainty using the derivatives of
  cumulants and flow with respect to the correlators and the full
  covariance of the correlators.

* `Bootstrap`__ simulations over sub-samples

* `Jackknife`__ simulations over sub-samples

__ :class:`correlations.stat.Derivates`
__ :class:`correlations.stat.Bootstrap`
__ :class:`correlations.stat.Jackknife`

Traditionally, the bootstrap and jackknife methods has been used,
because the cumulants have been perceived as too complicated to
analyse and many frameworks do not readily provides mechanisms for
storing covariance.  To address these issues, this framework
explicitly derives the derivatives needed and uses West's algorithm
for online updates (via the class :class:`nbi_stat.West` - see `here`__).

__ https://cholmcc.gitlab.io/nbi-python/nbi_stat

Below is a comparison of using all three methods for integrated flow

.. image:: static/gen_phi_medium_intg_uncr.png

Clearly, the full uncertainty calculation is more stable than the
Jackknife and Bootstrap methods.

Below are two plots of differential flow - the first using the full
uncertainty propagation and the second using Bootstrap

.. image:: static/gen_pt_medium_diff.png

.. image:: static/gen_pt_medium_diff_bootstrap.png


Again, the Bootstrap method underperforms.

Complexities
------------

For :math:`N` correlators needed (e.g., four for four particle
differential flow) the space complexity of full uncertainty
propagation is

* :math:`N` for means

* :math:`N^2` for the covariance

* :math:`2N^2` for sum of weights and square weights and another
  :math:`N` for weight of means

That yields a total complexity of

.. math::

   N+N^2+2N^2+N = 3N^2+2N

For the Bootstrap and Jackknife methods with :math:`M` sub-samples, we
need

* :math:`N` for the means
  
* :math:`N` for each sub-sample for means

Thus, the total complexity becomes


.. math::

   M\times (N+1)


The time complexity for the full propagation is

.. math::

   O(N\log N)

while for Jackknife and Bootstrap it is twice that (at least), since
we need to update means for sub-samples.


   
