Use of the `correlations` package
=================================

.. toctree::
   :maxdepth: 2

For flow measurements
---------------------

The package provides a number of utilities to make formulation of flow
analyses simple.  Let us start out with integrated flow - that is,
where we analyse *all* particles (or observations) for the common
azimuth Fourier coefficients.

Integrated flow
^^^^^^^^^^^^^^^

The example analysis :class:`correlations.ana.Integrated` illustrates
this.  Here, we make use of the class :class:`correlations.ana.XBin`
to set up a single bin that simply selects all observations.  We then
use an instance of that class with an inner
:class:`corrrelations.flow.Reference` object.

.. code-block::

   b = XBin(Reference(maxN,maxM,weights))

Here, ``maxN`` is the largest flow coefficient to calculate (i.e., the
largest :math:`n` in :math:`v_n\{m\}`) and ``maxM`` is the largest
number of particles (or observations) to correlate (i.e., the largest
:math:`m` in :math:`v_n\{m\}`).  The boolean argument ``weights``
selects whether we want to take observation weights into account
or not.

.. note::

   More arguments are possible, and in the example analysis we set a
   member of the class, but for illustration purposes we keep it
   simpler (albeit not precise) here.  Please refer to the example
   analysis :class:`correlations.ana.Integrated` and other relevant
   documentation for more details.

Having defined our integrated flow bin through the wrapper
:class:`correlations.ana.XBin` (actually a derived class), we are
ready to do our analysis.

At the start of each event we must ensure that the internal
:math:`Q`-vector is reset to zero, so we call ``reset`` on our wrapper
object.

.. code-block::

   b.reset()

.. note::

   In the analysis framework defined by the class
   :class:`correlations.ana.Analyzer` this is done in the ``pre``
   method which is executed before the actual event processing.

Assuming we have the observed azimuth angle :math:`\varphi` in the
variable ``phi`` and associated weights in ``weight`` (typically both
one-dimensional *NumPy* array), we fill in our observations by calling
the ``fill`` method on our wrapper object

.. code-block::

   b.fill(phi,phi,weight)

Note, we pass ``phi`` twice - once as the independent variable we
select on, and once as the observation.  Remember, we set up our
selection to select all particles above, but we do need to get the
right shape of our selection - hence the need to pass in ``phi``
twice in this case.

Once the observations have been filled in, we can calculate the
correlator values :math:`C_{\mathbf{n}}\{m\}` for the event.

.. code-block::

   b.update()

This automatically calculates the correlators we need and updates the
mean and covariances of these on-the-fly

.. note::

   Again, in the analysis framework defined by the class
   :class:`correlations.ana.Analyzer` this is done in the ``post``
   method which is executed before the actual event processing.
   
.. note::

   We make use of West's online algorithm for calculating means and
   covariances via the class :class:`nbi_stat.West`.  See also the
   documentation__ of that class, as well as the appropriate chapter
   of the accompanying book__

   __ https://cholmcc.gitlab.io/nbi-python/statistics/nbi_stat
   __ https://cholmcc.gitlab.io/nbi-python/statistics/#Statistik

After we have processed all events, we can calculate all the needed
cumulants :math:`c_{\mathbf{n}}\{m\}` and from that the wanted flow
coefficients :math:`v_n\{m\}`

.. code-block::

   result = b()

This will return a dictionary of values (including uncertainties) of
the flow coefficients in the variable ``result``.  The
:math:`v_n\{m\}` value (with uncertainty) can be extracted by double
indexing the dictionary

.. code-block::

   vnm = result[n][m]
   print(f'v_{n}{{{m}}} = {vnm[0]} +/- {vnm[1]}')

.. note::

   This is done in the ``end`` method of
   :class:`correlations.ana.Analyzer` and the results are written to a
   JSON formatted output file.

The result of running this analysis over 1000 generated events with

.. math::

   v_1 &=& 0.02\\
   v_2 &=& 0.07\\
   v_3 &=& 0.03

is shown in the plot below

.. image:: static/gen_phi_medium_intg.png

Differential flow
^^^^^^^^^^^^^^^^^

The example analysis :class:`correlations.ana.differential`
does transverse momentum *differential* flow measurements

.. math::

   v'_n\{m\}\quad\text{versus}\quad p_{\mathrm{T}}

Here, we will use the utility class :class:`correlations.ana.XBinned`
which wraps a number of :class:`correlations.ana.LowHigh` objects,
which in turn wraps :class:`correlations.flow.OfInterest` bins.
That is, our :class:`correlations.ana.LowHigh` selects
:class:`correlations.flow.OfInterest` bins to update with specific
observations based on the passed independent variable (in this case,
the transverse momentum :math:`p_{\mathrm{T}}`).

Here, we will use as *reference* observations all observations that
fall within the selected transverse momentum range.  Thus, we define
an integrated bin much like above, except we select particles that
have a transverse momentum within range. We assume that our bin ranges
are in the variable ``ptbins`` (typically a *NumPy* array made with
for example ``numpy.linspace`` or ``numpy.geomspace``)

.. code-block::
   
    ib = LowHigh(ptbins[0], ptbins[-1],
                 correlations.flow.Reference(maxN,maxM,weights))

We an now set up our transverse momentum differential bins.  

.. code-block:

    db = XBinned.make(ptbins,correlations.flow.OfInterest,
                      integrated=ib.bin)

As before, we need to zero all internal :math:`Q`-vectors before the
start of each event.  We do that by calling the method ``reset`` on
both ``ib`` and ``db``

.. code-block::

    ib.reset()
    db.reset()

Next, for each event, we want to fill in the observations minding the
transverse momentum bins we've defined.  Since we will use all
observations as reference particles, we label all of them with the
attribute :attr:`correlations.QVector.REF`.  We assume we have the
observations of the transverse momentum, azimuth angle, and weights in
the variables ``pt``, ``phi``, and ``weight`` respectively (typically
all *NumPy* one-dimensional arrays)

.. code-block::

   from numpy import full_like

   cls = full_like(pt, correlations.QVector.REF,dtype=int)

   ib.fill(pt,phi,weight,cls)
   db.fill(pt,phi,weight,cls)

The class :class:`correlations.ana.XBinned` will automatically select
appropriate observations for each defined bin and then label those
observations as *of-interest* (:attr:`correlations.QVector.POI`).

Once the event observations are filled in to our :math:`Q`-vectors, we
must calculate the needed correlators (both integrated and
differential), and update the means and covariances.

.. code-block::

   ib.update()
   db.update()


After all events are processed, we can calculate the needed cumulants
and flow coefficients.

.. code-block::

   result = db()

This will return a dictionary like

.. code-block::

   { 'profile' :
     { 'mean':  mean-number-of-observations-in-each-bin,
       'uncer': standard-error-on-means-in-each-bin }
     'edges': pt-bin-edges,
     'values':
     [ dictonary-of-flow-coefficients-in-first-bin,
       ...
       dictonary-of-flow-coefficients-in-last-bin ] }

which we can extract all the relevant values from.  For example

.. code-block::

   for pt0,pt1,vpnm in zip(result['edges'][:-1],
                           result['edges'][1:],
			   result['values']):
	print(f'{pt0} to {pt1}: {vpnm[n][m][0]} +/- {vpnm[n][m][1]}')


As an example, with 1000 generated events with an exponential
transverse momentum distribution

.. image:: static/gen_pt_medium_dist.png

and a :math:`p_{\mathrm{T}}` modulation of flow cofficients by

.. math::

    v'_n(p_{\mathrm{T}}) 
    &=& v_n\left(1.77p_{\mathrm{T}} - 0.31p_{\mathrm{T}}^2\right)\\
    v_1 &=& 0.02\\
    v_2 &=& 0.07\\
    v_3 &=& 0.03

we find the result

.. image:: static/gen_pt_medium_diff.png


Partioned flow
^^^^^^^^^^^^^^

By *partioned* flow we mean flow measurements performed over distinct
regions of the phase-space, also commonly referred to a s *sub-event*
flow, and specifically as flow with :math:`\eta`-gap.

The module :mod:`correlations.ana.partitioned` defines two example
analyses

* :class:`correlations.ana.partitioned.Integrated` for integrated flow
  using an :math:`\eta`-gap

* :class:`correlations.ana.partitioned.Differential` for transverse
  momentum differential flow using an :math:`\eta`-gap

To ease the formulation of these two analyses we define a
``Partitioner`` class.  It is the task of objects of this class to
assign observations to a particular partition.  We define two
partitions

* ``-`` for observations with :math:`\eta < \Delta\eta/2` and
* ``+`` for observations with :math:`\eta > \Delta\eta/2`

.. code-block::

    class Partitioner:
        def __init__(self,maxM,deltaEta):
            k           = maxM//2
            self._delta = deltaEta
            self._ids   = ['-']*k + ['+']*k
    
        @property 
        def ids(self):
            return self._ids
    
        def __call__(self,eta,pt,phi,weight):
            msk  = logical_or(eta < -self._delta/2,
                              eta >  self._delta/2)
            part = where(eta[msk] < self._delta/2, '-', '+')
            
            return eta[msk], pt[msk], phi[msk], weight[msk], part
       
Note, we are free to use any kind of (hashable) identifiers we want.
Note, that we can define any number of partitions we want, except that
we are limited by the maximum number of observations to correlate (the
:math:`m` in :math:`v_n\{m\}`).

We will continue with the differential example.

Next, we define our analysis.  For the sake of convenience we derive
from the class :class:`correlations.ana.differential.Differential`
(See above). Our set-up is thus

.. code-block::

   p  = Partitioner(maxM,deltaEta)
   ib = LowHigh(ptbins[0], ptbins[-1],
                correlations.flow.Reference(maxN,maxM,weights,
		                            partitions=p.ids))
   db = XBinned.make(ptbins,correlations.flow.OfInterest,
                     integrated=ib.bin)

That is bascially it.  As before, reset these on the start of each
event

.. code-block::

   ib.reset()
   db.reset()

For each event we need to fill in observations.  Here, we use our
``Partitioner`` class to make the proper selections


.. code-block::

    peta, ppt, pphi, pweight, part = p(eta,pt,phi,weight)
    cls = full_like(ppt, correlations.QVector.REF,dtype=int)

    ib.fill(ppt,pphi,pweight,cls,part)
    db.fill(ppt,pphi,pweight,cls,part)

Again, as before, we update statistics at the end of the event

.. code-block::

   ib.update()
   db.update()

After all events are processed, we can calculate the needed cumulants
and flow coefficients, just like before. 

.. code-block::

   result = db()

The result is a dictionary like above.

Let us see the effect of using partitions on the analysis.  Below is
are plot of flow coefficients determined over 1000 events where

* :math:`v_n` is modulated by the transverse momentum as

  .. math::

     v_n \rightarrow v_n(a p_{\mathrm{T}} + b p_{\mathrm{T}}^2)

* and for each large :math:`p_{\mathrm{T}}` there is an increasing
  probability that a jet-like structure is created.  What this means
  is, if a "jet" is made,

  * we create some random number of particles that are collimated
    (i.e., similar :math:`\eta,\varphi`) with the original particle
  * we mirror all these particle i.e. create copies with
    :math:`\eta\rightarrrow-\eta` and
    :math:`\varphi\rightarrow\varphi+\pi`

In this way, we introduce correlations into the data that one would
term as *non-flow* correlations (see also
:class:`correlations.gen.Jetlike`).

In the first plot, we *do not* use a partitioned analysis.

.. image:: static/jet_eta_medium_diff.png

As we see above, we get reasonable results, though we see
contributions of :math:`v_4` not present in the input.

In the second plot we *do* use a partitoned analysis.

.. image:: static/jet_eta_medium_pdif.png

We see improvements compared to above as we suppress contributions
from the introduced *non-flow*.




   

    
   
   
  
  
