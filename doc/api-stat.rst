The ``correlations.stat`` sub-package
=====================================

.. automodapi:: correlations.stat
   :include-all-objects:
   :no-heading:

