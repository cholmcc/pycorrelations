.. NBI Stat documentation master file, created by
   sphinx-quickstart on Fri Dec  7 23:58:41 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Correlations documentation!
======================================

This is a reference implementation of the *Generic framework for
anisotropic flow analyses with multi-particle azimuthal correlations*
in Python.  The `original reference implementation`__ is written in
C++, but this provides a pure Python implementation.

The mathematical bases for the generic framework is available in
`Phys.Rev.C89(2014)064904`__ 
or `arXiv:1312.3572`__. 


__ https://cern.ch/cholm/mcorrelations
__ https://journals.aps.org/prc/abstract/10.1103/PhysRevC.89.064904
__ http://arxiv.org/abs/1312.3572

.. toctree::
   :maxdepth: 2

   usage
   api
   uncertainties

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

  
