The ``correlations.ana`` sub-package
====================================

.. automodapi:: correlations.ana
   :include-all-objects:
   :no-heading:
