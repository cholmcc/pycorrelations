The ``correlations.flow`` sub-package
=====================================

.. automodapi:: correlations.flow
   :include-all-objects:
   :no-heading:      

.. automodapi:: correlations.flow.cumexpr
   :include-all-objects:
   :no-heading:
   :skip: binom
      
.. automodapi:: correlations.flow.flowexpr
   :include-all-objects:
   :no-heading:
