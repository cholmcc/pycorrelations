# correlations

This is a Python port of
[mcorrelations](https://cern.ch/cholm/mcorrelations) - the reference
implementation of the Generic Framework for particle correlations as
used in f.ex. flow measurements in High Energy Particle physics.

## Design 

This package will follow the design of the [original C++ header
library](https://cern.ch/cholm/mcorrelations).  That library uses
templates _extensively_ to simplify the code and calculations.  In
Python we do not have templates, but on the other hand the language is
weakly typed (as opposed to strictly typed like C++), so we need not
be so careful and we can defer method resolution etc. to runtime.
This will in some cases simplify the code somewhat (at the cost of
performance). 

Also, in Python, in particular when using
[_NumPy_](https://numpy.org), there is a lot to be gained from doing
as many simultaneous calculations as possible.  This means that we
will often assume that the user collects observables
($`\varphi,\eta,p_{\mathrm{T}}`$ and weights) into collections which
are then passed on to this package.   In C++ there is often no
performance penalty for doing individual rather than _en-bloc_
updates, and so the issue less important there. 

## Dependencies 

- [_NumPy_](https://numpy.org) 
- [`nbi_stat`](https://cholmcc.gitlab.io/nbi_python/statistics/#Statistik)
- [_Matplotlib_](https://matplotlib.org) 

Do 

	pip install -r requirements.txt
	
## Status 

- [ ] [Basic classes](correlations/)
- [ ] [Analysis package](correlations/ana)
- [x] [Generator package](correlatons/gen/)
- [x] [Statistics package](correlations/stat)
- [ ] [Flow package](correlations/flow)
- [ ] [Visualisation scripts](correlations/scripts)
  

<!--  LocalWords:  observables
 -->
