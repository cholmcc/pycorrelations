# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
#
UNCER		=  Derivatives
WRITE_ARGS	=  -r 1000 -R 1000 -A -s 42 
IMGS		=  gen_phi_medium_intg.png \
		   gen_phi_medium_dist.png \
		   gen_pt_medium_diff.png \
	           gen_pt_medium_dist.png \
		   gen_phi_small_corr.png \
		   gen_phi_medium_intg_uncr.png	\
		   jet_eta_medium_diff.png \
		   jet_eta_medium_pdif.png 
PYS		:= $(shell find . -name "*.py" -and -not -name nbi_stat.py \
		                 -and -not -name "conf.py")

all:
	$(MAKE) -C doc all

imgs:	$(IMGS)

test:
	python3 -m unittest discover tests -v 

pys:
	$(foreach p, $(PYS), echo "$(p)";)

doc_imgs:imgs
	cp -f $(IMGS) doc/static

clean:
	rm -rf *.json *~ 

	$(MAKE) -C doc $@

realclean: clean
	rm -rf *.conf *.png *.dat conf
	find . -name "__pycache__" | xargs rm -rf 

	$(MAKE) -C doc $@

%_small.dat:		NEV=100
%_medium.dat:		NEV=1000
%_large.dat:            NEV=10000

gen_%.dat:		GEN=default
jet_%.dat:		GEN=Jetlike
mom_%.dat:		GEN=Momentum

gen_phi_%.dat:		SAM=PhiW
gen_pt_%.dat:		SAM=PtPhiW
gen_eta_%.dat:		SAM=EtaPtPhiW

mom_pt_%.dat:		SAM=PtPhiW
mom_eta_%.dat:		SAM=EtaPtPhiW

jet_eta_%.dat:		SAM=EtaPtPhiW


%.dat:	correlations/progs/writer.py
	./$< -o $@ -e $(NEV) -G $(GEN) -S $(SAM) $(WRITE_ARGS)

%_dist.json:%.dat correlations/progs/distributions.py
	./correlations/progs/distributions.py -i $< -o $@

%_clsd.json %_recs.json %_recu.json: %.dat correlations/progs/benchmark.py

%_dist.json:			     correlations/progs/distributions.py
%_diff.json:			     correlations/progs/differential.py
%_intg.json:			     correlations/progs/integrated.py
%_pdif.json:                         correlations/progs/partitioned.py
%_pint.json:                         correlations/progs/partitioned.py

%_dist.json : %.dat
	./correlations/progs/distributions.py -i $< -o $@

%_diff.json : %.dat
	./correlations/progs/differential.py -i $< -o $@ -u $(UNCER)

%_intg.json : %.dat
	./correlations/progs/integrated.py -i $< -o $@ -u $(UNCER)

%_pdif.json : %.dat
	./correlations/progs/partitioned.py -i $< -o $@ -D -u $(UNCER)

%_pint.json : %.dat
	./correlations/progs/partitioned.py -i $< -o $@ -I -u $(UNCER)

%_clsd.json : %.dat 
	./correlations/progs/benchmark.py -i $< -o $@ -s 42 -a qvector_closed

%_recs.json : %.dat 
	./correlations/progs/benchmark.py -i $< -o $@ -s 42 -a qvector_recursive

%_recu.json : %.dat 
	./correlations/progs/benchmark.py -i $< -o $@ -s 42 -a qvector_recurrence

%_jackknife.json:
	if test -f $*.json ; then mv $*.json $*_keep.json ; fi 
	$(MAKE) $*.json UNCER=Jackknife
	mv $*.json $@
	if test -f $*_keep.json ; then mv $*_keep.json $*.json ; fi 

%_bootstrap.json:
	if test -f $*.json ; then mv $*.json $*_keep.json ; fi 
	$(MAKE) $*.json UNCER=Bootstrap
	mv $*.json $@
	if test -f $*_keep.json ; then mv $*_keep.json $*.json ; fi 

%_dist.png:		SCR=correlations/scripts/distributions.py
%_intg.png: 		SCR=correlations/scripts/integrated.py
%_intg_jackknife.png: 	SCR=correlations/scripts/integrated.py
%_intg_bootstrap.png: 	SCR=correlations/scripts/integrated.py
%_pint.png: 		SCR=correlations/scripts/integrated.py
%_diff.png: 		SCR=correlations/scripts/differential.py
%_diff_jackknife.png: 	SCR=correlations/scripts/differential.py
%_diff_bootstrap.png: 	SCR=correlations/scripts/differential.py
%_pdif.png: 		SCR=correlations/scripts/differential.py

%.png:%.json
	./$(SCR) $< $@

%_corr.png:%_clsd.json %_recs.json %_recu.json
	./correlations/scripts/benchmark.py  $^ -o $@

%_uncr.png:%.json %_jackknife.json %_bootstrap.json
	./correlations/scripts/uncertainty.py $^ -o $@

correlations/progs/%.py:correlations/ana/%.py

-include .depend

.depend: depend.sh $(PYS)
	@./depend.sh $^ > $@

.PRECIOUS:	%.json %.dat \
		%_jackknife.json %_bootstrap.json \
		%_diff.json %_intg.json \
		%_pint.json %_pdif.json
#
# EOF
#
